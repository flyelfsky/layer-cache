package cn.flyelf.cache.core;

import cn.flyelf.cache.core.model.CacheResult;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 缓存的hash类型的特定接口
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的映射key的类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/25
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/25 1.0 新增]
 */
public interface HashCache<K, H, V> extends Cache<K, Map<H, V>>{
    /**
     * 判定map容器中的某个key是否存在
     * @author wujr
     * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hk: map容器中的子key
     * @return 是否存在
     */
    Mono<CacheResult<Boolean>> exist(K key, H hk);

    /**
     * 删除map容器中的某些子键
     * @author wujr
     * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hks: map容器的子键
     * @return 删除的数量
     */
    Mono<CacheResult<Long>> remove(K key, Collection<H> hks);
    /**
     * 删除map容器中的某些子键
     * @author wujr
     * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hks: map容器的子键数组
     * @return 删除的数量
     */
    Mono<CacheResult<Long>> remove(K key, H... hks);

    /**
     * 获取map容器中的部分数据
     * @author wujr
     * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hks: map容器的部分key
     * @return map容器中的key对应的内容
     */
    Mono<CacheResult<List<V>>> get(K key, Collection<H> hks);
    /**
     * 获取map容器中的部分数据
     * @author wujr
     * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hks: map容器的部分key数组
     * @return map容器中的key对应的内容
     */
    Mono<CacheResult<List<V>>> get(K key, H... hks);

    /**
     * 在map容器中新增一个元素
     * 如果不存在key的map，则自动创建一个
     * @author wujr
     * 2020/3/6
     * 变更历史
     * [wujr 2020/3/6 1.0 新增]
     *
     * @param key: 缓存key
     * @param hk: map容器的key
     * @param hv: 实际的值
     * @return 加入结果
     */
    Mono<CacheResult<Boolean>> add(K key, H hk, V hv);
}
