package cn.flyelf.cache.core.model;

import lombok.Data;

/**
 * 缓存对象定义模型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@Data
public class CacheValue<V> {
    /**
     * 缓存值
     */
    private V value;
}
