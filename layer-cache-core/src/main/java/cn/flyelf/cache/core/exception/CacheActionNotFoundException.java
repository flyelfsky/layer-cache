package cn.flyelf.cache.core.exception;

/**
 * 没有找到缓存的动作
 *
 * @author wujr
 * 2019/12/19
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/19 1.0 新增]
 */
public class CacheActionNotFoundException extends CacheException {
    private static final long serialVersionUID = 9203846418957477737L;

    public CacheActionNotFoundException(String action){
        super(null, null, action);
    }
}
