package cn.flyelf.cache.core.support;

import io.netty.buffer.Unpooled;
import org.joor.Reflect;

import java.nio.ByteBuffer;

/**
 * 转换为字符串的对象序列化
 *
 * @author wujr
 * 2019/12/17
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/17 1.0 新增]
 */
public class StringObjectSerializer extends DefaultObjectSerializer {
    @Override
    protected <T> ByteBuffer wrap(T object) {
        return charset.encode(object.toString());
    }

    @Override
    protected  <T> T unwrap(ByteBuffer bytes, Class clz) {
        try {
            Reflect reflect = Reflect.onClass(clz).create(Unpooled.wrappedBuffer(bytes).toString(this.charset));
            return reflect.get();
        }catch (Exception e){
            throw new UnsupportedClassVersionError(clz.getName());
        }
    }
}
