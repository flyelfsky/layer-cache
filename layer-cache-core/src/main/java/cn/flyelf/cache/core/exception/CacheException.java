package cn.flyelf.cache.core.exception;

import cn.flyelf.cache.annotation.CacheConstant;

/**
 * 缓存异常
 *
 * @author wujr
 * 2019-12-15
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-15 1.0 新增]
 */
public class CacheException extends RuntimeException {
    private static final long serialVersionUID = -659242559424121340L;
    private final CacheConstant.RESULT result;
    private final String layerName;

    public CacheException(String layerName, CacheConstant.RESULT result, String message) {
        super(message);
        this.layerName = layerName;
        this.result = result;
    }

    public CacheException(String layerName, CacheConstant.RESULT result, String message, Throwable cause) {
        super(message, cause);
        this.layerName = layerName;
        this.result = result;
    }

    public CacheException(String layerName, CacheConstant.RESULT result, Throwable cause) {
        super(cause);
        this.layerName = layerName;
        this.result = result;
    }

    public CacheConstant.RESULT getResult(){
        return result;
    }
    public String getLayerName(){
        return layerName;
    }
}
