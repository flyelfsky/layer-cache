package cn.flyelf.cache.core.support;

import java.nio.ByteBuffer;

/**
 * 对象序列化接口定义
 *
 * @author wujr
 * 2019/12/16
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/16 1.0 新增]
 */
public interface ObjectSerializer {
    /**
     * 把对象编码为缓冲
     * @author wujr
     * 2019/12/16
     * 变更历史
     * [wujr 2019/12/16 1.0 新增]
     *
     * @param object: 对象
     * @param <T>: 对象类型
     * @return 缓冲对象
     */
    <T> ByteBuffer encode(T object);
    /**
     * 把缓存对象转换为对象
     * @author wujr
     * 2019/12/16
     * 变更历史
     * [wujr 2019/12/16 1.0 新增]
     *
     * @param bytes: 缓存
     * @param clz: 转换后的对象类
     * @param <T>: 结果类型
     * @return 对象
     */
    <T> T decode(ByteBuffer bytes, Class clz);
}
