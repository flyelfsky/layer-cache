package cn.flyelf.cache.core.action;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheConfigException;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import org.apache.commons.lang3.StringUtils;


/**
 * 缓存层处理器定义
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
public interface CacheLayerProcessor {
    /**
     * 缓存层的名称
     * 名称需要具有唯一性
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @return 缓冲层名称
     */
    default String name(){
        String fullName = this.getClass().getSimpleName();
        String baseName = CacheLayerProcessor.class.getSimpleName();
        if (fullName.endsWith(baseName)){
            return StringUtils.uncapitalize(StringUtils.substringBefore(fullName, baseName));
        }
        throw new CacheConfigException(StringUtils.uncapitalize(fullName));
    }

    /**
     * 缓存事件发布器
     * @author wujr
     * 2020/1/3
     * 变更历史
     * [wujr 2020/1/3 1.0 新增]
     *
     * @return 发布器
     */
    CacheEventPublisher eventPublisher();

    /**
     * 获取该缓存层支持的缓存动作
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @param action: 缓存动作
     * @param area: 缓存的区域
     * @return 缓存动作接口服务
     */
    CacheAction action(ACTION action, String area);

    /**
     * 获取该缓存层支持的缓存动作
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @param action: 缓存动作
     * @return 缓存动作接口服务
     */
    default CacheAction action(ACTION action){
        return action(action, CacheConstant.AREA_DEFAULT);
    }
}
