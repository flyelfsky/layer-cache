package cn.flyelf.cache.core.event;

import cn.flyelf.cache.core.model.CacheEvent;

/**
 * 缓存事件监听器
 *
 * @author wujr
 * 2019-12-26
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-26 1.0 新增]
 */
public interface CacheEventListener {
    /**
     * 接收到缓存事件的触发
     * @author wujr
    * 2019-12-26
     * 变更历史
     * [wujr 2019-12-26 1.0 新增]
     *
     * @param event: 缓存事件
     */
    void onCacheEvent(CacheEvent event);
    /**
     * 停止缓存事件的监听
     * @author wujr
    * 2019-12-26
     * 变更历史
     * [wujr 2019-12-26 1.0 新增]
     *
     */
    void onEventStop();
}
