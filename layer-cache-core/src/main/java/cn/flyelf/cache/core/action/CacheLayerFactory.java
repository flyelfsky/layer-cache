package cn.flyelf.cache.core.action;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.CacheActionNotFoundException;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.penetration.PenetrateCache;
import org.apache.commons.lang3.StringUtils;
import reactor.util.annotation.NonNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存层工厂模型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
public class CacheLayerFactory {
    /**
     * 缓存对象的策略
     * key：缓存对象的类名称+type+feature
     */
    private ConcurrentHashMap<String, CachePolicy> policies = new ConcurrentHashMap<>();
    /**
     * 缓存层实体
     * key：缓存层的名称
     */
    private ConcurrentHashMap<String, CacheLayerProcessor> layers = new ConcurrentHashMap<>(2);
    /**
     * 缓存穿透处理
     */
    private PenetrateCache penetration;

    public void setPenetration(PenetrateCache penetration){
        this.penetration = penetration;
    }
    public CachePolicy findPolicy(String[] layers, @NonNull Class<?> target, String type, String suffix){
        String key = target.getName() + "-"
                + (StringUtils.isBlank(type) ? CacheConstant.CACHE_SIMPLE : type)
                + "-" + Arrays.toString(layers)
                + (StringUtils.isBlank(suffix) ? "" : ("-" + suffix));
        return policies.get(key);
    }

    public void setPolicy(@NonNull List<CachePolicy> policies){
        this.policies.clear();
        policies.forEach(policy -> this.policies.put(policy.name(), policy));
    }
    public CachePolicy addPolicy(@NonNull CachePolicy policy){
        CachePolicy result = policies.putIfAbsent(policy.name(), policy);
        if (null == result){
            result = policy;
        }
        return result;
    }

    public void setLayers(@NonNull List<CacheLayerProcessor> processors){
        this.layers.clear();
        processors.forEach(processor -> this.layers.put(processor.name(), processor));
    }

    /**
     * 为某个缓存动作构建缓存执行的职责链
     * @author wujr
     * 2019-12-28
     * 变更历史
     * [wujr 2019-12-28 1.0 新增]
     *
     * @param action: 缓存动作
     * @param layers: 缓存层数组
     * @return 缓存链
     */
    public CacheActionChain buildActionChain(ACTION action, String[] layers){
        List<CacheAction> actions = new ArrayList<>(layers.length);
        for (String layer : layers){
            CacheLayerProcessor processor = this.layers.get(layer);
            if (null != processor){
                actions.add(processor.action(action));
            }
        }
        if (actions.isEmpty()){
            throw new CacheActionNotFoundException(action.toString());
        }
        return new DefaultCacheActionChain(action, actions, penetration);
    }
}
