package cn.flyelf.cache.core.server;

/**
 * 缓存加载器接口定义
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@FunctionalInterface
public interface CacheObjectLoader<K, V> {
    V load(K key);
}
