package cn.flyelf.cache.core.monitor;

import cn.flyelf.cache.core.conf.CacheGlobalConfig;
import cn.flyelf.cache.core.event.AbstractCacheEventSubscriber;

/**
 * 缓存监控器
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
public abstract class AbstractCacheMonitor extends AbstractCacheEventSubscriber {
    private final CacheGlobalConfig globalConfig;

    AbstractCacheMonitor(CacheGlobalConfig globalConfig){
        this.globalConfig = globalConfig;
    }
    /**
     * 全局参数配置
     * @author wujr
    * 2019-12-27
     * 变更历史
     * [wujr 2019-12-27 1.0 新增]
     *
     * @return 全局配置
     */
    CacheGlobalConfig globalConfig(){
        return this.globalConfig;
    }
}
