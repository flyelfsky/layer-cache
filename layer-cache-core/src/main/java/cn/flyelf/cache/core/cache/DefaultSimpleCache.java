package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.annotation.CacheConstant;

/**
 * 默认的简单对象缓存
 * @param <K>: 缓存key类型
 * @param <V>: 缓存对象类型
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
public class DefaultSimpleCache<K, V> extends AbstractDefaultCache<K, V> {
    public DefaultSimpleCache(CacheLayerFactory factory, String[] layers, Class keyClass, Class targetClass, Class serializeClass, String suffix){
        super(factory, layers, keyClass, targetClass, serializeClass, CacheConstant.CACHE_SIMPLE, suffix);
    }
    public DefaultSimpleCache(CacheLayerFactory factory, String[] layers, Class keyClass, Class targetClass, String suffix){
        this(factory, layers, keyClass, targetClass, targetClass, suffix);
    }
    public DefaultSimpleCache(CacheLayerFactory factory, String[] layers, Class keyClass, Class targetClass, Class serializeClass){
        this(factory, layers, keyClass, targetClass, serializeClass, null);
    }
    public DefaultSimpleCache(CacheLayerFactory factory, String[] layers, Class keyClass, Class targetClass){
        this(factory, layers, keyClass, targetClass, targetClass);
    }
}
