package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.ListCache;
import cn.flyelf.cache.core.exception.CacheException;
import cn.flyelf.cache.core.exception.CacheMultipleLayerException;
import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.core.server.CacheGetExchange;
import cn.flyelf.cache.core.server.CachePutExchange;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.*;

/**
 * 默认的list的容器缓存服务
 * @param <K>: 缓存key类型
 * @param <V>: 缓存对象类型
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
@Slf4j
public class DefaultListCache<K, V> extends AbstractDefaultCache<K, List<V>> implements ListCache<K, V> {
    private static final String EMPTY_ERROR = "缓存的list类型的push操作，在push时，内容不能为空";
    public DefaultListCache(CacheLayerFactory factory, String[] layers, Class keyClass, Class target, String suffix){
        super(factory, layers, keyClass, target, target, CacheConstant.CACHE_LIST, suffix);
    }
    public DefaultListCache(CacheLayerFactory factory, String[] layers, Class keyClass, Class target){
        this(factory, layers, keyClass, target, null);
    }

    @Override
    public Mono<CacheResult<List<V>>> get(K key, CacheObjectLoader<K, List<V>> loader) {
        Map<String, Object> attachment = new HashMap<>(2);
        attachment.put(CacheConstant.ATTACH_LIST_START, CacheConstant.POS_START);
        attachment.put(CacheConstant.ATTACH_LIST_STOP, CacheConstant.POS_STOP);
        return get(key, ACTION.GETLIST, loader, attachment);
    }
    @Override
    public Mono<CacheResult<Boolean>> put(K key, List<V> value) {
        return put(key, value, ACTION.PUTLIST);
    }

    @Override
    public Mono<CacheResult<V>> rpop(K key){
        return pop(key, CacheConstant.LIST_RIGHT);
    }

    @Override
    public Mono<CacheResult<V>> lpop(K key){
        return pop(key, CacheConstant.LIST_LEFT);
    }
    private Mono<CacheResult<V>> pop(K key, String direction){
        if (policy.getLayer().length > 1){
            log.warn("缓存的list类型的pop操作不支持多层缓存");
            return Mono.error(new CacheMultipleLayerException(policy.getObject().getSimpleName()));
        }
        CacheActionChain chain = factory.buildActionChain(ACTION.POP, policy.getLayer());
        CacheExchange<K, V, V> exchange = new CacheGetExchange<>(key, serializeClass, policy);
        exchange.getRequest().addAttachment(CacheConstant.ATTACH_LIST_DIRECTION, direction);
        return chain.cache(exchange);
    }
    @Override
    public Mono<CacheResult<Boolean>> lpush(K key, List<V> value){
        return pushObject(key, CacheConstant.LIST_LEFT, value);
    }
    List<V> asList(K key, V[] values){
        if (null == values || values.length == 0){
            log.warn(EMPTY_ERROR);
            throw new InvalidCacheValueException(null, key.toString());
        }
        List<V> list = new ArrayList<>(values.length);
        for (V v : values){
            if (null != v){
                list.add(v);
            }
        }
        if (list.isEmpty()){
            log.warn(EMPTY_ERROR);
            throw new InvalidCacheValueException(null, key.toString());
        }
        return list;
    }
    @SafeVarargs
    @Override
    public final Mono<CacheResult<Boolean>> lpush(K key, V... value){
        List<V> list;
        try {
            list = asList(key, value);
        }catch (CacheException e){
            return Mono.error(e);
        }
        return pushObject(key, CacheConstant.LIST_LEFT, list);
    }
    @Override
    public Mono<CacheResult<Boolean>> rpush(K key, List<V> value){
        return pushObject(key, CacheConstant.LIST_RIGHT, value);
    }
    @SafeVarargs
    @Override
    public final Mono<CacheResult<Boolean>> rpush(K key, V... value){
        List<V> list;
        try {
            list = asList(key, value);
        }catch (CacheException e){
            return Mono.error(e);
        }
        return pushObject(key, CacheConstant.LIST_RIGHT, list);
    }
    Mono<CacheResult<Boolean>> pushObject(K key, String direction, List<V> value){
        if (policy.getLayer().length > 1){
            log.warn("缓存的list类型的push操作不支持多层缓存");
            return Mono.error(new CacheMultipleLayerException(policy.getObject().getSimpleName()));
        }
        if (null == value || value.isEmpty()){
            log.warn(EMPTY_ERROR);
            return Mono.error(new InvalidCacheValueException(null, key.toString()));
        }
        CacheActionChain chain = factory.buildActionChain(ACTION.PUSH, policy.getLayer());
        CacheExchange<K, List<V>, Boolean> exchange = new CachePutExchange<>(key, value, serializeClass, policy);
        exchange.getRequest().addAttachment(CacheConstant.ATTACH_LIST_DIRECTION, direction);
        return chain.cache(exchange);
    }
}
