package cn.flyelf.cache.core.support;

import io.netty.buffer.Unpooled;
import org.apache.commons.io.serialization.ValidatingObjectInputStream;

import java.io.*;
import java.nio.ByteBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;

/**
 * 默认的对象序列化工具
 *
 * @author wujr
 * 2019/12/16
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/16 1.0 新增]
 */
public class DefaultObjectSerializer implements ObjectSerializer {
    Charset charset = StandardCharsets.UTF_8;

    @Override
    public <T> ByteBuffer encode(T object) {
        if (object instanceof String){
            return charset.encode((String)object);
        }else if (object instanceof Number){
            Number number = (Number)object;
            return charset.encode(String.valueOf(number));
        }
        return wrap(object);
    }

    protected <T> ByteBuffer wrap(T object){
        try {
            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
            ObjectOutputStream os = new ObjectOutputStream(bytes);
            os.writeObject(object);
            os.close();
            byte[] body = bytes.toByteArray();
            return ByteBuffer.wrap(body);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public <T> T decode(ByteBuffer bytes, Class clz) {
        if (String.class.isAssignableFrom(clz)){
            return (T)Unpooled.wrappedBuffer(bytes).toString(this.charset);
        }else if (Integer.class.isAssignableFrom(clz)){
            return (T)Integer.valueOf(Unpooled.wrappedBuffer(bytes).toString(this.charset));
        }else if (Long.class.isAssignableFrom(clz)){
            return (T)Long.valueOf(Unpooled.wrappedBuffer(bytes).toString(this.charset));
        }else if (Short.class.isAssignableFrom(clz)){
            return (T)Short.valueOf(Unpooled.wrappedBuffer(bytes).toString(this.charset));
        }else if (Float.class.isAssignableFrom(clz)){
            return (T)Float.valueOf(Unpooled.wrappedBuffer(bytes).toString(this.charset));
        }else if (Double.class.isAssignableFrom(clz)){
            return (T)Double.valueOf(Unpooled.wrappedBuffer(bytes).toString(this.charset));
        }else if (Number.class.isAssignableFrom(clz)){
            String value = Unpooled.wrappedBuffer(bytes).toString(this.charset);
            return (T)Long.valueOf(value);
        }
        return unwrap(bytes, clz);
    }

    @SuppressWarnings("unchecked")
    protected <T> T unwrap(ByteBuffer bytes, Class clz){
        try {
            byte[] array = new byte[bytes.remaining()];
            bytes.get(array);
            ByteArrayInputStream bis = new ByteArrayInputStream(array);
            ValidatingObjectInputStream ois = new ValidatingObjectInputStream(bis).accept(clz).accept("java.*","[Ljava.*");
            Object object = ois.readObject();
            return (T)clz.cast(object);
        } catch (Exception e) {
            return null;
        }
    }
}
