package cn.flyelf.cache.core.monitor;

import cn.flyelf.cache.core.conf.CacheGlobalConfig;
import cn.flyelf.cache.core.model.CacheEvent;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Flux;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * 缓存监控服务
 *
 * @author wujr
 * 2019/12/25
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/25 1.0 新增]
 */
@Slf4j
public class CacheMonitorService extends AbstractCacheMonitor {
    /**
     * 最近一段时间的汇总
     * key：缓存层的存储区域，layer-area-action
     */
    private final ConcurrentHashMap<String, CacheStat> stats = new ConcurrentHashMap<>(2);
    private final ConcurrentSkipListSet<String> cacheKeys = new ConcurrentSkipListSet<>();
    private Long lastTicket;

    public CacheMonitorService(CacheGlobalConfig globalConfig){
        super(globalConfig);
        lastTicket = System.currentTimeMillis();
        runMonitor();
    }

    private void runMonitor(){
        Flux.interval(Duration.ofSeconds(globalConfig().getCacheMonitorInterval())).subscribe(l -> printCacheStat());
    }

    void printCacheStat(){
        StringBuilder sb = buildLogTitle();
        if (cacheKeys.isEmpty()){
            lastTicket = System.currentTimeMillis();
            log.info(sb.toString());
            return;
        }
        int maxNameLength = 0;
        List<CacheStat> list = new ArrayList<>(cacheKeys.size());
        for (String key : cacheKeys) {
            CacheStat stat = stats.replace(key, new CacheStat());
            if (null != stat) {
                stat.setCacheName(key);
                stat.setEnd(System.currentTimeMillis());
                list.add(stat);
                maxNameLength = Math.max(maxNameLength, key.length());
            }
        }
        lastTicket = System.currentTimeMillis();
        if (list.isEmpty()){
            lastTicket = System.currentTimeMillis();
            log.info(sb.toString());
            return;
        }

        String title = statTitle(maxNameLength);

        sb.append(title).append("\n");
        printSepLine(sb, title);
        for (CacheStat s : list) {
            buildCacheStat(maxNameLength, sb, s);
        }
        printSepLine(sb, title);
        log.info(sb.toString());
    }
    private String statTitle(int maxNameLength){
        String[] fmt = {"%-" + maxNameLength + "s", "%10s", "%8s", "%14s", "%14s", "%14s", "%8s", "%8s", "%8s", "%10s", "%10s"};
        String[] values = {"cache action", "qps", "hitRate", "total count", "hit count", "fail count", "average", "hit avg", "fail avg",
                "max used", "min used"};
        StringBuilder sb = new StringBuilder();
        for (int index = 0; index < fmt.length; ++ index){
            sb.append(String.format(fmt[index], values[index])).append(END_FLAG);
        }
        return sb.toString();
    }

    private static final char END_FLAG = '|';
    private void printSepLine(StringBuilder sb, String title) {
        title.chars().forEach(c -> {
            if (c == END_FLAG) {
                sb.append('+');
            } else {
                sb.append('-');
            }
        });
        sb.append('\n');
    }
    private StringBuilder buildLogTitle(){
        StringBuilder sb = new StringBuilder(1024);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss,SSS");
        sb.append("\nlayer-cache 打印缓存的使用统计信息，统计时间： ").append(sdf.format(new Date(lastTicket)))
                .append("  -  ").append(sdf.format(new Date())).append("\n");
        return sb;
    }
    private static final String FMT_AVERAGE = "%,8.2f";
    private static final String FMT_TOTAL = "%,14d";
    private static final String FMT_BORDER = "%,10d";
    private void buildCacheStat(int maxNameLength, StringBuilder sb, CacheStat stat){
        String[] fmt = {"%-" + maxNameLength + "s", "%,10.2f", "%,7.2f%%", FMT_TOTAL, FMT_TOTAL, FMT_TOTAL, FMT_AVERAGE, FMT_AVERAGE, FMT_AVERAGE};
        Object[] values = {stat.getCacheName(), stat.qps(), stat.hitRate() * 100, stat.getTotal(), stat.getSuccess(), stat.getFailure(),
                stat.average(), stat.successAverage(), stat.failureAverage()};
        for (int index = 0; index < fmt.length; ++ index){
            sb.append(String.format(fmt[index], values[index])).append(END_FLAG);
        }
        sb.append(stat.max(FMT_BORDER)).append(END_FLAG);
        sb.append(stat.min(FMT_BORDER)).append(END_FLAG);
        sb.append('\n');
    }

    @Override
    public void onCacheEvent(CacheEvent event) {
        log.trace("接收到缓存事件: {}", event);
        String key = event.getLayer() + "-" + event.getArea() + "-" + event.getAction().owner();
        CacheStat stat = stats.computeIfAbsent(key, k -> {
            cacheKeys.add(k);
            return new CacheStat();
        });

        if (event.getResult().success()){
            // 请求成功了
            stat.success(event.used());
        }else{
            stat.failure(event.used());
        }
    }
}
