package cn.flyelf.cache.core.event;

import cn.flyelf.cache.core.model.CacheEvent;

/**
 * 缓存事件发布器
 *
 * @author wujr
 * 2019/12/25
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/25 1.0 新增]
 */
public interface CacheEventPublisher {
    /**
     * 发布事件
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param event: 事件
     */
    void publish(CacheEvent event);

    /**
     * 订阅缓存事件
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param subscriber:

     */
    void subscribe(AbstractCacheEventSubscriber subscriber);

    /**
     * 停止缓存事件的发布
     * @author wujr
    * 2019-12-26
     * 变更历史
     * [wujr 2019-12-26 1.0 新增]
     *
     */
    void stop();
}
