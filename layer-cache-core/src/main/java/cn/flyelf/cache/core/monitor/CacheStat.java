package cn.flyelf.cache.core.monitor;

import lombok.Getter;
import lombok.Setter;

/**
 * 缓存汇总模型
 *
 * @author wujr
 * 2019/12/25
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/25 1.0 新增]
 */
@Getter
@Setter
public class CacheStat {
    private String cacheName;
    /**
     * 计时的开始时间
     */
    private Long tick = System.currentTimeMillis();
    /**
     * 停止计时的时间
     */
    private Long end;
    /**
     * 成功的数量
     */
    private long success = 0L;
    /**
     * 总次数
     */
    private long total = 0L;
    /**
     * 失败的次数
     */
    private long failure = 0L;
    /**
     * 平均耗时，毫秒
     */
    private double average = 0.;
    /**
     * 成功的平均耗时，毫秒
     */
    private double averageSuccess = 0.;
    /**
     * 失败的平均耗时，毫秒
     */
    private double averageFailure = 0.;
    /**
     * 总的耗时，毫秒
     */
    private double usedTotal = 0.;
    /**
     * 成功的总耗时，毫秒
     */
    private double usedSuccess = 0.;
    /**
     * 失败的总耗时，毫秒
     */
    private double usedFailure = 0.;
    /**
     * 最短耗时，毫秒
     */
    private long min = Long.MAX_VALUE;
    /**
     * 最长耗时，毫秒
     */
    private long max = Long.MIN_VALUE;

    public String max(String format){
        if (max == Long.MIN_VALUE){
            return String.format(format, 0);
        }
        return String.format(format, max);
    }
    public String min(String format){
        if (min == Long.MAX_VALUE){
            return String.format(format, 0);
        }
        return String.format(format, min);
    }

    public void success(long used){
        ++ total;
        ++ success;
        usedTotal += used;
        usedSuccess += used;
        min = Math.min(min, used);
        max = Math.max(max, used);
    }
    public void failure(long used){
        ++ total;
        ++ failure;
        usedTotal += used;
        usedFailure += used;
        min = Math.min(min, used);
        max = Math.max(max, used);
    }
    public double qps(){
        return tps(total);
    }
    public double hitRate(){
        if (total == 0){
            return 0;
        }
        return 1. * success / total;
    }
    public double average(){
        if (total == 0){
            return 0;
        }
        return usedTotal / total;
    }
    public double successAverage(){
        if (success == 0){
            return 0;
        }
        return usedSuccess / success;
    }
    public double failureAverage(){
        if (failure == 0){
            return 0;
        }
        return usedFailure / failure;
    }

    double tps(long count){
        long t = used();
        if (t == 0) {
            return 0;
        } else {
            return 1000.0 * count / t;
        }
    }
    private long used(){
        long t = end == null ? System.currentTimeMillis() : end;
        return t - tick;
    }
}
