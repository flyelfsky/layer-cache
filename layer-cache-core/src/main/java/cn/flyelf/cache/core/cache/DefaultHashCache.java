package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.HashCache;
import cn.flyelf.cache.core.server.*;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheResult;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 默认的map的容器缓存服务
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的映射key类型
 * @param <V>: 缓存对象类型
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
@Slf4j
public class DefaultHashCache<K, H, V> extends AbstractDefaultCache<K, Map<H, V>> implements HashCache<K, H, V> {
    public DefaultHashCache(CacheLayerFactory factory, String[] layers, Class<?> keyClass, Class<?> target, String suffix){
        super(factory, layers, keyClass, target, target, CacheConstant.CACHE_MAP, suffix);
    }
    public DefaultHashCache(CacheLayerFactory factory, String[] layers, Class<?> keyClass, Class<?> target){
        this(factory, layers, keyClass, target, null);
    }

    @Override
    public Mono<CacheResult<Map<H, V>>> get(K key, CacheObjectLoader<K, Map<H, V>> loader) {
        return get(key, ACTION.GETMAP, loader, null);
    }
    @Override
    public Mono<CacheResult<Boolean>> put(K key, Map<H, V> value) {
        return put(key, value, ACTION.PUTMAP);
    }

    /**
     * 判定map容器中的某个key是否存在
     * @author wujr
    * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hk: map容器中的子key
     * @return 是否存在
     */
    @Override
    public Mono<CacheResult<Boolean>> exist(K key, H hk){
        checkKey(key);
        CacheActionChain chain = factory.buildActionChain(ACTION.HASMAPKEY, policy.getLayer());
        CacheExchange<K, Boolean, Boolean> exchange = new CacheGetExchange<>(key, policy.object(), policy);
        exchange.getRequest().addAttachment(CacheConstant.ATTACH_HASH_HK, hk);
        return chain.cache(exchange);
    }

    /**
     * 删除map容器中的某些子键
     * @author wujr
    * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hks: map容器的子键
     * @return 删除的数量
     */
    @Override
    public Mono<CacheResult<Long>> remove(K key, Collection<H> hks){
        return deleteByHk(key, hks);
    }
    @SafeVarargs
    @Override
    public final Mono<CacheResult<Long>> remove(K key, H... hks){
        return deleteByHk(key, hks);
    }
    private Mono<CacheResult<Long>> deleteByHk(K key, Object hks){
        checkKey(key);
        CacheActionChain chain = factory.buildActionChain(ACTION.DELMAPKEY, policy.getLayer());
        CacheExchange<K, Long, Long> exchange = new CacheDefaultExchange<>(new CacheRequest<>(key, policy.object()), policy);
        exchange.getRequest().addAttachment(CacheConstant.ATTACH_HASH_HK, hks);
        return chain.cache(exchange);
    }

    /**
     * 获取map容器中的部分数据
     * @author wujr
    * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hks: map容器的部分key
     * @return map容器中的key对应的内容
     */
    @Override
    public Mono<CacheResult<List<V>>> get(K key, Collection<H> hks){
        if (hks == null || hks.isEmpty()){
            log.warn("缓存的map容器的根据容器的key获取内容时，容器key不能为空");
            return Mono.error(new InvalidCacheValueException(null, key.toString()));
        }
        return getByHk(key, hks);
    }

    /**
     * 获取map容器中的部分数据
     * @author wujr
    * 2019-12-21
     * 变更历史
     * [wujr 2019-12-21 1.0 新增]
     *
     * @param key: 缓存key
     * @param hks: map容器的部分key
     * @return map容器中的key对应的内容
     */
    @SafeVarargs
    @Override
    public final Mono<CacheResult<List<V>>> get(K key, H... hks){
        if (hks == null || hks.length == 0){
            log.warn("缓存的map容器的根据容器的key获取内容时，容器key不能为空");
            return Mono.error(new InvalidCacheValueException(null, key.toString()));
        }
        return getByHk(key, hks);
    }
    private Mono<CacheResult<List<V>>> getByHk(K key, Object hks){
        checkKey(key);
        CacheActionChain chain = factory.buildActionChain(ACTION.GETMAPKEY, policy.getLayer());
        CacheExchange<K, List<V>, List<V>> exchange = new CacheGetExchange<>(key, policy.getObject(), policy, null);
        exchange.getRequest().addAttachment(CacheConstant.ATTACH_HASH_HK, hks);
        return chain.cache(exchange);
    }

    /**
     * 在map容器新增一个元素
     * @author wujr
     * 2020/3/6
     * 变更历史
     * [wujr 2020/3/6 1.0 新增]
     *
     * @param key: 缓存key
     * @param hk: map容器的key
     * @param hv: 具体的值
     * @return 加入结果
     */
    @Override
    public Mono<CacheResult<Boolean>> add(K key, H hk, V hv){
        checkKey(key);
        CacheActionChain chain = factory.buildActionChain(ACTION.ADDMAPKEY, policy.getLayer());
        CacheExchange<K, Map<H, V>, Boolean> exchange = new CacheDefaultExchange<>(new CacheRequest<>(key, policy.object()), policy);
        Map<H, V> map = new HashMap<>(1);
        map.put(hk, hv);
        exchange.getRequest().setValue(map);
        return chain.cache(exchange);
    }
}
