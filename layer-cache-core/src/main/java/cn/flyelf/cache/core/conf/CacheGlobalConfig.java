package cn.flyelf.cache.core.conf;

import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import lombok.Data;

/**
 * 缓存全局配置模型
 *
 * @author wujr
 * 2019-12-27
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-27 1.0 新增]
 */
@Data
public class CacheGlobalConfig {
    /**
     * 缓存监控的输出时间间隔，秒
     */
    private Long cacheMonitorInterval;
    /**
     * 缓存穿透的配置
     */
    private CachePenetrationConfig penetrationConfig;
}
