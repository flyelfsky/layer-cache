package cn.flyelf.cache.core.server;

import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheResponse;

/**
 * 设置缓存内容的交换模型
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
public class CachePutExchange<K, V> extends CacheDefaultExchange<K, V, Boolean> {
    public CachePutExchange(K key, V value, Class serializeClass, CachePolicy policy){
        super(new CacheRequest<>(key, value, serializeClass), new CacheResponse<>(), policy, null);
    }
}
