package cn.flyelf.cache.core.exception;

/**
 * 无效的缓存内容
 *
 * @author wujr
 * 2019-12-20
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-20 1.0 新增]
 */
public class InvalidCacheValueException extends CacheException {
    private static final long serialVersionUID = 3669772556103536703L;

    public InvalidCacheValueException(String layer, String key){
        super(layer, null, key);
    }
}
