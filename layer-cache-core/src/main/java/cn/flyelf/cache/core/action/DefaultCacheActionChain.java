package cn.flyelf.cache.core.action;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.penetration.PenetrateCache;
import cn.flyelf.cache.penetration.model.PenetrateLockWrapper;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * 默认的缓存动作链，外部实际调用入口
 * 管理动作链的数组
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
public class DefaultCacheActionChain implements CacheActionChain {
    private final List<CacheAction> actions;
    private final ACTION action;
    private int index;
    private final PenetrateCache penetration;

    DefaultCacheActionChain(ACTION action, List<CacheAction> actions, PenetrateCache penetration){
        this.actions = actions;
        this.action = action;
        this.index = 0;
        this.penetration = penetration;
    }

    @SuppressWarnings("unchecked")
    private <K, V, R> Mono<CacheResult<R>> prePenetrate(CacheExchange<K, V, R> exchange){
        if (this.index == 0 && action.allowLoad() && penetration.enabled()){
            // 缓存穿透预防处理
            Object r = penetration.get(exchange.getRequest().getKey());
            if (null != r){
                // 存在key
                R v = penetration.isNull(r) ? null : (R)r;
                if (v == null){
                    exchange.getResponse().setThrowable(new CacheNotExistException(penetration.name(), exchange.getRequest().getKey().toString()));
                    return Mono.error(exchange.getResponse().getThrowable());
                }
                return Mono.just(CacheResult.success(penetration.name(), v));
            }
        }
        return null;
    }
    private <K, V, R> Mono<CacheResult<R>> cacheFailure(CacheExchange<K, V, R> exchange){
        if (action.allowLoad() && exchange.getLoader() != null){
            // 仅get动作支持加载
            final PenetrateLockWrapper locker = new PenetrateLockWrapper();
            return load(exchange, locker);
        }
        if (exchange.getResponse().getThrowable() != null){
            return Mono.error(exchange.getResponse().getThrowable());
        }else {
            return Mono.just(exchange.getResponse().getResult());
        }
    }
    @Override
    @SuppressWarnings("unchecked")
    public <K, V, R> Mono<CacheResult<R>> cache(CacheExchange<K, V, R> exchange) {
        Mono<CacheResult<R>> result = prePenetrate(exchange);
        if (null != result){
            return result;
        }
        return Mono.defer(() -> {
            if (this.index < actions.size()){
                int pos = this.index ++;
                return actions.get(pos).cache(exchange, this);
            }
            // 所有的缓存动作都失败了
            return cacheFailure(exchange);
        });
    }
    @Override
    @SuppressWarnings("unchecked")
    public <K, V, R> void synchronize(CacheAction<K, V, R> action, CacheExchange<K, V, R> exchange, CacheResult<R> result){
        // 同步结果到action前面的action
        if (result == null || !result.isSuccess()){
            return;
        }
        List<CacheAction> list = new ArrayList<>(actions.size());
        for (CacheAction record : actions){
            if (record == action){
                break;
            }
            list.add(record);
        }
        if (list.isEmpty()){
            return;
        }
        Flux.fromIterable(list).flatMap(a -> a.onLoadAfter(exchange, result.getValue()))
                .parallel()
                .subscribe();
    }
    @SuppressWarnings("unchecked")
    private <K, V, R> V penetrateLoad(PenetrateLockWrapper locker, CacheExchange<K, V, R> exchange){
        while (true) {
            penetration.getLocker(exchange.getRequest().getKey(), locker);
            if (locker.isOwner() || locker.isCurrent()){
                V value = exchange.getLoader().load(exchange.getRequest().getKey());
                locker.getLocker().setValue(value);
                locker.getLocker().setSuccess(true);
                return value;
            }
            try {
                if (!locker.getLocker().await(penetration.timeout())){
                    return exchange.getLoader().load(exchange.getRequest().getKey());
                }
            }catch (InterruptedException e){
                Thread.currentThread().interrupt();
                return exchange.getLoader().load(exchange.getRequest().getKey());
            }
            if (locker.getLocker().isSuccess()){
                return (V)locker.getLocker().getValue();
            }
        }
    }
    private boolean isEmptyCollection(Object value){
        return value instanceof Collection && ((Collection)value).isEmpty();
    }
    @SuppressWarnings("unchecked")
    <K, V, R> Mono<CacheResult<R>> load(CacheExchange<K, V, R> exchange, PenetrateLockWrapper locker){
        V value;
        if (penetration.enabled()){
            value = penetrateLoad(locker, exchange);
            if (!locker.isOwner() && !locker.isCurrent()){
                if (null == value){
                    exchange.getResponse().setThrowable(new CacheNotExistException(null, exchange.getRequest().getKey().toString()));
                    return Mono.error(exchange.getResponse().getThrowable());
                }
                return Mono.just((CacheResult<R>)CacheResult.success(null, value));
            }
        }else {
            value = exchange.getLoader().load(exchange.getRequest().getKey());
        }

        if (null == value && penetration.enabled()){
            // 需要进行缓存穿透处理
            penetration.put(exchange.getRequest().getKey(), exchange.getPolicy().getNullExpire());
            penetration.releaseLocker(locker);
            exchange.getResponse().setThrowable(new CacheNotExistException(null, exchange.getRequest().getKey().toString()));
            return Mono.error(exchange.getResponse().getThrowable());
        }else if (isEmptyCollection(value) && penetration.enabled()){
            // 需要进行缓存穿透处理
            penetration.put(exchange.getRequest().getKey(), value, exchange.getPolicy().getNullExpire());
            penetration.releaseLocker(locker);
            exchange.getResponse().setThrowable(new CacheNotExistException(null, exchange.getRequest().getKey().toString()));
            return Mono.error(exchange.getResponse().getThrowable());
        }

        // 发布从数据库加载结果的消息
        return publish(exchange, locker, value);
    }

    @SuppressWarnings("unchecked")
    <K, V, R> Mono<CacheResult<R>> publish(CacheExchange<K, V, R> exchange, PenetrateLockWrapper locker, V value){
        CacheResult<V> result = null;
        Flux flux = Flux.fromIterable(actions).flatMap(a -> a.onLoadAfter(exchange, value));
        if (null != value){
            if (value instanceof Collection) {
                if (!((Collection) value).isEmpty()) {
                    result = CacheResult.success(null, value);
                }
            }else if (value instanceof Map){
                if (!((Map)value).isEmpty()){
                    result = CacheResult.success(null, value);
                }
            }else{
                result = CacheResult.success(null, value);
            }
        }

        if (null != result){
            return flux.then(Mono.just((CacheResult<R>)result)).doOnSuccessOrError((r,e) -> penetration.releaseLocker(locker));
        }else{
            exchange.getResponse().setThrowable(new CacheNotExistException(null, exchange.getRequest().getKey().toString()));
            return flux.then(Mono.error(exchange.getResponse().getThrowable())).doOnSuccessOrError((r,e) -> penetration.releaseLocker(locker));
        }
    }

    @Override
    public boolean last(){
        return this.index == actions.size();
    }
}
