package cn.flyelf.cache.core.exception;

import cn.flyelf.cache.annotation.CacheConstant;

/**
 * 不支持的缓存对象
 *
 * @author wujr
 * 2019-12-14
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-14 1.0 新增]
 */
public class UnsupportedCacheObjectException extends CacheException {
    private static final long serialVersionUID = 2208015576845306499L;

    public UnsupportedCacheObjectException(String className){
        super(null, CacheConstant.RESULT.UNKNOWN, className);
    }
}
