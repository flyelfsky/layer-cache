package cn.flyelf.cache.core.model;

import lombok.Data;

import java.util.concurrent.TimeUnit;

/**
 * 时长模型
 *
 * @author wujr
 * 2019/12/19
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/19 1.0 新增]
 */
@Data
public class Duration {
    /**
     * 时长
     */
    private Long duration;
    /**
     * 时间单位
     */
    private TimeUnit unit;

    public Duration(){}

    public Duration(long duration, TimeUnit unit){
        this.duration = duration;
        this.unit = unit;
    }
}
