package cn.flyelf.cache.core.exception;

import cn.flyelf.cache.annotation.CacheConstant;

/**
 * 缓存key类型不匹配
 *
 * @author wujr
 * 2019-12-14
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-14 1.0 新增]
 */
public class CacheKeyMismatchException extends CacheException {
    private static final long serialVersionUID = -960212008399221753L;

    public CacheKeyMismatchException(String keyClass){
        super(null, CacheConstant.RESULT.UNKNOWN, keyClass);
    }
}
