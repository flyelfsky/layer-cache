package cn.flyelf.cache.core.exception;

import cn.flyelf.cache.annotation.CacheConstant;

/**
 * 缓存key不存在
 *
 * @author wujr
 * 2019/12/18
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/18 1.0 新增]
 */
public class CacheNotExistException extends CacheException {
    private static final long serialVersionUID = 90166165350928505L;

    public CacheNotExistException(String layerName, String key){
        super(layerName, CacheConstant.RESULT.NO_KEY, key);
    }
}
