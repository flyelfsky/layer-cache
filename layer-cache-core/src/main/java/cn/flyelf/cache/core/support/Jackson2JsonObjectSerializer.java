package cn.flyelf.cache.core.support;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.serialization.ValidatingObjectInputStream;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;

/**
 * 利用jackson完成对象和json的转换
 *
 * @author wujr
 * 2019/12/16
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/16 1.0 新增]
 */
public class Jackson2JsonObjectSerializer extends DefaultObjectSerializer {
    private ObjectMapper objectMapper;

    public Jackson2JsonObjectSerializer(){
        mapper();
    }

    void setObjectMapper(ObjectMapper objectMapper){
        if (objectMapper == null){
            throw new NullPointerException("ObjectMapper can not be null");
        }
        this.objectMapper = objectMapper;
    }

    private void mapper(){
        objectMapper = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        objectMapper.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL, JsonTypeInfo.As.PROPERTY);
        // 修正：设置日期的格式化，否则无法存储早于1970-01-01的事件的问题
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }

    @Override
    protected <T> ByteBuffer wrap(T object) {
        try {
            return ByteBuffer.wrap(objectMapper.writeValueAsBytes(object));
        }catch (Exception e){
            return super.wrap(object);
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    protected <T> T unwrap(ByteBuffer bytes, Class clz) {
        if (null == bytes){
            return null;
        }
        byte[] value = new byte[bytes.remaining()];
        bytes.get(value);
        try {
            return (T)objectMapper.readValue(value, 0, value.length, clz);
        } catch (IOException e) {
            try {
                ValidatingObjectInputStream is = new ValidatingObjectInputStream(new ByteArrayInputStream(value));
                is.accept(clz).accept("java.*","[Ljava.*");
                return (T) is.readObject();
            }catch (Exception e1){
                if (String.class.isAssignableFrom(clz)) {
                    return (T) new String(value, charset);
                }else{
                    return null;
                }
            }
        }
    }
}
