package cn.flyelf.cache.core.model;

import lombok.Data;

import java.util.Date;

/**
 * 用于测试的模型
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
@Data
public class CacheTestModel {
    private Long id;
    private String name;
    private Integer index;
    private Date date;

    public CacheTestModel(){}

    public CacheTestModel(String content){
        this.name = content;
    }
}
