package cn.flyelf.cache.core;

import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import reactor.core.publisher.Mono;

/**
 * 定义缓存动作接口
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * @param <R>: 缓存处理的结果返回类型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
public interface CacheAction<K, V, R> {
    /**
     * 处理当前缓存层的缓存动作，并且代理到下一个缓存层对应的动作
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @param exchange:当前的缓存交换信息
     * @param chain: 缓存链
     * @return 指示缓存动作什么时候结束
     */
    Mono<CacheResult<R>> cache(CacheExchange<K, V, R> exchange, CacheActionChain chain);

    /**
     * 处理层
     * @author wujr
     * 2019/12/18
     * 变更历史
     * [wujr 2019/12/18 1.0 新增]
     *
     * @return 缓存处理层处理器
     */
    CacheLayerProcessor processor();

    /**
     * 加载数据完毕事件
     * @author wujr
     * 2019/12/19
     * 变更历史
     * [wujr 2019/12/19 1.0 新增]
     *
     * @param exchange: 本次的缓存交换请求
     * @param value: 从数据库加载的内容
     * @return mono
     */
    default Mono<Boolean> onLoadAfter(CacheExchange<K, V, R> exchange, V value){
        return Mono.just(true);
    }
}
