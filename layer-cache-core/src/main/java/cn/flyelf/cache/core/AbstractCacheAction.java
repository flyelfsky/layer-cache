package cn.flyelf.cache.core;

import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.context.CacheContext;
import cn.flyelf.cache.core.exception.CacheException;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.core.util.TypeUtil;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.model.CacheEvent;
import cn.flyelf.cache.core.model.CacheResult;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Collection;

/**
 * 定义基本抽象的缓存动作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存对象类型
 * @param <R>: 动作的返回类型
 * @param <P>: 缓存层的处理器：CacheLayerProcessor
 *
 * @author wujr
 * 2019-12-19
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-19 1.0 新增]
 */
@Slf4j
public abstract class AbstractCacheAction<K, V, R, P extends CacheLayerProcessor> implements CacheAction<K, V, R> {
    protected final P layerProcessor;
    protected final ACTION action;
    protected final String area;
    protected CacheContext cacheContext;

    public AbstractCacheAction(ACTION action, P processor, String area){
        this.action = action;
        this.layerProcessor = processor;
        this.area = area;
    }

    @Override
    public P processor(){
        return this.layerProcessor;
    }

    protected CacheContext initContext(CacheExchange<K, V, R> exchange){
        log.trace("init cache context with exchange: {}", exchange);
        return null;
    }
    protected void destroyContext(){}

    @Override
    public Mono<CacheResult<R>> cache(CacheExchange<K, V, R> exchange, CacheActionChain chain){
        cacheContext = initContext(exchange);
        if (null == cacheContext){
            return chain.cache(exchange);
        }
        CacheEvent event = new CacheEvent(action, processor().name(), area, exchange.getRequest().getKey().toString());
        Mono<CacheResult<R>> result = doAction(exchange);
        result = result.doOnSuccessOrError((r, e) -> {
            destroyContext();
            if (null != e){
                // 本动作执行失败了
                r = CacheResult.exception(e, processor().name());
            }else{
                // 动作执行成功了
                if (action.needSynchronize()){
                    // 需要同步给该缓存层之前的其它缓存
                    chain.synchronize(AbstractCacheAction.this, exchange, r);
                }
            }
            event.stop(r.getResult());
            processor().eventPublisher().publish(event);
        });
        if (action.ignoreResult()){
            log.info("{}缓存，执行动作[{}]，忽略执行结果，继续执行下一个缓存链", processor().name(), action);
            result = result.flatMap(r -> {
                exchange.getResponse().setResult(r);
                return chain.cache(exchange);
            });
        }
        return result.onErrorResume(e -> {
            log.warn("{}缓存，执行动作[{}] 失败了，异常为：{}", processor().name(), action, e.getMessage());
            if (!(e instanceof CacheException)){
                log.warn("异常详情：", e);
            }
            exchange.getResponse().setThrowable(e);
            exchange.getResponse().setResult(CacheResult.exception(e, processor().name()));
            return chain.cache(exchange);
        });
    }

    /**
     * 执行具体的动作，不考虑缓存链
     * @author wujr
     * 2019/12/23
     * 变更历史
     * [wujr 2019/12/23 1.0 新增]
     *
     * @param exchange: 缓存交换
     * @return 缓存结果
     */
    protected abstract Mono<CacheResult<R>> doAction(CacheExchange<K, V, R> exchange);

    @SuppressWarnings("unchecked")
    protected <T> T[] getArrayFromAttachment(CacheExchange exchange, String key){
        Object object = exchange.getRequest().getAttachment(key);
        if (null == object){
            return TypeUtil.emptyArray(exchange.getPolicy().getObject());
        }
        T[] array;
        if (object instanceof Collection){
            Collection<T> keys = (Collection<T>)object;
            if (keys.isEmpty()){
                return TypeUtil.emptyArray(exchange.getPolicy().getObject());
            }
            array = TypeUtil.collection2Array(keys);
        }else{
            array = (T[])object;
        }
        return array;
    }
}
