package cn.flyelf.cache.core.exception;

import cn.flyelf.cache.annotation.CacheConstant;

/**
 * 缓存配置异常
 *
 * @author wujr
 * 2019-12-15
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-15 1.0 新增]
 */
public class CacheConfigException extends CacheException {
    private static final long serialVersionUID = -8064622962705833769L;

    public CacheConfigException(String message) {
        super(null, CacheConstant.RESULT.UNKNOWN, message);
    }
}
