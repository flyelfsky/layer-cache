package cn.flyelf.cache.core.server;

import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheResponse;

/**
 * 默认的缓存交换
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
public class CacheDefaultExchange<K, V, R> implements CacheExchange<K, V, R> {
    private CacheRequest<K, V> request;
    private CacheResponse<R> response;
    private CacheObjectLoader<K, V> loader;
    private CachePolicy policy;

    CacheDefaultExchange(CacheRequest<K, V> request, CacheResponse<R> response, CachePolicy policy, CacheObjectLoader<K, V> loader){
        this.request = request;
        this.response = response;
        this.policy = policy;
        this.loader = loader;
    }
    public CacheDefaultExchange(CacheRequest<K, V> request, CachePolicy policy){
        this(request, new CacheResponse<>(), policy, null);
    }
    @Override
    public CacheRequest<K, V> getRequest() {
        return request;
    }

    @Override
    public CacheResponse<R> getResponse() {
        return response;
    }

    @Override
    public CachePolicy getPolicy() {
        return policy;
    }

    @Override
    public CacheObjectLoader<K, V> getLoader() {
        return loader;
    }
}
