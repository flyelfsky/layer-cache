package cn.flyelf.cache.core;

import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.core.model.CacheResult;
import reactor.core.publisher.Mono;

import java.util.Collection;

/**
 * 定义缓存服务接口
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
public interface Cache<K, V> extends Cloneable{
    /**
     * 获取实际的缓存服务
     * @author wujr
    * 2019-12-29
     * 变更历史
     * [wujr 2019-12-29 1.0 新增]
     *
     * @return 返回实际进行操作的缓存服务
     */
    default Cache actual(){
        return this;
    }
    /**
     * 缓存的名称，具有唯一性
     * @author wujr
    * 2019-12-23
     * 变更历史
     * [wujr 2019-12-23 1.0 新增]
     *
     * @return 缓存名称
     */
    String name();
    /**
     * 获取缓存对象
     * 如果loader不为null，并且所有的缓存层都没有该缓存key，则调用loader进行加载并缓存到所有缓存层中
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @param key: 缓存key
     * @param loader: 缓存对象加载器
     * @return 缓存对象结果
     */
    Mono<CacheResult<V>> get(K key, CacheObjectLoader<K, V> loader);
    /**
     * 获取某个key的内容
     * @author wujr
     * 2019/12/23
     * 变更历史
     * [wujr 2019/12/23 1.0 新增]
     *
     * @param key: 缓存key
     * @return 缓存内容
     */
    default Mono<CacheResult<V>> get(K key){
        return get(key, null);
    }

    /**
     * 设置缓存对象
     * 如果缓存key已经存在，则更新对应的缓存对象
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @param key: 缓存key
     * @param value: 缓存对象
     * @return 缓存结果
     */
    Mono<CacheResult<Boolean>> put(K key, V value);

    /**
     * 删除缓存对象
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @param key: 缓存key
     * @return 缓存结果
     */
    Mono<CacheResult<Long>> delete(K key);
    Mono<CacheResult<Long>> delete(K... keys);
    Mono<CacheResult<Long>> delete(Collection<K> keys);
}
