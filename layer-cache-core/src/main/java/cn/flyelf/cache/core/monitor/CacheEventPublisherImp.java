package cn.flyelf.cache.core.monitor;

import cn.flyelf.cache.core.event.AbstractCacheEventSubscriber;
import cn.flyelf.cache.core.event.CacheEventListener;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.event.CacheEventSource;
import cn.flyelf.cache.core.model.CacheEvent;
import reactor.core.publisher.Flux;

/**
 * 缓存事件发布器的具体实现
 *
 * @author wujr
 * 2019/12/25
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/25 1.0 新增]
 */
public class CacheEventPublisherImp implements CacheEventPublisher {
    private final Flux<CacheEvent> publisher;
    private final CacheEventSource emitter = new CacheEventSource();

    public CacheEventPublisherImp(){
        publisher = Flux.create(sink -> emitter.register(new CacheEventListener() {
            @Override
            public void onCacheEvent(CacheEvent event) {
                sink.next(event);
            }

            @Override
            public void onEventStop() {
                sink.complete();
            }
        }));
    }

    @Override
    public void publish(CacheEvent event) {
        emitter.event(event);
    }

    @Override
    public void subscribe(AbstractCacheEventSubscriber subscriber) {
        publisher.subscribe(subscriber);
    }

    @Override
    public void stop(){
        emitter.stop();
    }
}
