package cn.flyelf.cache.core.context;

import lombok.Data;

/**
 * 缓存动作的上下文
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
@Data
public class CacheContext {
}
