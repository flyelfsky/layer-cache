package cn.flyelf.cache.core.util;

import cn.flyelf.cache.annotation.CacheConstant;

import java.lang.reflect.Array;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * 类型辅助工具
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class TypeUtil {
    private TypeUtil(){}

    public static Class resolverTypeClass(Type type){
        if (type instanceof Class){
            return (Class)type;
        }else if (type instanceof ParameterizedType){
            ParameterizedType t = (ParameterizedType)type;
            Type[] subs = t.getActualTypeArguments();
            return resolverTypeClass(subs[subs.length - 1]);
        }else{
            return type.getClass();
        }
    }

    @SuppressWarnings("unchecked")
    public static Class checkAssignable(Class collectionClass, Class objectClass, Class def){
        if (collectionClass.isAssignableFrom(objectClass)){
            if (objectClass.isAssignableFrom(collectionClass)){
                return def;
            }else{
                return objectClass;
            }
        }
        return null;
    }
    public static Class checkRawType(Class objectClass){
        Class[] collectionClass = {List.class, Set.class, Map.class};
        Class[] defaultClass = {ArrayList.class, HashSet.class, HashMap.class};
        for (int index = 0; index < collectionClass.length; ++ index){
            Class clz = checkAssignable(collectionClass[index], objectClass, defaultClass[index]);
            if (clz != null){
                return clz;
            }
        }
        return objectClass;
    }
    public static Class resolverRawType(Type type){
        if (type instanceof Class){
            return (Class)type;
        }else if (type instanceof ParameterizedType){
            ParameterizedType t = (ParameterizedType)type;
            Type rawType = t.getRawType();
            if (!(rawType instanceof Class)){
                return rawType.getClass();
            }
            Class<?> c = (Class)rawType;
            return checkRawType(c);
        }else{
            return type.getClass();
        }
    }

    public static String resolveCacheType(Type type){
        if (type instanceof ParameterizedType){
            ParameterizedType pt = (ParameterizedType)type;
            Type t = pt.getRawType();
            if (t instanceof Class) {
                Class clz = (Class)t;
                if (List.class.isAssignableFrom(clz)) {
                    return CacheConstant.CACHE_LIST;
                } else if (Set.class.isAssignableFrom(clz)) {
                    return CacheConstant.CACHE_SET;
                } else if (Map.class.isAssignableFrom(clz)) {
                    return CacheConstant.CACHE_MAP;
                }
            }
        }
        return CacheConstant.CACHE_SIMPLE;
    }

    /**
     * 把collection转换为数组
     * @author wujr
    * 2019-12-20
     * 变更历史
     * [wujr 2019-12-20 1.0 新增]
     *
     * @param collection: 容器
     * @param <T>: 实体类型
     * @return 实体数组
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] collection2Array(Collection<T> collection){
        return collection.toArray((T[]) Array.newInstance(collection.iterator().next().getClass(), 0));
    }
    /**
     * 把列表转换为数组
     * @author wujr
     * 2019/12/20
     * 变更历史
     * [wujr 2019/12/20 1.0 新增]
     *
     * @param list: 列表
     * @param <T>: 数据类型
     * @return 实体数组
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] list2Array(List<T> list){
        return list.toArray((T[])Array.newInstance(list.iterator().next().getClass(), 0));
    }
    /**
     * 生成一个空的数组
     * @author wujr
    * 2020-01-04
     * 变更历史
     * [wujr 2020-01-04 1.0 新增]
     *
     * @param clz: 实体类
     * @param <T>: 实体类型
     * @return 空数组
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] emptyArray(Class clz){
        return (T[])Array.newInstance(clz, 0);
    }
    /**
     * 把集合转换为数组
     * @author wujr
    * 2019-12-20
     * 变更历史
     * [wujr 2019-12-20 1.0 新增]
     *
     * @param set: 集合
     * @param <T>: 数据类型
     * @return 实体数组
     */
    @SuppressWarnings("unchecked")
    public static <T> T[] set2Array(Set<T> set){
        return set.toArray((T[])Array.newInstance(set.iterator().next().getClass(), 0));
    }
}
