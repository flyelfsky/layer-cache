package cn.flyelf.cache.core.event;

import cn.flyelf.cache.core.model.CacheEvent;

import java.util.ArrayList;
import java.util.List;

/**
 * 缓存事件源头，用于产生缓存事件，进而触发该事件
 *
 * @author wujr
 * 2019-12-26
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-26 1.0 新增]
 */
public class CacheEventSource {
    private final List<CacheEventListener> listeners = new ArrayList<>(1);

    public void register(CacheEventListener listener){
        listeners.add(listener);
    }

    public void event(CacheEvent event){
        listeners.forEach(listener -> listener.onCacheEvent(event));
    }

    public void stop(){
        listeners.forEach(CacheEventListener::onEventStop);
    }
}
