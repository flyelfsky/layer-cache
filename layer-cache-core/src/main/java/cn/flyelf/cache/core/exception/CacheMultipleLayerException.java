package cn.flyelf.cache.core.exception;

/**
 * 缓存多层异常
 *
 * @author wujr
 * 2019/12/20
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/20 1.0 新增]
 */
public class CacheMultipleLayerException extends CacheException {
    private static final long serialVersionUID = -4352791779120143028L;

    public CacheMultipleLayerException(String object){
        super(null, null, object);
    }
}
