package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.SetCache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import reactor.core.publisher.Mono;

import java.util.Set;

/**
 * 默认的set的容器的缓存服务
 * @param <K>: 缓存key类型
 * @param <V>: 缓存对象类型
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
public class DefaultSetCache<K, V> extends AbstractDefaultCache<K, Set<V>> implements SetCache<K, V> {
    public DefaultSetCache(CacheLayerFactory factory, String[] layers, Class keyClass, Class target, String suffix){
        super(factory, layers, keyClass, target, target, CacheConstant.CACHE_SET, suffix);
    }
    public DefaultSetCache(CacheLayerFactory factory, String[] layers, Class keyClass, Class target){
        this(factory, layers, keyClass, target, null);
    }

    @Override
    public Mono<CacheResult<Set<V>>> get(K key, CacheObjectLoader<K, Set<V>> loader) {
        return get(key, ACTION.GETSET, loader, null);
    }
    @Override
    public Mono<CacheResult<Boolean>> put(K key, Set<V> value) {
        return put(key, value, ACTION.PUTSET);
    }
}
