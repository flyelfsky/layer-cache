package cn.flyelf.cache.core;

import java.util.Set;

/**
 * 缓存的set类型的接口
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-28
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-28 1.0 新增]
 */
public interface SetCache<K, V> extends Cache<K, Set<V>> {
}
