package cn.flyelf.cache.core;

import cn.flyelf.cache.core.model.CacheResult;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * 缓存的list类型的接口
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * @author wujr
 * 2019/12/25
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/25 1.0 新增]
 */
public interface ListCache<K, V> extends Cache<K, List<V>>{
    /**
     * 右边弹出一个实体
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param key: 缓存key
     * @return 最右边的实体，或者为空
     */
    Mono<CacheResult<V>> rpop(K key);

    /**
     * 默认为右边弹出
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param key: 缓存key
     * @return 弹出的实体
     */
    default Mono<CacheResult<V>> pop(K key){
        return rpop(key);
    }
    /**
     * 左边弹出一个实体
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param key: 缓存key
     * @return 最左边的实体，或者空
     */
    Mono<CacheResult<V>> lpop(K key);

    /**
     * 左边压入列表实体
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param key: 缓存key
     * @param value: 实体列表
     * @return 压入结果
     */
    Mono<CacheResult<Boolean>> lpush(K key, List<V> value);
    /**
     * 左边压入实体数组
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param key: 缓存key
     * @param value: 实体数组
     * @return 压入结果
     */
    Mono<CacheResult<Boolean>> lpush(K key, V... value);
    /**
     * 右边压入实体列表
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param key: 缓存key
     * @param value: 实体列表
     * @return 压入结果
     */
    Mono<CacheResult<Boolean>> rpush(K key, List<V> value);
    /**
     * 右边压入实体数组
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param key: 缓存key
     * @param value: 实体数组
     * @return 压入结果
     */
    Mono<CacheResult<Boolean>> rpush(K key, V... value);

    /**
     * 压入内容，默认为从左边压入
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param key: 缓存key
     * @param value: 实体内容
     * @return 结果
     */
    default Mono<CacheResult<Boolean>> push(K key, V... value){
        return lpush(key, value);
    }
    /**
     * 压入内容，默认为从左边压入
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param key: 缓存key
     * @param value: 实体内容
     * @return 结果
     */
    default Mono<CacheResult<Boolean>> push(K key, List<V> value){
        return lpush(key, value);
    }
}
