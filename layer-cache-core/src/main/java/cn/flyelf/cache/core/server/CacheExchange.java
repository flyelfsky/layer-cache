package cn.flyelf.cache.core.server;

import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheResponse;

/**
 * 定义缓存交换接口
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * @param <R>: 缓存结果返回类型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
public interface CacheExchange<K, V, R> {
    /**
     * 获取缓存交换的缓存请求对象
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @return 缓存的请求对象
     */
    CacheRequest<K, V> getRequest();

    /**
     * 获取缓存的交换处理结果
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @return 缓存处理结果
     */
    CacheResponse<R> getResponse();

    /**
     * 缓存的策略
     * @author wujr
     * 2019/12/13
     * 变更历史
     * [wujr 2019/12/13 1.0 新增]
     *
     * @return 缓存策略
     */
    CachePolicy getPolicy();

    /**
     * 缓存对象加载器
     * @author wujr
     * 2019/12/12
     * 变更历史
     * [wujr 2019/12/12 1.0 新增]
     *
     * @return 缓存对象加载器
     */
    CacheObjectLoader<K, V> getLoader();
}
