package cn.flyelf.cache.core.model;

import lombok.Data;

import java.util.HashMap;
import java.util.Map;

/**
 * 定义缓存的请求模型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@Data
public class CacheRequest<K, V> {
    /**
     * 缓存的key
     */
    private K key;
    /**
     * 缓存的值
     */
    private V value;
    /**
     * 缓存值的序列化类
     */
    private Class serializerClass;
    /**
     * 请求的附加参数
     */
    private final Map<String, Object> attachment = new HashMap<>();

    public <T> void addAttachment(String key, T object){
        this.attachment.put(key, object);
    }
    @SuppressWarnings("unchecked")
    public <T> T getAttachment(String key){
        return (T)this.attachment.get(key);
    }
    @SuppressWarnings("unchecked")
    public <T> T getAttachmentDefault(String key, T def){
        return (T)this.attachment.getOrDefault(key, def);
    }
    public void setAttachment(Map<String, Object> attachment){
        if (null != attachment) {
            this.attachment.putAll(attachment);
        }
    }

    public CacheRequest(K key, Class serializerClass){
        this.key = key;
        this.serializerClass = serializerClass;
    }
    public CacheRequest(K key, V value, Class serializerClass){
        this.key = key;
        this.value = value;
        this.serializerClass = serializerClass;
    }

    @SuppressWarnings("unchecked")
    public Class<K> getKeyClass(){
        return (Class<K>)key.getClass();
    }
}
