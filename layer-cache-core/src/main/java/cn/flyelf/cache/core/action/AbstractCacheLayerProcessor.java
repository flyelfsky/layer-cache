package cn.flyelf.cache.core.action;

import cn.flyelf.cache.core.event.CacheEventPublisher;

/**
 * 定义抽象的缓存层处理器
 *
 * @author wujr
 * 2020/1/3
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/3 1.0 新增]
 */
public abstract class AbstractCacheLayerProcessor implements CacheLayerProcessor {
    private final CacheEventPublisher eventPublisher;

    public AbstractCacheLayerProcessor(CacheEventPublisher eventPublisher){
        this.eventPublisher = eventPublisher;
    }
    @Override
    public CacheEventPublisher eventPublisher(){
        return this.eventPublisher;
    }
}
