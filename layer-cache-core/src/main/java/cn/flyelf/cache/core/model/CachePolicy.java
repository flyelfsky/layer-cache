package cn.flyelf.cache.core.model;

import cn.flyelf.cache.core.util.DurationConverter;
import cn.flyelf.cache.annotation.CacheConstant;
import lombok.Data;
import org.apache.commons.lang3.StringUtils;

import java.util.Arrays;
import java.util.concurrent.TimeUnit;

/**
 * 缓存策略模型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@Data
public class CachePolicy {
    /**
     * 存储的最终实体的类
     */
    private Class<?> object;
    /**
     * 缓存的实体类型，默认为：CacheConstant.CACHE_SIMPLE
     */
    private String type;
    /**
     * 缓存为容器时，如果有嵌套容器的，则可以通过该属性进行区分的特征
     */
    private String feature;
    /**
     * 缓存的有效时长
     */
    private Long duration;
    /**
     * 有效时长的时间单位
     */
    private TimeUnit timeUnit;
    /**
     * 该缓存的缓存的层名称
     * 该属性为根据配置自动生成，例如：
     * 某个对象需要在caffeine和redis中进行二级缓存，则那么为：caffeine-redis
     * 根据该名称，能够找到对应的缓存链实例
     */
    private String[] layer;
    /**
     * 数据的存储最大数量
     */
    private Long limit;
    /**
     * 缓存null，默认为false
     */
    private boolean cacheNull = false;
    /**
     * null的缓存时长，默认为60秒
     * 单位：毫秒
     */
    private Long nullExpire;

    @SuppressWarnings("unchecked")
    public <T> Class<T> object(){
        return (Class<T>)this.object;
    }

    public String name(){
        return object.getName() + "-"
                + (StringUtils.isBlank(type) ? CacheConstant.CACHE_SIMPLE : type)
                + "-" + Arrays.toString(layer)
                + (StringUtils.isBlank(feature) ? "" : ("-" + feature));
    }

    /**
     * 设置有效时长
     * @author wujr
    * 2019/12/19
     * 变更历史
     * [wujr 2019/12/19 1.0 新增]
     *
     * @param time: 有效时长字符串
     */
    public void duration(String time){
        if (StringUtils.isBlank(time)){
            return;
        }
        Duration duration = DurationConverter.of(time);
        this.duration = duration.getDuration();
        this.timeUnit = duration.getUnit();
    }
}
