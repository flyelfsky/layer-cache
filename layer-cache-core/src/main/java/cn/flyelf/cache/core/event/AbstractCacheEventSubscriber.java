package cn.flyelf.cache.core.event;

import cn.flyelf.cache.core.model.CacheEvent;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Subscription;
import reactor.core.publisher.BaseSubscriber;


/**
 * 缓存事件的订阅者接口
 *
 * @author wujr
 * 2019/12/25
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/25 1.0 新增]
 */
@Slf4j
public abstract class AbstractCacheEventSubscriber extends BaseSubscriber<CacheEvent> {
    @Override
    protected void hookOnSubscribe(Subscription subscription) {
        request(1);
    }
    /**
     * 接受处理缓存事件
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param event: 缓存事件
     */
    @Override
    protected void hookOnNext(CacheEvent event){
        onCacheEvent(event);
        request(1);
    }

    @Override
    protected void hookOnError(Throwable t){
        log.error("缓存事件订阅发生错误：", t);
    }
    @Override
    protected void hookOnComplete(){
        upstream().cancel();
    }
    /**
     * 取消订阅
     * @author wujr
    * 2019-12-26
     * 变更历史
     * [wujr 2019-12-26 1.0 新增]
     *
     */
    @Override
    protected void hookOnCancel(){
        upstream().cancel();
    }

    /**
     * 缓存事件触发
     * @author wujr
     * 2019/12/25
     * 变更历史
     * [wujr 2019/12/25 1.0 新增]
     *
     * @param event: 缓存事件
     */
    protected abstract void onCacheEvent(CacheEvent event);
}
