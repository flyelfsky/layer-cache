package cn.flyelf.cache.core.model;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import lombok.Data;

/**
 * 缓存事件定义模型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@Data
public class CacheEvent {
    public CacheEvent(ACTION action, String layer, String area, String key){
        this.action = action;
        this.layer = layer;
        this.area = area;
        this.key = key;
        this.start = System.currentTimeMillis();
    }
    public void stop(CacheConstant.RESULT result){
        this.stop = System.currentTimeMillis();
        this.result = result;
    }
    public long used(){
        return stop - start;
    }
    /**
     * 开始时间
     */
    private Long start;
    /**
     * 停止时间
     */
    private Long stop;
    /**
     * 事件动作
     */
    private ACTION action;
    /**
     * 缓存操作的key
     */
    private String key;
    /**
     * 缓存的层
     */
    private String layer;
    /**
     * 缓存区域
     */
    private String area;
    /**
     * 缓存处理结果
     */
    private CacheConstant.RESULT result;
}
