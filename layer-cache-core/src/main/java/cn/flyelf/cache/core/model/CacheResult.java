package cn.flyelf.cache.core.model;

import cn.flyelf.cache.core.exception.CacheException;
import cn.flyelf.cache.annotation.CacheConstant;
import lombok.Data;

/**
 * 缓存结果封装模型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@Data
public class CacheResult<V> {
    /**
     * 结果对象
     */
    private V value;
    /**
     * 当前的处理层名称
     */
    private String layer;
    /**
     * 缓存处理结果
     */
    private CacheConstant.RESULT result = CacheConstant.RESULT.OK;

    public boolean isSuccess(){
        return CacheConstant.RESULT.OK.equals(result);
    }
    public CacheResult(){}

    public CacheResult(CacheConstant.RESULT result, String layer, V value){
        this.result = result;
        this.layer = layer;
        this.value = value;
    }
    public CacheResult(CacheConstant.RESULT result, String layer){
        this(result, layer, null);
    }

    public static <V> CacheResult<V> success(String layer, V value){
        return new CacheResult<>(CacheConstant.RESULT.OK, layer, value);
    }
    public static <V> CacheResult<V> success(String layer){
        return new CacheResult<>(CacheConstant.RESULT.OK, layer, null);
    }

    public static <V> CacheResult<V> key(String layer){
        return new CacheResult<>(CacheConstant.RESULT.NO_KEY, layer);
    }

    public static <V> CacheResult<V> exception(Throwable e, String layerName){
        if (e instanceof CacheException){
            CacheException exception = (CacheException)e;
            return new CacheResult<>(exception.getResult(), exception.getLayerName());
        }else{
            return new CacheResult<>(CacheConstant.RESULT.UNKNOWN, layerName);
        }
    }
}
