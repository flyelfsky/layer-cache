package cn.flyelf.cache.core.model;

import cn.flyelf.cache.core.support.Jackson2JsonObjectSerializer;
import cn.flyelf.cache.core.support.ObjectSerializer;
import cn.flyelf.cache.core.support.StringObjectSerializer;

/**
 * 缓存的序列化常量定义
 *
 * @author wujr
 * 2020-01-12
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-12 1.0 新增]
 */
public class CacheSerializer {
    /**
     * 默认的序列化器
     */
    public static final ObjectSerializer DEFAULT = new Jackson2JsonObjectSerializer();
    /**
     * 字符串的序列化器
     */
    public static final ObjectSerializer UTF8 = new StringObjectSerializer();
}
