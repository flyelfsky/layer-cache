package cn.flyelf.cache.core.util;

import cn.flyelf.cache.core.model.Duration;
import org.apache.commons.lang3.StringUtils;

import java.time.format.DateTimeParseException;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;
import java.util.regex.Pattern;

/**
 * 时长转换器
 *
 * @author wujr
 * 2019/12/19
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/19 1.0 新增]
 */
public class DurationConverter {
    private DurationConverter(){}

    private static final ConcurrentHashMap<String, TimeUnit> TIME_UNIT = new ConcurrentHashMap<>(6);
    static {
        TIME_UNIT.put("s", TimeUnit.SECONDS);
        TIME_UNIT.put("m", TimeUnit.MINUTES);
        TIME_UNIT.put("h", TimeUnit.HOURS);
        TIME_UNIT.put("ms", TimeUnit.MILLISECONDS);
        TIME_UNIT.put("ns", TimeUnit.NANOSECONDS);
        TIME_UNIT.put("d", TimeUnit.DAYS);
        TIME_UNIT.put("us", TimeUnit.MICROSECONDS);
    }
    public static Duration of(String time){
        String regEx = "[^0-9]+";
        Pattern pattern = Pattern.compile(regEx);
        //用定义好的正则表达式拆分字符串，，把字符串中的数字留出来
        String[] cs = pattern.split(time);
        if (cs.length == 0 || StringUtils.isBlank(cs[0])){
            throw new DateTimeParseException("time cannot be parsed to a Duration", time, 0);
        }

        long duration = Long.parseLong(cs[0]);
        String unit = time.substring(cs[0].length());
        TimeUnit timeUnit = TIME_UNIT.getOrDefault(unit, TimeUnit.SECONDS);
        return new Duration(duration, timeUnit);
    }
}
