package cn.flyelf.cache.core.server;

import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheResponse;

/**
 * 获取操作的交换
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
public class CacheGetExchange<K, V> extends CacheDefaultExchange<K, V, V> {
    public CacheGetExchange(K key, Class serializeClass, CachePolicy policy, CacheObjectLoader<K, V> loader){
        super(new CacheRequest<>(key, serializeClass), new CacheResponse<>(), policy, loader);
    }
    public CacheGetExchange(K key, Class serializeClass, CachePolicy policy){
        this(key, serializeClass, policy, null);
    }
}
