package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.core.exception.CacheKeyMismatchException;
import cn.flyelf.cache.core.exception.UnsupportedCacheObjectException;
import cn.flyelf.cache.core.server.*;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Collection;
import java.util.Map;

/**
 * 默认的缓存服务实现
 * @param <K>: 缓存key类型
 * @param <V>: 缓存对象类型
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
@Slf4j
public class AbstractDefaultCache<K, V> implements Cache<K, V> {
    private final Class keyClass;
    /**
     * 实际缓存的实体的类
     */
    final Class serializeClass;
    protected final CachePolicy policy;
    /**
     * 缓存层工厂
     */
    final CacheLayerFactory factory;

    AbstractDefaultCache(CacheLayerFactory factory, String[] layers,
                         Class keyClass, Class targetClass, Class serializeClass, String type, String suffix){
        if (null == factory){
            throw new NullPointerException();
        }
        this.factory = factory;
        this.keyClass = keyClass;
        this.serializeClass = serializeClass;
        this.policy = this.factory.findPolicy(layers, targetClass, type, suffix);
        if (null == this.policy){
            // 不支持的缓存对象
            log.error("不支持的存储对象：{}", targetClass.getName());
            throw new UnsupportedCacheObjectException(targetClass.getTypeName());
        }
    }

    @Override
    public String name(){
        return policy.name();
    }
    @Override
    public Mono<CacheResult<V>> get(K key, CacheObjectLoader<K, V> loader) {
        return get(key, ACTION.GET, loader, null);
    }
    Mono<CacheResult<V>> get(K key, ACTION getAction, CacheObjectLoader<K, V> loader, Map<String, Object> attachment){
        checkKey(key);
        CacheActionChain chain = factory.buildActionChain(getAction, policy.getLayer());
        CacheExchange<K, V, V> exchange = new CacheGetExchange<>(key, serializeClass, policy, loader);
        exchange.getRequest().setAttachment(attachment);
        return chain.cache(exchange);
    }

    @Override
    public Mono<CacheResult<Boolean>> put(K key, V value) {
        return put(key, value, ACTION.PUT);
    }
    Mono<CacheResult<Boolean>> put(K key, V value, ACTION putAction) {
        checkKey(key);
        CacheActionChain chain = factory.buildActionChain(putAction, policy.getLayer());
        CacheExchange<K, V, Boolean> exchange = new CachePutExchange<>(key, value, serializeClass, policy);
        return chain.cache(exchange);
    }

    @Override
    public Mono<CacheResult<Long>> delete(K key) {
        checkKey(key);
        CacheActionChain chain = factory.buildActionChain(ACTION.DELETE, policy.getLayer());
        CacheExchange<K, V, Long> exchange = new CacheDefaultExchange<>(new CacheRequest<>(key, serializeClass), policy);
        return chain.cache(exchange);
    }
    @SafeVarargs
    @Override
    public final Mono<CacheResult<Long>> delete(K... keys){
        if (null == keys || keys.length == 0){
            log.warn("删除缓存，同时删除多个key，不允许key为空");
            return Mono.error(new NullPointerException());
        }
        return remove(keys[0], keys);
    }
    @Override
    public Mono<CacheResult<Long>> delete(Collection<K> keys){
        if (null == keys || keys.isEmpty()){
            log.warn("删除缓存，同时删除多个key，不允许key为空");
            return Mono.error(new NullPointerException());
        }
        return remove(keys.iterator().next(), keys);
    }
    private Mono<CacheResult<Long>> remove(K key, Object keys){
        CacheActionChain chain = factory.buildActionChain(ACTION.DELETE, policy.getLayer());
        CacheExchange<K, V, Long> exchange = new CacheDefaultExchange<>(new CacheRequest<>(key, serializeClass), policy);
        exchange.getRequest().addAttachment(CacheConstant.ATTACH_KEY, keys);
        return chain.cache(exchange);
    }
    void checkKey(K key){
        if (null == key){
            throw new NullPointerException();
        }
        if (!key.getClass().isAssignableFrom(keyClass)){
            throw new CacheKeyMismatchException(key.getClass().getName());
        }
    }
}
