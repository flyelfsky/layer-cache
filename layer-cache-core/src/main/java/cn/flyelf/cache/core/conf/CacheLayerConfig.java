package cn.flyelf.cache.core.conf;

import lombok.Data;

/**
 * 缓存层的配置参数
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@Data
public class CacheLayerConfig {
    /**
     * 值序列化的策略
     */
    private String valueSerializer;
    /**
     * 键序列化的策略
     */
    private String keySerializer;
    /**
     * 每个缓存实例的最大元素的全局配置，注意是每个缓存实例的限制，而不是全部
     */
    private Long limit;
    /**
     * 以毫秒为单位指定超时时间的全局配，默认为：null，表示不限制超时时间
     */
    private Long expireAfterWriteInMillis;
    /**
     * 执行指令的超时时间
     * 默认：60毫秒
     */
    private Long commandTimeout = 60L;
    /**
     * 关闭的超时时间
     * 默认：100毫秒
     */
    private Long shutdownTimeout = 100L;
    /**
     * 连接串
     */
    private String[] uri;
    /**
     * 连接池的配置
     */
    private Pool pool;
    /**
     * 资源配置
     */
    private Resources resources;

    /**
     * 连接池的参数
     */
    @Data
    public static class Pool{
        Integer minIdle;
        Integer maxIdle;
        Integer maxActive;
        /**
         * 最大等待时长，毫秒
         */
        Long maxWait = -1L;
    }

    @Data
    public static class Resources{
        Integer ioThread;
        Integer computeThread;
    }
}
