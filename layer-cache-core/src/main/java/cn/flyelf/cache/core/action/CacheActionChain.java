package cn.flyelf.cache.core.action;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import reactor.core.publisher.Mono;

/**
 * 缓存动作代理链接口定义
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
public interface CacheActionChain {
    /**
     * 代理缓存动作链的下一个缓存层的动作
     * @param exchange 当前的缓存交换信息
     * @param <K>: 缓存key类型
     * @param <V>: 缓存内容类型
     * @param <R>: 缓存动作处理结果类型
     * @return 用于指示当请求处理何时结束
     */
    <K, V, R> Mono<CacheResult<R>> cache(CacheExchange<K, V, R> exchange);

    /**
     * 同步缓存执行结果
     * @author wujr
     * 2019-12-26
     * 变更历史
     * [wujr 2019-12-26 1.0 新增]
     *
     * @param action: 当前的执行成功的缓存层的动作
     * @param exchange: 缓存交换
     * @param result: 执行结果
     * @param <K>: 缓存key类型
     * @param <V>: 缓存内容类型
     * @param <R>: 缓存动作处理结果类型
     */
    <K, V, R> void synchronize(CacheAction<K, V, R> action, CacheExchange<K, V, R> exchange, CacheResult<R> result);

    /**
     * 判断该缓存动作是否是缓存链的最后一个
     * @author wujr
     * 2019/12/13
     * 变更历史
     * [wujr 2019/12/13 1.0 新增]
     *
     * @return true表示最后一个
     */
    boolean last();
}
