package cn.flyelf.cache.core.model;

import lombok.Data;

/**
 * 缓存动作响应模型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@Data
public class CacheResponse<V> {
    /**
     * 缓存处理结果
     */
    private CacheResult<V> result;
    /**
     * 处理异常
     */
    private Throwable throwable;

    public CacheResponse(){
        this.result = new CacheResult<>();
    }
}
