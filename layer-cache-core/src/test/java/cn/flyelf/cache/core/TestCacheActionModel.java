package cn.flyelf.cache.core;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.context.CacheContext;
import cn.flyelf.cache.core.exception.CacheException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import reactor.core.publisher.Mono;

/**
 * 用于测试的缓存动作
 *
 * @author wujr
 * 2020-01-06
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-06 1.0 新增]
 */
public class TestCacheActionModel<K, V, R, P extends CacheLayerProcessor> extends AbstractCacheAction<K, V, R, P> {
    TestCacheActionModel(ACTION action, P processor){
        super(action, processor, "default");
    }
    @Override
    protected CacheContext initContext(CacheExchange<K, V, R> exchange){
        if (null == exchange){
            return super.initContext(exchange);
        }
        return new CacheContext();
    }
    @Override
    protected Mono<CacheResult<R>> doAction(CacheExchange<K, V, R> exchange) {
        int testType = exchange.getRequest().getAttachment("testType");
        if (1 == testType){
            return Mono.just(CacheResult.success("test", exchange.getRequest().getAttachment("result")));
        }else if (2 == testType){
            return Mono.error(new CacheException("test", CacheConstant.RESULT.UNKNOWN, "error"));
        }else if (3 == testType){
            return Mono.error(new Exception("test"));
        }
        return null;
    }

    @Override
    public Mono<CacheResult<R>> cache(CacheExchange<K, V, R> exchange, CacheActionChain chain) {
        return super.cache(exchange, chain);
    }
}
