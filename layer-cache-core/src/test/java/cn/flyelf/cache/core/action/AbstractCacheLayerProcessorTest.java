package cn.flyelf.cache.core.action;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheConfigException;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import org.junit.Assert;
import org.junit.Test;

/**
 * AbstractCacheLayerProcessor的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class AbstractCacheLayerProcessorTest {
    private static CacheEventPublisher publisher = new CacheEventPublisherImp();
    static class TestCacheLayerProcessor extends AbstractCacheLayerProcessor{
        TestCacheLayerProcessor(){
            super(publisher);
        }

        @Override
        public CacheAction action(ACTION action, String area) {
            return null;
        }
    }
    static class TestProcessor extends AbstractCacheLayerProcessor{
        TestProcessor(){
            super(publisher);
        }
        @Override
        public CacheAction action(ACTION action, String area) {
            return null;
        }
    }
    private static TestCacheLayerProcessor processor = new TestCacheLayerProcessor();
    @Test
    public void testPublisher(){
        CacheEventPublisher result = processor.eventPublisher();
        Assert.assertEquals(publisher, result);
    }
    @Test
    public void testName(){
        String name = processor.name();
        Assert.assertEquals("test", name);
    }
    @Test(expected = CacheConfigException.class)
    public void testNameInvalid(){
        TestProcessor processor = new TestProcessor();
        processor.name();
    }

    @Test
    public void testDefaultAction(){
        CacheAction action = processor.action(ACTION.POP);
        Assert.assertNull(action);
    }
}
