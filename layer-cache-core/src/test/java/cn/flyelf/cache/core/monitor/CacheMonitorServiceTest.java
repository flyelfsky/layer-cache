package cn.flyelf.cache.core.monitor;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.conf.CacheGlobalConfig;
import cn.flyelf.cache.core.model.CacheEvent;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.Field;
import java.util.concurrent.ConcurrentSkipListSet;

/**
 * CacheMonitorService的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class CacheMonitorServiceTest {
    @Test
    public void testOnCacheEvent(){
        CacheGlobalConfig globalConfig = Mockito.mock(CacheGlobalConfig.class);
        Mockito.when(globalConfig.getCacheMonitorInterval()).thenReturn(300L);
        CacheMonitorService monitor = new CacheMonitorService(globalConfig);

        CacheEvent event = new CacheEvent(ACTION.POP, "test", "default", "key");
        event.stop(CacheConstant.RESULT.OK);
        monitor.onCacheEvent(event);
        event.stop(CacheConstant.RESULT.NO_KEY);
        monitor.onCacheEvent(event);
        Assert.assertTrue(true);
    }
    @Test
    public void testPrintEmpty(){
        CacheGlobalConfig globalConfig = Mockito.mock(CacheGlobalConfig.class);
        Mockito.when(globalConfig.getCacheMonitorInterval()).thenReturn(300L);
        CacheMonitorService monitor = new CacheMonitorService(globalConfig);
        monitor.printCacheStat();
        Assert.assertTrue(true);
    }
    @Test
    public void testPrint(){
        CacheGlobalConfig globalConfig = Mockito.mock(CacheGlobalConfig.class);
        Mockito.when(globalConfig.getCacheMonitorInterval()).thenReturn(300L);
        CacheMonitorService monitor = new CacheMonitorService(globalConfig);
        CacheEvent event = new CacheEvent(ACTION.POP, "test", "default", "key");
        event.stop(CacheConstant.RESULT.OK);
        monitor.onCacheEvent(event);
        monitor.printCacheStat();
        Assert.assertTrue(true);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPrintWithNull() throws Exception{
        CacheGlobalConfig globalConfig = Mockito.mock(CacheGlobalConfig.class);
        Mockito.when(globalConfig.getCacheMonitorInterval()).thenReturn(300L);
        CacheMonitorService monitor = new CacheMonitorService(globalConfig);
        CacheEvent event = new CacheEvent(ACTION.POP, "test", "default", "key");
        event.stop(CacheConstant.RESULT.OK);
        monitor.onCacheEvent(event);
        Field field = FieldUtils.getField(CacheMonitorService.class, "cacheKeys", true);
        ConcurrentSkipListSet<String> cacheKeys = (ConcurrentSkipListSet<String>)field.get(monitor);
        cacheKeys.add("test");
        monitor.printCacheStat();
        Assert.assertTrue(true);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPrintWithNullAndEmpty() throws Exception{
        CacheGlobalConfig globalConfig = Mockito.mock(CacheGlobalConfig.class);
        Mockito.when(globalConfig.getCacheMonitorInterval()).thenReturn(300L);
        CacheMonitorService monitor = new CacheMonitorService(globalConfig);

        Field field = FieldUtils.getField(CacheMonitorService.class, "cacheKeys", true);
        ConcurrentSkipListSet<String> cacheKeys = (ConcurrentSkipListSet<String>)field.get(monitor);
        cacheKeys.add("test");
        cacheKeys.add("print");
        monitor.printCacheStat();
        Assert.assertTrue(true);
    }
}
