package cn.flyelf.cache.core.server;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * CacheDefaultExchange的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class CacheDefaultExchangeTest {
    @Test
    @SuppressWarnings("unchecked")
    public void testGetLoader(){
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        CacheDefaultExchange exchange = new CacheDefaultExchange(null, null, null, loader);
        CacheObjectLoader result = exchange.getLoader();
        Assert.assertEquals(loader, result);
    }
}
