package cn.flyelf.cache.core.support;

import cn.flyelf.cache.core.exception.CacheException;
import cn.flyelf.cache.core.model.CacheSerializer;
import cn.flyelf.cache.core.model.CacheTestModel;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.nio.ByteBuffer;

/**
 * jackson的序列化测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class Jackson2JsonObjectSerializerTest {
    private static Jackson2JsonObjectSerializer serializer = new Jackson2JsonObjectSerializer();

    @Test(expected = NullPointerException.class)
    public void testSetMapperExp(){
        serializer.setObjectMapper(null);
    }
    @Test
    public void testSetMapper(){
        Jackson2JsonObjectSerializer serializer = new Jackson2JsonObjectSerializer();
        serializer.setObjectMapper(new ObjectMapper());
        Assert.assertTrue(true);
    }

    @Test
    public void testUnwrapNull(){
        CacheTestModel result = serializer.unwrap(null, CacheTestModel.class);
        Assert.assertNull(result);
    }
    @Test
    public void testWrap(){
        CacheTestModel model = new CacheTestModel();
        model.setId(1L);
        ByteBuffer buffer = serializer.wrap(model);
        Assert.assertNotNull(buffer);
        CacheTestModel result = serializer.unwrap(buffer, CacheTestModel.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(1L, result.getId().longValue());
    }
    @Test
    public void testUnwrapLongNull(){
        CacheTestModel model = new CacheTestModel();
        model.setId(1L);
        ByteBuffer buffer = serializer.wrap(model);
        Assert.assertNotNull(buffer);
        Long result = serializer.unwrap(buffer, Long.class);
        Assert.assertNull(result);
    }
    @Test
    public void testUnwrapString(){
        CacheTestModel model = new CacheTestModel();
        model.setId(1L);
        ByteBuffer buffer = serializer.wrap(model);
        Assert.assertNotNull(buffer);
        String result = serializer.unwrap(buffer, String.class);
        Assert.assertNotNull(result);
    }
    @Test
    public void testWrapExp() throws Exception{
        CacheException model = new CacheException("layer", null, "test");
        ObjectMapper mapper = Mockito.mock(ObjectMapper.class);
        JsonProcessingException exp = Mockito.mock(JsonProcessingException.class);
        Mockito.when(mapper.writeValueAsBytes(Mockito.any())).thenThrow(exp);
        Jackson2JsonObjectSerializer serializer = (Jackson2JsonObjectSerializer) CacheSerializer.DEFAULT;
        serializer.setObjectMapper(mapper);
        ByteBuffer buffer = serializer.wrap(model);
        Assert.assertNotNull(buffer);
        DefaultObjectSerializer def = new DefaultObjectSerializer();
        CacheException result = def.decode(buffer, CacheException.class);
        Assert.assertNotNull(result);
        Assert.assertEquals("test", result.getMessage());
    }
    @Test
    public void testUnwrapExp(){
        CacheException model = new CacheException("layer", null, "test");
        DefaultObjectSerializer def = new DefaultObjectSerializer();
        ByteBuffer buffer = def.encode(model);
        Assert.assertNotNull(buffer);
        CacheException result = serializer.decode(buffer, CacheException.class);
        Assert.assertNotNull(result);
        Assert.assertEquals("test", result.getMessage());
    }
}
