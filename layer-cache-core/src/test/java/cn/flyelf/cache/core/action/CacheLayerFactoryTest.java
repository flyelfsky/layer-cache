package cn.flyelf.cache.core.action;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.exception.CacheActionNotFoundException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.penetration.PenetrateCache;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

/**
 * CacheLayerFactory的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class CacheLayerFactoryTest {
    private static PenetrateCache penetrateCache = Mockito.mock(PenetrateCache.class);
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static List<CachePolicy> policies = new ArrayList<>(2);

    @BeforeClass
    public static void setUp(){
        factory.setPenetration(penetrateCache);

        CachePolicy policy = new CachePolicy();
        policy.setLayer(new String[]{"test"});
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        policies.add(policy);

        policy = new CachePolicy();
        policy.setLayer(new String[]{"test"});
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_LIST);
        policies.add(policy);
    }

    @Test
    public void testSetPenetration(){
        factory.setPenetration(penetrateCache);
        Assert.assertTrue(true);
    }

    @Test
    public void testAddPolicy(){
        CacheLayerFactory factory = new CacheLayerFactory();
        CachePolicy policy = policies.get(0);
        CachePolicy result = factory.addPolicy(policy);
        Assert.assertEquals(policy, result);

        CachePolicy object = new CachePolicy();
        object.setLayer(new String[]{"test"});
        object.setObject(CacheTestModel.class);
        object.setType(CacheConstant.CACHE_SIMPLE);
        result = factory.addPolicy(object);
        Assert.assertEquals(policy, result);
    }

    @Test
    public void testSetPolicy(){
        factory.setPolicy(policies);
        Assert.assertTrue(true);
    }

    @Test
    public void testFindPolicy(){
        factory.setPolicy(policies);
        CachePolicy result = factory.findPolicy(new String[]{"test"}, CacheTestModel.class, CacheConstant.CACHE_SIMPLE, null);
        Assert.assertEquals(policies.get(0), result);

        result = factory.findPolicy(new String[]{"test"}, CacheTestModel.class, null, null);
        Assert.assertEquals(policies.get(0), result);

        result = factory.findPolicy(new String[]{"test"}, CacheTestModel.class, CacheConstant.CACHE_SIMPLE, "test");
        Assert.assertNull(result);
    }

    @Test
    public void testSetLayer(){
        List<CacheLayerProcessor> processors = new ArrayList<>(1);
        CacheLayerProcessor processor = Mockito.mock(CacheLayerProcessor.class);
        Mockito.when(processor.name()).thenReturn("test");
        processors.add(processor);
        factory.setLayers(processors);
        Assert.assertTrue(true);
    }

    @Test(expected = CacheActionNotFoundException.class)
    public void testBuildActionChainEmpty(){
        List<CacheLayerProcessor> processors = new ArrayList<>(1);
        CacheLayerProcessor processor = Mockito.mock(CacheLayerProcessor.class);
        Mockito.when(processor.name()).thenReturn("test");
        processors.add(processor);
        factory.setLayers(processors);

        factory.buildActionChain(ACTION.POP, new String[]{"a"});
    }
    @Test
    public void testBuildActionChain(){
        List<CacheLayerProcessor> processors = new ArrayList<>(1);
        CacheLayerProcessor processor = Mockito.mock(CacheLayerProcessor.class);
        Mockito.when(processor.name()).thenReturn("test");
        processors.add(processor);
        factory.setLayers(processors);
        factory.setPenetration(penetrateCache);

        CacheActionChain result = factory.buildActionChain(ACTION.POP, new String[]{"test"});
        Assert.assertTrue(result instanceof DefaultCacheActionChain);
    }
}
