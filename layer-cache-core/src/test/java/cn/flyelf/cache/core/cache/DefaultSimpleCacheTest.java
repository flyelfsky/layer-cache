package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheTestModel;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * 默认的简单对象缓存测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class DefaultSimpleCacheTest {
    @Test
    public void testConstructor(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        new DefaultSimpleCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Assert.assertTrue(true);
    }
    @Test
    public void testConstructorWithSuffix(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        new DefaultSimpleCache(factory, policy.getLayer(), String.class, CacheTestModel.class, "test");
        Assert.assertTrue(true);
    }
}
