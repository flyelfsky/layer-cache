package cn.flyelf.cache.core.monitor;

import cn.flyelf.cache.core.conf.CacheGlobalConfig;
import cn.flyelf.cache.core.model.CacheEvent;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * AbstractCacheMonitor的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class AbstractCacheMonitorTest {
    @Test
    public void test(){
        CacheGlobalConfig globalConfig = Mockito.mock(CacheGlobalConfig.class);
        AbstractCacheMonitor monitor = new AbstractCacheMonitor(globalConfig){
            @Override
            protected void onCacheEvent(CacheEvent event) {
            }
        };
        Assert.assertNotNull(monitor);
        CacheGlobalConfig result = monitor.globalConfig();
        Assert.assertEquals(globalConfig, result);
    }
}
