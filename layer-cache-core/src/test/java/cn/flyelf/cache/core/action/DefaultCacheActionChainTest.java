package cn.flyelf.cache.core.action;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheResponse;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.penetration.PenetrateCache;
import cn.flyelf.cache.penetration.caffeine.PenetrateCaffeineCache;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.penetration.model.PenetrateLock;
import cn.flyelf.cache.penetration.model.PenetrateLockWrapper;
import org.joor.Reflect;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * DefaultCacheActionChain的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class DefaultCacheActionChainTest {
    private static List<CacheAction> actions = new ArrayList<>(2);
    private static PenetrateCache penetrateCache = new PenetrateCaffeineCache(new CachePenetrationConfig());

    @BeforeClass
    @SuppressWarnings("unchecked")
    public static void setUp(){
        CacheAction action = Mockito.mock(CacheAction.class);
        Mockito.when(action.onLoadAfter(Mockito.any(), Mockito.any())).thenReturn(Mono.just(true));
        actions.add(action);
        action = Mockito.mock(CacheAction.class);
        Mockito.when(action.onLoadAfter(Mockito.any(), Mockito.any())).thenReturn(Mono.just(true));
        actions.add(action);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testSynchronizeNoResult(){
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, penetrateCache);
        chain.synchronize(actions.get(0), exchange, null);
        Assert.assertTrue(true);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testSynchronizeFailure(){
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, penetrateCache);
        CacheResult result = new CacheResult();
        result.setResult(CacheConstant.RESULT.UNKNOWN);
        chain.synchronize(actions.get(0), exchange, result);
        Assert.assertTrue(true);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testSynchronizeEmpty(){
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, penetrateCache);
        CacheResult result = new CacheResult();
        result.setResult(CacheConstant.RESULT.OK);
        chain.synchronize(actions.get(0), exchange, result);
        Assert.assertTrue(true);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testSynchronizeNoActions(){
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, new ArrayList<>(), penetrateCache);
        CacheResult result = new CacheResult();
        result.setResult(CacheConstant.RESULT.OK);
        chain.synchronize(actions.get(0), exchange, result);
        Assert.assertTrue(true);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testSynchronize(){
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, penetrateCache);
        CacheResult result = new CacheResult();
        result.setResult(CacheConstant.RESULT.OK);
        chain.synchronize(actions.get(1), exchange, result);
        Assert.assertTrue(true);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPenetrateLoadOwner(){
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(1L);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Reflect reflect = Reflect.on(chain).call("penetrateLoad", wrapper, exchange);
        Assert.assertEquals(1L, (long)reflect.get());
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPenetrateLoadCurrent(){
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(false);
        Mockito.when(wrapper.isCurrent()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(1L);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Reflect reflect = Reflect.on(chain).call("penetrateLoad", wrapper, exchange);
        Assert.assertEquals(1L, (long)reflect.get());
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPenetrateLoadNotAwait() throws Exception{
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(false);
        Mockito.when(wrapper.isCurrent()).thenReturn(false);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(1L);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Reflect reflect = Reflect.on(chain).call("penetrateLoad", wrapper, exchange);
        Assert.assertEquals(1L, (long)reflect.get());
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPenetrateLoadAwaitInterrupted() throws Exception{
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(false);
        Mockito.when(wrapper.isCurrent()).thenReturn(false);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenThrow(new InterruptedException());
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(1L);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Reflect reflect = Reflect.on(chain).call("penetrateLoad", wrapper, exchange);
        Assert.assertEquals(1L, (long)reflect.get());
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPenetrateLoadSuccess() throws Exception{
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(false);
        Mockito.when(wrapper.isCurrent()).thenReturn(false);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(true);
        Mockito.when(lock.isSuccess()).thenReturn(true);
        Mockito.when(lock.getValue()).thenReturn(1L);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(1L);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Reflect reflect = Reflect.on(chain).call("penetrateLoad", wrapper, exchange);
        Assert.assertEquals(1L, (long)reflect.get());
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPenetrateLoadLoop() throws Exception{
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(false);
        Mockito.when(wrapper.isCurrent()).thenReturn(false);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(10L)).thenReturn(true);
        Mockito.when(lock.await(20L)).thenReturn(false);
        Mockito.when(lock.isSuccess()).thenReturn(false);
        Mockito.when(lock.getValue()).thenReturn(1L);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        PenetrateCache cache = new PenetrateCache() {
            private int index = 0;
            @Override
            public Long timeout() {
                ++ index;
                if (index == 1){
                    return 10L;
                }else {
                    return 20L;
                }
            }
            @Override
            public long expire() {
                return 0;
            }
            @Override
            public <K> void put(K key, Object value, Long expire) {
            }

            @Override
            public <K> Object get(K key) {
                return null;
            }
            @Override
            public boolean isNull(Object object) {
                return false;
            }
            @Override
            public <K> void getLocker(K key, PenetrateLockWrapper locker) {
            }
            @Override
            public void releaseLocker(PenetrateLockWrapper locker) {
            }
        };
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(1L);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Reflect reflect = Reflect.on(chain).call("penetrateLoad", wrapper, exchange);
        Assert.assertEquals(1L, (long)reflect.get());
    }

    @Test
    public void testLast(){
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, penetrateCache);
        boolean b = chain.last();
        Assert.assertFalse(b);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testLoadNotOwnerNotCurrent() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(false);
        Mockito.when(wrapper.isCurrent()).thenReturn(false);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(1L);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        Assert.assertNotNull(request);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue().equals(1L)).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadNotOwnerNotCurrentAndNull() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(false);
        Mockito.when(wrapper.isCurrent()).thenReturn(false);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheResponse response = new CacheResponse();
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(null);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Mockito.when(exchange.getResponse()).thenReturn(response);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadEnableNull() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(true);
        Mockito.when(wrapper.isCurrent()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(null);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        CacheResponse response = new CacheResponse();
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(exchange.getPolicy()).thenReturn(policy);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        Assert.assertNotNull(request);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadEnableEmptyList() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(true);
        Mockito.when(wrapper.isCurrent()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(new ArrayList());
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        CacheResponse response = new CacheResponse();
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(exchange.getPolicy()).thenReturn(policy);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        Assert.assertNotNull(request);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadNoEnableNull() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(false);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(true);
        Mockito.when(wrapper.isCurrent()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(null);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        CacheResponse response = new CacheResponse();
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(exchange.getPolicy()).thenReturn(policy);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadEnableHasList() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(true);
        Mockito.when(wrapper.isCurrent()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        List list = new ArrayList(1);
        list.add(1L);
        Mockito.when(loader.load(Mockito.any())).thenReturn(list);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(exchange.getPolicy()).thenReturn(policy);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue() instanceof ArrayList && 1 == ((ArrayList) r.getValue()).size()).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadNoEnableEmptyList() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(false);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(true);
        Mockito.when(wrapper.isCurrent()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(new ArrayList());
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        CacheResponse response = new CacheResponse();
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(exchange.getPolicy()).thenReturn(policy);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadEnableObject() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(true);
        Mockito.when(wrapper.isCurrent()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(1L);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(exchange.getPolicy()).thenReturn(policy);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue().equals(1L)).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadEnableNullNoOwner() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(false);
        Mockito.when(wrapper.isCurrent()).thenReturn(true);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheResponse response = new CacheResponse();
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(null);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(exchange.getPolicy()).thenReturn(policy);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLoadEnableNullNoCurrent() throws Exception{
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        PenetrateLockWrapper wrapper = Mockito.mock(PenetrateLockWrapper.class);
        Mockito.when(wrapper.isOwner()).thenReturn(true);
        Mockito.when(wrapper.isCurrent()).thenReturn(false);
        PenetrateLock lock = Mockito.mock(PenetrateLock.class);
        Mockito.when(lock.await(Mockito.anyLong())).thenReturn(false);
        Mockito.when(wrapper.getLocker()).thenReturn(lock);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn(null);
        Mockito.when(exchange.getLoader()).thenReturn(loader);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        CacheResponse response = new CacheResponse();
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(exchange.getPolicy()).thenReturn(policy);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.POP, actions, cache);
        Mono<CacheResult> result = chain.load(exchange, wrapper);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testCachePenetratedNull(){
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        Mockito.when(cache.get(Mockito.any())).thenReturn("nil");
        Mockito.when(cache.isNull(Mockito.any())).thenReturn(true);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheResponse response = new CacheResponse();
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Mockito.when(exchange.getResponse()).thenReturn(response);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCachePenetratedNotNullObject(){
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        Mockito.when(cache.get(Mockito.any())).thenReturn("object");
        Mockito.when(cache.isNull(Mockito.any())).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        CacheResponse response = new CacheResponse();
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Mockito.when(exchange.getResponse()).thenReturn(response);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && "object".equals(r.getValue())).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCachePenetratedObject(){
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        Mockito.when(cache.get(Mockito.any())).thenReturn("nil");
        Mockito.when(cache.isNull(Mockito.any())).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        Mockito.when(exchange.getRequest()).thenReturn(request);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue().equals("nil")).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCachePenetrateNotAndFirstSuccess(){
        List<CacheAction> actions = new ArrayList<>(2);
        CacheAction action = Mockito.mock(CacheAction.class);
        Mockito.when(action.cache(Mockito.any(), Mockito.any())).thenReturn(Mono.just(CacheResult.success("a", "value")));
        Mockito.when(action.onLoadAfter(Mockito.any(), Mockito.any())).thenReturn(Mono.just(true));
        actions.add(action);
        action = Mockito.mock(CacheAction.class);
        Mockito.when(action.onLoadAfter(Mockito.any(), Mockito.any())).thenReturn(Mono.just(true));
        actions.add(action);

        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        Mockito.when(cache.name()).thenReturn("test");
        Mockito.when(cache.get(Mockito.any())).thenReturn(null);
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        Mockito.when(exchange.getRequest()).thenReturn(request);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue().equals("value")).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCacheFirstSuccess(){
        List<CacheAction> actions = new ArrayList<>(2);
        CacheAction action = Mockito.mock(CacheAction.class);
        Mockito.when(action.cache(Mockito.any(), Mockito.any())).thenReturn(Mono.just(CacheResult.success("a", "value")));
        Mockito.when(action.onLoadAfter(Mockito.any(), Mockito.any())).thenReturn(Mono.just(true));
        actions.add(action);
        action = Mockito.mock(CacheAction.class);
        Mockito.when(action.onLoadAfter(Mockito.any(), Mockito.any())).thenReturn(Mono.just(true));
        actions.add(action);

        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        Mockito.when(exchange.getRequest()).thenReturn(request);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue().equals("value")).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCacheAllFailed(){
        List<CacheAction> actions = new ArrayList<>(2);
        FailCacheAction action1 = new FailCacheAction();
        actions.add(action1);
        FailCacheAction action2 = new FailCacheAction();
        actions.add(action2);

        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheResponse response = Mockito.mock(CacheResponse.class);
        Mockito.when(response.getResult()).thenReturn(CacheResult.exception(new CacheNotExistException("b", "test"), "test"));
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Mockito.when(exchange.getResponse()).thenReturn(response);
        Mockito.when(exchange.getLoader()).thenReturn(null);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> !r.isSuccess()).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCacheAllFailedNotAllowed(){
        List<CacheAction> actions = new ArrayList<>(2);
        FailCacheAction action1 = new FailCacheAction();
        actions.add(action1);
        FailCacheAction action2 = new FailCacheAction();
        actions.add(action2);

        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.PUT, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheResponse response = Mockito.mock(CacheResponse.class);
        Mockito.when(response.getResult()).thenReturn(CacheResult.exception(new CacheNotExistException("b", "test"), "test"));
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Mockito.when(exchange.getResponse()).thenReturn(response);
        Mockito.when(exchange.getLoader()).thenReturn(null);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> !r.isSuccess()).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCacheAllFailedNotAllowedExp(){
        List<CacheAction> actions = new ArrayList<>(2);
        FailCacheAction action1 = new FailCacheAction();
        actions.add(action1);
        FailCacheAction action2 = new FailCacheAction();
        actions.add(action2);

        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.PUT, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheResponse response = Mockito.mock(CacheResponse.class);
        Throwable throwable = Mockito.mock(Throwable.class);
        Mockito.when(response.getThrowable()).thenReturn(throwable);
        Mockito.when(response.getResult()).thenReturn(CacheResult.exception(new CacheNotExistException("b", "test"), "test"));
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Mockito.when(exchange.getResponse()).thenReturn(response);
        Mockito.when(exchange.getLoader()).thenReturn(null);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(Throwable.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCacheAllFailedAndLoad(){
        List<CacheAction> actions = new ArrayList<>(2);
        FailCacheAction action1 = new FailCacheAction();
        actions.add(action1);
        FailCacheAction action2 = new FailCacheAction();
        actions.add(action2);

        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);

        CacheRequest request = Mockito.mock(CacheRequest.class);
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheResponse response = Mockito.mock(CacheResponse.class);
        Mockito.when(response.getResult()).thenReturn(CacheResult.exception(new CacheNotExistException("b", "test"), "test"));
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mockito.when(loader.load(Mockito.any())).thenReturn("object");
        Mockito.when(exchange.getLoader()).thenReturn(loader);

        Mono<CacheResult<String>> result = chain.cache(exchange);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue().equals("object")).verifyComplete();
        boolean b = chain.last();
        Assert.assertTrue(b);
    }
    static class FailCacheAction implements CacheAction{
        @Override
        @SuppressWarnings("unchecked")
        public Mono<CacheResult> cache(CacheExchange exchange, CacheActionChain chain) {
            return chain.cache(exchange);
        }

        @Override
        public CacheLayerProcessor processor() {
            return null;
        }
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPublishMapEmpty(){
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheResponse response = new CacheResponse();
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Map map = new HashMap<>();
        PenetrateLockWrapper wrapper = new PenetrateLockWrapper();
        wrapper.setLocker(null);
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        Mockito.when(cache.get(Mockito.any())).thenReturn("nil");
        Mockito.when(cache.isNull(Mockito.any())).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);
        Mono<CacheResult> result = chain.publish(exchange, wrapper, map);
        StepVerifier.create(result).expectError(CacheNotExistException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPublishMap(){
        CacheExchange exchange = Mockito.mock(CacheExchange.class);
        CacheResponse response = new CacheResponse();
        Mockito.when(exchange.getResponse()).thenReturn(response);
        CacheRequest request = Mockito.mock(CacheRequest.class);
        Mockito.when(request.getKey()).thenReturn("key");
        Mockito.when(exchange.getRequest()).thenReturn(request);
        Map map = new HashMap<>();
        map.put("test", 102L);
        PenetrateLockWrapper wrapper = new PenetrateLockWrapper();
        wrapper.setLocker(null);
        PenetrateCache cache = Mockito.mock(PenetrateCache.class);
        Mockito.when(cache.enabled()).thenReturn(true);
        Mockito.when(cache.get(Mockito.any())).thenReturn("nil");
        Mockito.when(cache.isNull(Mockito.any())).thenReturn(false);
        Mockito.when(cache.name()).thenReturn("test");
        DefaultCacheActionChain chain = new DefaultCacheActionChain(ACTION.GET, actions, cache);
        Mono<CacheResult> result = chain.publish(exchange, wrapper, map);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue() != null && r.getValue() instanceof Map).verifyComplete();
    }
}
