package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.*;

/**
 * DefaultHashCache的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class DefaultHashCacheTest {
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static CachePolicy policy;
    private static CacheTestModel model;

    @BeforeClass
    public static void setUp(){
        model = new CacheTestModel();
        model.setId(1L);
        model.setName("model1");
        model.setDate(new Date());

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        policy.setLayer(new String[]{"test"});

        factory.addPolicy(policy);
    }
    @Test
    public void testConstructor(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Assert.assertTrue(true);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPut(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Map<Long, CacheTestModel> map = new HashMap<>(1);
        map.put(model.getId(), model);
        Mono<CacheResult<Long>> result = cache.put("key", map);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGet(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        Map<Long, CacheTestModel> map = new HashMap<>(1);
        map.put(model.getId(), model);
        CacheResult cacheResult = CacheResult.success("test", map);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mono<CacheResult<Map<Long, CacheTestModel>>> result = cache.get("key", loader);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testExist(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<Map<Long, CacheTestModel>>> result = cache.exist("key", 1L);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRemoveList(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", 2L);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        List<Long> list = new ArrayList<>(2);
        list.add(1L);
        list.add(2L);
        Mono<CacheResult<Long>> result = cache.remove("key", list);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRemoveArray(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", 2L);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<Long>> result = cache.remove("key", 1L, 2L);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGetListByListNull(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<List<CacheTestModel>>> result = cache.get("key", (List)null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testGetListByListEmpty(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        List<Long> records = new ArrayList<>();
        Mono<CacheResult<List<CacheTestModel>>> result = cache.get("key", records);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(InvalidCacheValueException.class).verify();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGetListByArrayNull(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<List<CacheTestModel>>> result = cache.get("key", (Long[])null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testGetListByArrayEmpty(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Long[] records = new Long[0];
        Mono<CacheResult<List<CacheTestModel>>> result = cache.get("key", records);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testGetList(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        List<CacheTestModel> records = new ArrayList<>(1);
        records.add(model);
        CacheResult cacheResult = CacheResult.success("test", records);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        List<Long> list = new ArrayList<>(2);
        list.add(1L);
        list.add(2L);
        Mono<CacheResult<List<CacheTestModel>>> result = cache.get("key", list);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testGetArray(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        List<CacheTestModel> records = new ArrayList<>(1);
        records.add(model);
        CacheResult cacheResult = CacheResult.success("test", records);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultHashCache cache = new DefaultHashCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<List<CacheTestModel>>> result = cache.get("key", 1L, 2L);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
}
