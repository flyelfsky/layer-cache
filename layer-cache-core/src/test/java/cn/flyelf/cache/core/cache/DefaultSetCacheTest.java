package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.*;

/**
 * DefaultSetCache的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class DefaultSetCacheTest {
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static CachePolicy policy;
    private static CacheTestModel model;

    @BeforeClass
    public static void setUp(){
        model = new CacheTestModel();
        model.setId(1L);
        model.setName("model1");
        model.setDate(new Date());

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_SET);
        policy.setLayer(new String[]{"test"});

        factory.addPolicy(policy);
    }

    @Test
    public void testConstructor(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        new DefaultSetCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Assert.assertTrue(true);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPut(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultSetCache cache = new DefaultSetCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Set<CacheTestModel> records = new HashSet<>(1);
        records.add(model);
        Mono<CacheResult<Boolean>> result = cache.put("key", records);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGet(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        Set<CacheTestModel> records = new HashSet<>(1);
        records.add(model);
        CacheResult cacheResult = CacheResult.success("test", records);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultSetCache cache = new DefaultSetCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mono<CacheResult<Set<CacheTestModel>>> result = cache.get("key", loader);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
}
