package cn.flyelf.cache.core.monitor;

import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.event.AbstractCacheEventSubscriber;
import cn.flyelf.cache.core.model.CacheEvent;
import org.junit.Assert;
import org.junit.Test;

/**
 * CacheEventPublisherImp的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class CacheEventPublisherImpTest {
    @Test
    public void testOnCacheEvent(){
        CacheEventPublisher publisher = new CacheEventPublisherImp();
        publisher.subscribe(new AbstractCacheEventSubscriber() {
            @Override
            protected void onCacheEvent(CacheEvent event) {
            }
        });
        publisher.publish(new CacheEvent(ACTION.POP, "test", "default", "key"));
        publisher.stop();
        Assert.assertTrue(true);
    }
}
