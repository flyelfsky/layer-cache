package cn.flyelf.cache.core;

import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.core.model.CacheResult;
import org.junit.Assert;
import org.junit.Test;
import reactor.core.publisher.Mono;

import java.util.Collection;

/**
 * cache接口的测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
  变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class CacheTest {
    private static Cache cache = new Cache() {
        @Override
        public String name() {
            return null;
        }

        @Override
        public Mono<CacheResult> get(Object key, CacheObjectLoader loader) {
            return null;
        }

        @Override
        public Mono<CacheResult<Boolean>> put(Object key, Object value) {
            return null;
        }

        @Override
        public Mono<CacheResult<Long>> delete(Object key) {
            return null;
        }

        @Override
        public Mono<CacheResult<Long>> delete(Object[] keys) {
            return null;
        }

        @Override
        public Mono<CacheResult<Long>> delete(Collection keys) {
            return null;
        }
    };

    @Test
    public void testActual(){
        Cache result = cache.actual();
        Assert.assertEquals(cache, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testGet(){
        Mono<CacheResult<CacheTestModel>> result = cache.get("abc");
        Assert.assertNull(result);
    }
}
