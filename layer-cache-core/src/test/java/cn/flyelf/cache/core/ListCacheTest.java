package cn.flyelf.cache.core;

import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.core.model.CacheResult;
import org.junit.Assert;
import org.junit.Test;
import reactor.core.publisher.Mono;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * ListCache接口的测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class ListCacheTest {
    private static ListCache cache = new ListCache() {
        @Override
        public String name() {
            return null;
        }

        @Override
        public Mono<CacheResult> get(Object key, CacheObjectLoader loader) {
            return null;
        }

        @Override
        public Mono<CacheResult<Boolean>> put(Object key, Object value) {
            return null;
        }

        @Override
        public Mono<CacheResult<Long>> delete(Object key) {
            return null;
        }

        @Override
        public Mono<CacheResult<Long>> delete(Object[] keys) {
            return null;
        }

        @Override
        public Mono<CacheResult<Long>> delete(Collection keys) {
            return null;
        }

        @Override
        public Mono<CacheResult> rpop(Object key) {
            return null;
        }

        @Override
        public Mono<CacheResult> lpop(Object key) {
            return null;
        }

        @Override
        public Mono<CacheResult<Boolean>> lpush(Object key, List value) {
            return null;
        }

        @Override
        public Mono<CacheResult<Boolean>> lpush(Object key, Object[] value) {
            return null;
        }

        @Override
        public Mono<CacheResult<Boolean>> rpush(Object key, List value) {
            return null;
        }

        @Override
        public Mono<CacheResult<Boolean>> rpush(Object key, Object[] value) {
            return null;
        }
    };

    @Test
    @SuppressWarnings("unchecked")
    public void testPop(){
        Mono<CacheResult<CacheTestModel>> result = cache.pop("abc");
        Assert.assertNull(result);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPushArray(){
        Mono<CacheResult<CacheTestModel>> result = cache.push("abc", new CacheTestModel());
        Assert.assertNull(result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPushList(){
        Mono<CacheResult<CacheTestModel>> result = cache.push("abc", new ArrayList());
        Assert.assertNull(result);
    }
}
