package cn.flyelf.cache.core.event;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.model.CacheEvent;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.reactivestreams.Subscription;

/**
 * AbstractCacheEventSubscriber的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class AbstractCacheEventSubscriberTest {
    private static AbstractCacheEventSubscriber subscriber = new AbstractCacheEventSubscriber(){
        @Override
        protected void onCacheEvent(CacheEvent event) {
        }
    };
    @Test
    public void test(){
        Subscription subscription = Mockito.mock(Subscription.class);
        subscriber.hookOnSubscribe(subscription);
        subscriber.hookOnNext(new CacheEvent(ACTION.POP, "test", "default", "key"));
        subscriber.cancel();
        subscriber.hookOnCancel();
        subscriber.hookOnError(new Exception("error"));
        subscriber.hookOnComplete();
        Assert.assertTrue(true);
    }
}
