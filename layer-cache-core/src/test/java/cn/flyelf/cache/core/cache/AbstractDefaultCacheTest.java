package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.exception.CacheKeyMismatchException;
import cn.flyelf.cache.core.exception.UnsupportedCacheObjectException;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * AbstractDefaultCache的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class AbstractDefaultCacheTest {
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static CachePolicy policy;
    private static CacheTestModel model;
    static class TestDefaultCache extends AbstractDefaultCache{
        TestDefaultCache(CacheLayerFactory factory, Class keyClass, Class targetClass, Class serializeClass, String type, String suffix){
            super(factory, AbstractDefaultCacheTest.policy.getLayer(), keyClass, targetClass, serializeClass, type, suffix);
        }
    }
    private static TestDefaultCache cache;

    @BeforeClass
    public static void setUp(){
        model = new CacheTestModel();
        model.setId(1L);
        model.setName("model1");
        model.setDate(new Date());

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        policy.setLayer(new String[]{"test"});

        factory.addPolicy(policy);

        cache = new TestDefaultCache(factory, String.class, CacheTestModel.class, CacheTestModel.class, CacheConstant.CACHE_SIMPLE, null);
    }

    @Test(expected = UnsupportedCacheObjectException.class)
    public void testConstructorExp(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.anyString())).thenReturn(null);
        new TestDefaultCache(factory, String.class, Long.class, Long.class, "simple", null);
    }
    @Test(expected = NullPointerException.class)
    public void testConstructorNullFactory(){
        new TestDefaultCache(null, String.class, Long.class, Long.class, "simple", null);
    }

    @Test
    public void testName(){
        String name = cache.name();
        Assert.assertEquals(policy.name(), name);
    }

    @Test(expected = NullPointerException.class)
    @SuppressWarnings("unchecked")
    public void testCheckKeyNull(){
        cache.checkKey(null);
    }
    @Test(expected = CacheKeyMismatchException.class)
    @SuppressWarnings("unchecked")
    public void testCheckKeyMis(){
        cache.checkKey(101L);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testCheckKey(){
        cache.checkKey("key");
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteArrayNull(){
        Mono<CacheResult<Long>> result = cache.delete((Object[])null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(NullPointerException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteArrayEmpty(){
        String[] keys = new String[0];
        Mono<CacheResult<Long>> result = cache.delete((Object[])keys);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(NullPointerException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteArray(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", 2L);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        TestDefaultCache cache = new TestDefaultCache(factory, String.class, CacheTestModel.class, CacheTestModel.class, CacheConstant.CACHE_SIMPLE, null);
        Mono<CacheResult<Long>> result = cache.delete("key1", "key2");
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteListNull(){
        Mono<CacheResult<Long>> result = cache.delete((List)null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(NullPointerException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteListEmpty(){
        List<String> keys = new ArrayList<>();
        Mono<CacheResult<Long>> result = cache.delete(keys);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(NullPointerException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteList(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", 2L);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        TestDefaultCache cache = new TestDefaultCache(factory, String.class, CacheTestModel.class, CacheTestModel.class, CacheConstant.CACHE_SIMPLE, null);
        List<String> keys = new ArrayList<>(2);
        keys.add("key1");
        keys.add("key2");
        Mono<CacheResult<Long>> result = cache.delete(keys);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testDelete(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", 2L);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        TestDefaultCache cache = new TestDefaultCache(factory, String.class, CacheTestModel.class, CacheTestModel.class, CacheConstant.CACHE_SIMPLE, null);
        Mono<CacheResult<Long>> result = cache.delete("key");
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPut(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        TestDefaultCache cache = new TestDefaultCache(factory, String.class, CacheTestModel.class, CacheTestModel.class, CacheConstant.CACHE_SIMPLE, null);
        Mono<CacheResult<Long>> result = cache.put("key", model);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGet(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", model);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        TestDefaultCache cache = new TestDefaultCache(factory, String.class, CacheTestModel.class, CacheTestModel.class, CacheConstant.CACHE_SIMPLE, null);
        Mono<CacheResult<Long>> result = cache.get("key", null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
}
