package cn.flyelf.cache.core.cache;

import cn.flyelf.cache.core.exception.CacheMultipleLayerException;
import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.*;

/**
 * DefaultListCache的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class DefaultListCacheTest {
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static CachePolicy policy;
    private static CacheTestModel model;

    @BeforeClass
    public static void setUp(){
        model = new CacheTestModel();
        model.setId(1L);
        model.setName("model1");
        model.setDate(new Date());

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_LIST);
        policy.setLayer(new String[]{"test"});

        factory.addPolicy(policy);
    }

    @Test
    public void testConstructor(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Assert.assertTrue(true);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPut(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        List<CacheTestModel> records = new ArrayList<>(1);
        records.add(model);
        Mono<CacheResult<Boolean>> result = cache.put("key", records);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGet(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        List<CacheTestModel> records = new ArrayList<>(1);
        records.add(model);
        CacheResult cacheResult = CacheResult.success("test", records);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        CacheObjectLoader loader = Mockito.mock(CacheObjectLoader.class);
        Mono<CacheResult<List<CacheTestModel>>> result = cache.get("key", loader);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testRpop(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", model);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<CacheTestModel>> result = cache.rpop("key");
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLpop(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", model);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<CacheTestModel>> result = cache.lpop("key");
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLpopLayers(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        CachePolicy policy = new CachePolicy();
        policy.setLayer(new String[]{"a", "b"});
        policy.setObject(CacheTestModel.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", model);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<CacheTestModel>> result = cache.lpop("key");
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(CacheMultipleLayerException.class).verify();
    }

    @Test(expected = InvalidCacheValueException.class)
    @SuppressWarnings("unchecked")
    public void testAsListNull(){
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        cache.asList("key", null);
    }
    @Test(expected = InvalidCacheValueException.class)
    @SuppressWarnings("unchecked")
    public void testAsListEmpty(){
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        CacheTestModel[] array = new CacheTestModel[0];
        cache.asList("key", array);
    }
    @Test(expected = InvalidCacheValueException.class)
    @SuppressWarnings("unchecked")
    public void testAsListEmptyList(){
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        CacheTestModel[] array = new CacheTestModel[2];
        array[0] = null;
        array[1] = null;
        cache.asList("key", array);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testAsList(){
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        CacheTestModel[] array = new CacheTestModel[2];
        array[0] = null;
        array[1] = model;
        List<CacheTestModel> result = cache.asList("key", array);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.size());
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPushObjectMultiLayer(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        CachePolicy policy = new CachePolicy();
        policy.setLayer(new String[]{"a", "b"});
        policy.setObject(CacheTestModel.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        List<CacheTestModel> records = new ArrayList<>();
        Mono<CacheResult<Boolean>> result = cache.pushObject("key", "left", records);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(CacheMultipleLayerException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPushObjectListNull(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<Boolean>> result = cache.pushObject("key", "left", null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPushObjectListEmpty(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        List<CacheTestModel> records = new ArrayList<>();
        Mono<CacheResult<Boolean>> result = cache.pushObject("key", "left", records);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(InvalidCacheValueException.class).verify();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testLpushArrayEmpty(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        CacheTestModel[] array = new CacheTestModel[0];
        Mono<CacheResult<Boolean>> result = cache.lpush("key", array);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLpushArray(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<Boolean>> result = cache.lpush("key", model);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLpushList(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        List<CacheTestModel> records = new ArrayList<>();
        records.add(model);
        Mono<CacheResult<Boolean>> result = cache.lpush("key", records);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testRpushArrayEmpty(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        CacheTestModel[] array = new CacheTestModel[0];
        Mono<CacheResult<Boolean>> result = cache.rpush("key", array);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRpushArray(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        Mono<CacheResult<Boolean>> result = cache.rpush("key", model);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRpushList(){
        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(Mockito.any(), Mockito.any(), Mockito.anyString(), Mockito.any())).thenReturn(policy);
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(factory.buildActionChain(Mockito.any(), Mockito.any())).thenReturn(chain);
        CacheResult cacheResult = CacheResult.success("test", true);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(cacheResult));
        DefaultListCache cache = new DefaultListCache(factory, policy.getLayer(), String.class, CacheTestModel.class);
        List<CacheTestModel> records = new ArrayList<>();
        records.add(model);
        Mono<CacheResult<Boolean>> result = cache.rpush("key", records);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(cacheResult).verifyComplete();
    }
}
