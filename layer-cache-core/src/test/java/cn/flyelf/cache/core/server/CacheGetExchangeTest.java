package cn.flyelf.cache.core.server;

import cn.flyelf.cache.core.model.CachePolicy;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * CacheGetExchange的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class CacheGetExchangeTest {
    @Test
    @SuppressWarnings("unchecked")
    public void testConstructor(){
        CachePolicy policy = Mockito.mock(CachePolicy.class);
        new CacheGetExchange("key", Long.class, policy);
        Assert.assertTrue(true);
    }
}
