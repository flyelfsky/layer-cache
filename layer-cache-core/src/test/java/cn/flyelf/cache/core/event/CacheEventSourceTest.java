package cn.flyelf.cache.core.event;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * CacheEventSource的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class CacheEventSourceTest {
    @Test
    public void testSource(){
        CacheEventSource source = new CacheEventSource();
        CacheEventListener listener = Mockito.mock(CacheEventListener.class);
        source.register(listener);
        source.stop();
        Assert.assertTrue(true);
    }
}
