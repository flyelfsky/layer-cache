package cn.flyelf.cache.core.util;

import cn.flyelf.cache.core.model.Duration;
import org.junit.Assert;
import org.junit.Test;

import java.time.format.DateTimeParseException;
import java.util.concurrent.TimeUnit;

/**
 * 耗时转换器的测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class DurationConverterTest {
    @Test(expected = DateTimeParseException.class)
    public void testOfLength0(){
        DurationConverter.of("ab");
    }
    @Test(expected = DateTimeParseException.class)
    public void testOfBlank(){
        DurationConverter.of("h8");
    }
    @Test
    public void testOf(){
        Duration duration = DurationConverter.of("9s");
        Assert.assertNotNull(duration);
        Assert.assertEquals(9, duration.getDuration().longValue());
        Assert.assertEquals(TimeUnit.SECONDS, duration.getUnit());
    }
}
