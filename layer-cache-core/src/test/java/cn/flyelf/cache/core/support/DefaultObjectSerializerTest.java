package cn.flyelf.cache.core.support;

import cn.flyelf.cache.core.exception.CacheException;
import cn.flyelf.cache.core.model.CacheTestModel;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;

/**
 * 默认对象序列化的测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class DefaultObjectSerializerTest {
    private static DefaultObjectSerializer serializer = new DefaultObjectSerializer();

    @Test
    public void testWithString(){
        ByteBuffer buffer = serializer.encode("abc");
        Assert.assertNotNull(buffer);
        String result = serializer.decode(buffer, String.class);
        Assert.assertNotNull(result);
        Assert.assertEquals("abc", result);
    }

    @Test
    public void testWithInteger(){
        ByteBuffer buffer = serializer.encode(1);
        Assert.assertNotNull(buffer);
        Integer result = serializer.decode(buffer, Integer.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.intValue());
    }
    @Test
    public void testWithShort(){
        ByteBuffer buffer = serializer.encode(1);
        Assert.assertNotNull(buffer);
        Short result = serializer.decode(buffer, Short.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(1, result.shortValue());
    }
    @Test
    public void testWithFloat(){
        ByteBuffer buffer = serializer.encode(1.);
        Assert.assertNotNull(buffer);
        Float result = serializer.decode(buffer, Float.class);
        Assert.assertNotNull(result);
    }
    @Test
    public void testWithDouble(){
        ByteBuffer buffer = serializer.encode(1.);
        Assert.assertNotNull(buffer);
        Double result = serializer.decode(buffer, Double.class);
        Assert.assertNotNull(result);
    }
    @Test
    public void testWithLong(){
        ByteBuffer buffer = serializer.encode(1L);
        Assert.assertNotNull(buffer);
        Long result = serializer.decode(buffer, Long.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(1L, result.longValue());
    }
    @Test
    public void testWithNumber(){
        ByteBuffer buffer = serializer.encode(1L);
        Assert.assertNotNull(buffer);
        Number result = serializer.decode(buffer, Number.class);
        Assert.assertNotNull(result);
        Assert.assertEquals(1L, result.longValue());
    }

    @Test
    public void testWithClassNull(){
        CacheTestModel model = new CacheTestModel();
        model.setId(1L);
        ByteBuffer buffer = serializer.encode(model);
        Assert.assertNull(buffer);
    }
    @Test
    public void testWithClass(){
        CacheException model = new CacheException(null, null, "test");
        ByteBuffer buffer = serializer.encode(model);
        Assert.assertNotNull(buffer);
        CacheException result = serializer.decode(buffer, CacheException.class);
        Assert.assertNotNull(result);
        Assert.assertEquals("test", result.getMessage());
    }
    @Test
    public void testUnwrapNull(){
        CacheTestModel result = serializer.decode(null, CacheTestModel.class);
        Assert.assertNull(result);
    }
}
