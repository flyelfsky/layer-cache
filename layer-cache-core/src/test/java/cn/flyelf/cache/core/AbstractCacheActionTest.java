package cn.flyelf.cache.core;


import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.context.CacheContext;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheException;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.core.server.CacheDefaultExchange;
import cn.flyelf.cache.core.server.CacheExchange;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * 抽象缓存动作的单元测试
 *
 * @author wujr
 * 2020-01-04
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-04 1.0 新增]
 */
public class AbstractCacheActionTest {
    private static TestCacheActionModel<String, CacheTestModel, CacheTestModel, CacheLayerProcessor> cacheAction;
    private static CacheLayerProcessor processor;
    private static CachePolicy policy;
    private static CacheTestModel model;
    private static CacheEventPublisher eventPublisher = new CacheEventPublisherImp();

    @BeforeClass
    public static void setUp(){
        processor = Mockito.mock(CacheLayerProcessor.class);
        Mockito.when(processor.name()).thenReturn("test");
        Mockito.when(processor.eventPublisher()).thenReturn(eventPublisher);
        cacheAction = new TestCacheActionModel<>(ACTION.GET, processor);
        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(new String[]{"test"});

        model = new CacheTestModel();
        model.setId(1L);
        model.setName("model1");
        model.setDate(new Date());
    }

    @Test
    public void testProcessor(){
        CacheLayerProcessor result = cacheAction.processor();
        Assert.assertEquals(processor, result);
    }
    @Test
    public void testInitContextNull(){
        CacheContext context = cacheAction.initContext(null);
        Assert.assertNull(context);
    }
    @Test
    public void testInitContext(){
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        CacheExchange<String, CacheTestModel, CacheTestModel> exchange = new CacheDefaultExchange<>(request, policy);
        CacheContext context = cacheAction.initContext(exchange);
        Assert.assertNotNull(context);
    }
    @Test
    public void testDestroyContext(){
        cacheAction.destroyContext();
        Assert.assertTrue(true);
    }

    @Test
    public void testGetArrayFromAttachmentNull(){
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        CacheExchange<String, CacheTestModel, CacheTestModel> exchange = new CacheDefaultExchange<>(request, policy);
        CacheTestModel[] models = cacheAction.getArrayFromAttachment(exchange, "test");
        Assert.assertNotNull(models);
        Assert.assertEquals(0, models.length);
    }
    @Test
    public void testGetArrayFromAttachmentEmptyList(){
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        List<CacheTestModel> list = new ArrayList<>(0);
        request.addAttachment("test", list);
        CacheExchange<String, CacheTestModel, CacheTestModel> exchange = new CacheDefaultExchange<>(request, policy);
        CacheTestModel[] models = cacheAction.getArrayFromAttachment(exchange, "test");
        Assert.assertNotNull(models);
        Assert.assertEquals(0, models.length);
    }
    @Test
    public void testGetArrayFromAttachmentList(){
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        List<CacheTestModel> list = new ArrayList<>(1);
        list.add(model);
        request.addAttachment("test", list);
        CacheExchange<String, CacheTestModel, CacheTestModel> exchange = new CacheDefaultExchange<>(request, policy);
        CacheTestModel[] models = cacheAction.getArrayFromAttachment(exchange, "test");
        Assert.assertNotNull(models);
        Assert.assertEquals(1, models.length);
        Assert.assertEquals(1L, models[0].getId().longValue());
    }
    @Test
    public void testGetArrayFromAttachmentArray(){
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        CacheTestModel[] list = new CacheTestModel[]{model};
        request.addAttachment("test", list);
        CacheExchange<String, CacheTestModel, CacheTestModel> exchange = new CacheDefaultExchange<>(request, policy);
        CacheTestModel[] models = cacheAction.getArrayFromAttachment(exchange, "test");
        Assert.assertNotNull(models);
        Assert.assertEquals(1, models.length);
        Assert.assertEquals(1L, models[0].getId().longValue());
    }

    @Test
    public void testCacheNullContext(){
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(CacheResult.success("test", model)));
        Mono<CacheResult<CacheTestModel>> result = cacheAction.cache(null, chain);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && model == r.getValue()).verifyComplete();
    }

    @Test
    public void testCacheSuccessNeedSyncAndNotIgnore(){
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        request.addAttachment("testType", 1);
        request.addAttachment("result", model);
        CacheExchange<String, CacheTestModel, CacheTestModel> exchange = new CacheDefaultExchange<>(request, policy);
        Mono<CacheResult<CacheTestModel>> result = cacheAction.cache(exchange, chain);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && model == r.getValue()).verifyComplete();
    }
    @Test
    public void testCacheSuccessNeedSyncAndIgnore(){
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(CacheResult.success("test", true)));
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        request.addAttachment("testType", 1);
        request.addAttachment("result", true);
        CacheExchange<String, CacheTestModel, Boolean> exchange = new CacheDefaultExchange<>(request, policy);
        TestCacheActionModel<String, CacheTestModel, Boolean, CacheLayerProcessor> cacheAction = new TestCacheActionModel<>(ACTION.PUT, processor);
        Mono<CacheResult<Boolean>> result = cacheAction.cache(exchange, chain);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> r.isSuccess() && r.getValue()).verifyComplete();
    }
    @Test
    public void testCacheError(){
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(CacheResult.exception(new CacheException("test", CacheConstant.RESULT.UNKNOWN, "error"), "test")));
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        request.addAttachment("testType", 2);
        request.addAttachment("result", false);
        CacheExchange<String, CacheTestModel, Boolean> exchange = new CacheDefaultExchange<>(request, policy);
        TestCacheActionModel<String, CacheTestModel, Boolean, CacheLayerProcessor> cacheAction = new TestCacheActionModel<>(ACTION.PUT, processor);
        Mono<CacheResult<Boolean>> result = cacheAction.cache(exchange, chain);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> !r.isSuccess()).verifyComplete();
    }
    @Test
    public void testCacheError2(){
        CacheActionChain chain = Mockito.mock(CacheActionChain.class);
        Mockito.when(chain.cache(Mockito.any())).thenReturn(Mono.just(CacheResult.exception(new Exception("test"), "test")));
        CacheRequest<String, CacheTestModel> request = new CacheRequest<>("test", CacheTestModel.class);
        request.addAttachment("testType", 3);
        request.addAttachment("result", false);
        CacheExchange<String, CacheTestModel, Boolean> exchange = new CacheDefaultExchange<>(request, policy);
        TestCacheActionModel<String, CacheTestModel, Boolean, CacheLayerProcessor> cacheAction = new TestCacheActionModel<>(ACTION.PUT, processor);
        Mono<CacheResult<Boolean>> result = cacheAction.cache(exchange, chain);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNextMatches(r -> !r.isSuccess()).verifyComplete();
    }
}
