package cn.flyelf.cache.core;

import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.util.TypeUtil;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.ParameterizeType;
import cn.flyelf.cache.core.model.TestParameterizeType;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * 类型工具的单元测试
 *
 * @author wujr
 * 2020-01-04
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-04 1.0 新增]
 */
public class TypeUtilTest {
    private static CacheTestModel model;
    private static Type innerType = new Type() {
        @Override
        public String getTypeName() {
            return Long.class.getTypeName();
        }
    };

    @BeforeClass
    public static void setUp(){
        model = new CacheTestModel();
        model.setId(1L);
        model.setName("model1");
        model.setIndex(1);
        model.setDate(new Date());
    }
    @Test
    public void testSet2Array(){
        Set<CacheTestModel> set = new HashSet<>(1);
        set.add(model);
        CacheTestModel[] array = TypeUtil.set2Array(set);
        Assert.assertNotNull(array);
        Assert.assertEquals(1, array.length);
        Assert.assertEquals(1L, array[0].getId().longValue());
    }
    @Test
    public void testEmptyArray(){
        CacheTestModel[] empty = TypeUtil.emptyArray(CacheTestModel.class);
        Assert.assertNotNull(empty);
        Assert.assertEquals(0, empty.length);
    }
    @Test
    public void testList2Array(){
        List<CacheTestModel> list = new ArrayList<>(1);
        list.add(model);
        CacheTestModel[] array = TypeUtil.list2Array(list);
        Assert.assertNotNull(array);
        Assert.assertEquals(1, array.length);
        Assert.assertEquals(1L, array[0].getId().longValue());
    }
    @Test
    public void testCollection2Array(){
        Collection<CacheTestModel> records = new ArrayList<>(1);
        records.add(model);
        CacheTestModel[] array = TypeUtil.collection2Array(records);
        Assert.assertNotNull(array);
        Assert.assertEquals(1, array.length);
        Assert.assertEquals(1L, array[0].getId().longValue());
    }
    @Test
    public void testResolveCacheTypeClass(){
        String type = TypeUtil.resolveCacheType(CacheTestModel.class);
        Assert.assertEquals(CacheConstant.CACHE_SIMPLE, type);
    }
    @Test
    public void testResolveCacheTypeParameterizeUnknown(){
        ParameterizeType<CacheTestModel> object = new TestParameterizeType<>();
        String type = TypeUtil.resolveCacheType(object.getClass().getGenericSuperclass());
        Assert.assertEquals(CacheConstant.CACHE_SIMPLE, type);
    }

    @Test
    public void testResolveCacheTypeParameterizeList(){
        List<CacheTestModel> object = new ArrayList<>(1);
        object.add(model);
        String type = TypeUtil.resolveCacheType(object.getClass().getGenericSuperclass());
        Assert.assertEquals(CacheConstant.CACHE_LIST, type);
    }
    @Test
    public void testResolveCacheTypeParameterizeSet(){
        Set<CacheTestModel> object = new HashSet<>(1);
        object.add(model);
        String type = TypeUtil.resolveCacheType(object.getClass().getGenericSuperclass());
        Assert.assertEquals(CacheConstant.CACHE_SET, type);
    }
    @Test
    public void testResolveCacheTypeParameterizeMap(){
        Map<String, CacheTestModel> object = new HashMap<>(1);
        object.put(model.getName(), model);
        String type = TypeUtil.resolveCacheType(object.getClass().getGenericSuperclass());
        Assert.assertEquals(CacheConstant.CACHE_MAP, type);
    }

    @Test
    public void testResolveCacheTypeParameterizeParameter(){
        ParameterizedType type = Mockito.mock(ParameterizedType.class);
        ParameterizedType rawType = Mockito.mock(ParameterizedType.class);
        Mockito.when(type.getRawType()).thenReturn(rawType);
        String cacheType = TypeUtil.resolveCacheType(type);
        Assert.assertEquals(CacheConstant.CACHE_SIMPLE, cacheType);
    }

    @Test
    public void testCheckAssignableDef(){
        Class clz = TypeUtil.checkAssignable(List.class, List.class, ArrayList.class);
        Assert.assertNotNull(clz);
        Assert.assertEquals(ArrayList.class, clz);
    }
    @Test
    public void testCheckAssignableNoDef(){
        Class clz = TypeUtil.checkAssignable(List.class, ArrayList.class, ArrayList.class);
        Assert.assertNotNull(clz);
        Assert.assertEquals(ArrayList.class, clz);
    }
    @Test
    public void testCheckAssignableNull(){
        Class clz = TypeUtil.checkAssignable(List.class, String.class, ArrayList.class);
        Assert.assertNull(clz);
    }

    @Test
    public void testCheckRawTypeOk(){
        Class clz = TypeUtil.checkRawType(List.class);
        Assert.assertNotNull(clz);
        Assert.assertEquals(ArrayList.class, clz);
    }
    @Test
    public void testCheckRawTypeNull(){
        Class clz = TypeUtil.checkRawType(String.class);
        Assert.assertNotNull(clz);
        Assert.assertEquals(String.class, clz);
    }

    @Test
    public void testResolverRawTypeClass(){
        Class clz = TypeUtil.resolverRawType(String.class);
        Assert.assertNotNull(clz);
        Assert.assertEquals(String.class, clz);
    }
    @Test
    public void testResolverRawTypeParameterClass(){
        ParameterizedType type = Mockito.mock(ParameterizedType.class);
        Class rawType = String.class;
        Mockito.when(type.getRawType()).thenReturn(rawType);
        Class clz = TypeUtil.resolverRawType(type);
        Assert.assertNotNull(clz);
        Assert.assertEquals(String.class, clz);
    }
    @Test
    public void testResolverRawTypeParameterType(){
        ParameterizedType type = Mockito.mock(ParameterizedType.class);
        Mockito.when(type.getRawType()).thenReturn(innerType);
        Class clz = TypeUtil.resolverRawType(type);
        Assert.assertNotNull(clz);
        String clzName = clz.getName();
        clzName = clzName.substring(0, clzName.lastIndexOf("$"));
        Assert.assertTrue(clzName.endsWith("TypeUtilTest"));
    }

    @Test
    public void testResolverRawTypeType(){
        Class clz = TypeUtil.resolverRawType(innerType);
        Assert.assertNotNull(clz);
        String clzName = clz.getName();
        clzName = clzName.substring(0, clzName.lastIndexOf("$"));
        Assert.assertTrue(clzName.endsWith("TypeUtilTest"));
    }

    @Test
    public void testResolverTypeClassType(){
        Class clz = TypeUtil.resolverTypeClass(innerType);
        Assert.assertNotNull(clz);
        String clzName = clz.getName();
        clzName = clzName.substring(0, clzName.lastIndexOf("$"));
        Assert.assertTrue(clzName.endsWith("TypeUtilTest"));
    }
    @Test
    public void testResolverTypeClassClass(){
        Class clz = TypeUtil.resolverTypeClass(String.class);
        Assert.assertNotNull(clz);
        Assert.assertEquals(String.class, clz);
    }
    @Test
    public void testResolverTypeParameter(){
        ParameterizedType type = Mockito.mock(ParameterizedType.class);
        Type[] types = new Type[]{innerType};
        Mockito.when(type.getActualTypeArguments()).thenReturn(types);
        Class clz = TypeUtil.resolverTypeClass(type);
        Assert.assertNotNull(clz);
        String clzName = clz.getName();
        clzName = clzName.substring(0, clzName.lastIndexOf("$"));
        Assert.assertTrue(clzName.endsWith("TypeUtilTest"));
    }
}
