package cn.flyelf.cache.core;

import cn.flyelf.cache.core.action.CacheActionChain;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import org.junit.Assert;
import org.junit.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

/**
 * CacheAction接口的测试用例
 *
 * @author wujr
 * 2020-01-06
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-06 1.0 新增]
 */
public class CacheActionTest {
    @Test
    @SuppressWarnings("unchecked")
    public void testOnLoadAfter(){
        CacheAction action = new CacheAction() {
            @Override
            public Mono<CacheResult> cache(CacheExchange exchange, CacheActionChain chain) {
                return null;
            }

            @Override
            public CacheLayerProcessor processor() {
                return null;
            }
        };
        Mono<Boolean> result = action.onLoadAfter(null, null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(true).verifyComplete();
    }
}
