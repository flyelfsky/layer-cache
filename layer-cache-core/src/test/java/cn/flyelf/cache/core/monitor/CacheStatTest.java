package cn.flyelf.cache.core.monitor;

import org.junit.Assert;
import org.junit.Test;


/**
 * CacheStat的测试用例
 *
 * @author wujr
 * 2020/1/7
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/7 1.0 新增]
 */
public class CacheStatTest {
    @Test
    public void testMaxNotSet(){
        CacheStat stat = new CacheStat();
        String result = stat.max("%10d");
        Assert.assertEquals("         0", result);
    }
    @Test
    public void testMax(){
        CacheStat stat = new CacheStat();
        stat.setMax(12L);
        String result = stat.max("%10d");
        Assert.assertEquals("        12", result);
    }
    @Test
    public void testMinNotSet(){
        CacheStat stat = new CacheStat();
        String result = stat.min("%10d");
        Assert.assertEquals("         0", result);
    }
    @Test
    public void testMin(){
        CacheStat stat = new CacheStat();
        stat.setMin(12L);
        String result = stat.min("%10d");
        Assert.assertEquals("        12", result);
    }
    @Test
    public void testHitRate(){
        CacheStat stat = new CacheStat();
        double d = stat.hitRate();
        Assert.assertEquals(0, d, 0.0001);
    }
    @Test
    public void testHitRate2(){
        CacheStat stat = new CacheStat();
        stat.setSuccess(2);
        stat.setTotal(3);
        double d = stat.hitRate();
        Assert.assertEquals(0.6667, d, 0.0001);
    }
    @Test
    public void testAverageZero(){
        CacheStat stat = new CacheStat();
        double d = stat.average();
        Assert.assertEquals(0, d, 0.0001);
    }
    @Test
    public void testAverage(){
        CacheStat stat = new CacheStat();
        stat.setTotal(3);
        stat.setUsedTotal(13);
        double d = stat.average();
        Assert.assertEquals(4.3333, d, 0.001);
    }
    @Test
    public void testSuccessAverageZero(){
        CacheStat stat = new CacheStat();
        double d = stat.successAverage();
        Assert.assertEquals(0, d, 0.0001);
    }
    @Test
    public void testSuccessAverage(){
        CacheStat stat = new CacheStat();
        stat.setSuccess(3);
        stat.setUsedSuccess(13);
        double d = stat.successAverage();
        Assert.assertEquals(4.3333, d, 0.001);
    }
    @Test
    public void testFailureAverageZero(){
        CacheStat stat = new CacheStat();
        double d = stat.failureAverage();
        Assert.assertEquals(0, d, 0.0001);
    }
    @Test
    public void testFailureAverage(){
        CacheStat stat = new CacheStat();
        stat.setFailure(3);
        stat.setUsedFailure(13);
        double d = stat.failureAverage();
        Assert.assertEquals(4.3333, d, 0.001);
    }

    @Test
    public void testTpsZero(){
        CacheStat stat = new CacheStat();
        double d = stat.tps(0);
        Assert.assertEquals(0, d, 0.0001);
    }
    @Test
    public void testTps(){
        CacheStat stat = new CacheStat();
        for (int index = 0; index < 100; ++ index) {
            Math.random();
        }
        double d = stat.tps(10);
        Assert.assertTrue(d >= 0);
    }

    @Test
    public void testData(){
        CacheStat stat = new CacheStat();
        stat.setCacheName("test");
        Assert.assertEquals("test", stat.getCacheName());

        stat.setTick(1L);
        Assert.assertEquals(1L, stat.getTick().longValue());

        stat.setEnd(2L);
        Assert.assertEquals(2L, stat.getEnd().longValue());

        stat.setSuccess(1L);
        Assert.assertEquals(1L, stat.getSuccess());

        stat.setTotal(1L);
        Assert.assertEquals(1L, stat.getTotal());

        stat.setFailure(1L);
        Assert.assertEquals(1L, stat.getFailure());

        stat.setAverage(1.0);
        Assert.assertEquals(1.0, stat.getAverage(), 0.1);

        stat.setAverageSuccess(1.1);
        Assert.assertEquals(1.1, stat.getAverageSuccess(), 0.1);

        stat.setAverageFailure(1.1);
        Assert.assertEquals(1.1, stat.getAverageFailure(), 0.1);

        stat.setUsedTotal(1.1);
        Assert.assertEquals(1.1, stat.getUsedTotal(), 0.1);

        stat.setUsedSuccess(1.1);
        Assert.assertEquals(1.1, stat.getUsedSuccess(), 0.1);

        stat.setUsedFailure(1.1);
        Assert.assertEquals(1.1, stat.getUsedFailure(), 0.1);
    }
}
