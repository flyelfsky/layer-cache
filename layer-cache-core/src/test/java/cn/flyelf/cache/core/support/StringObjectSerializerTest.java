package cn.flyelf.cache.core.support;

import cn.flyelf.cache.core.model.CacheSerializer;
import cn.flyelf.cache.core.model.CacheTestModel;
import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;

/**
 * 字符串序列化测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class StringObjectSerializerTest {
    private static StringObjectSerializer serializer = (StringObjectSerializer) CacheSerializer.UTF8;

    @Test
    public void testWithClass(){
        CacheTestModel model = new CacheTestModel();
        ByteBuffer buffer = serializer.encode(model);
        Assert.assertNotNull(buffer);
        CacheTestModel result = serializer.decode(buffer, CacheTestModel.class);
        Assert.assertNotNull(result);
        Assert.assertTrue(result.getName().startsWith("CacheTestModel"));
    }

    @Test(expected = UnsupportedClassVersionError.class)
    public void testUnwrapExp(){
        serializer.unwrap(null, CacheTestModel.class);
    }
}
