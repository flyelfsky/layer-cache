[![Coverage](http://sonar.flyelf.cn/api/project_badges/measure?project=cn.flyelf.cache%3Alayer-cache&metric=coverage)](http://sonar.flyelf.cn/dashboard?id=cn.flyelf.cache%3Alayer-cache)
[![License](https://img.shields.io/badge/license-Apache%202-4EB1BA.svg)](https://www.apache.org/licenses/LICENSE-2.0.html)

# 简介
Layer-Cache是一个基于Java的缓存系统封装，提供统一的API和注解来简化缓存的使用。 Layer-Cache提供了原生的支持TTL、多级缓存、缓存穿透、雪崩的处理，还提供了Cache接口用于手工缓存操作。
当前有两个实现，RedisCache、CaffeineCache(in memory)，要添加新的实现也是非常简单的。

全部特性:
* 通过统一的API访问Cache系统
* 通过注解创建并配置Cache实例，支持TTL和多级缓存
* 针对所有Cache实例缓存的自动统计
* Key的生成策略和Value的序列化策略当前仅支持utf8和fastjson进行序列化
* 响应式的Cache接口，redis使用lettuce
* Spring Boot支持（2.2.1.RELEASE）

要求:
* JDK1.8
* Spring Framework5.2.1+ (可选, 注解需要)
* Spring Boot2.2.1+ (可选)
Spring Boot为可选，需要2.2.1以上版本。如果不使用注解（仅使用layer-cache-core），Spring Framework也是可选的，此时使用方式与Guava/Caffeinecache类似。

更多文档访问 [wiki](https://gitlab.com/flyelfsky/layer-cache/-/wikis/home)

# 快速入门

## 缓存接口
通过注解```@CacheBean```创建一个 ```Cache``` 实例Bean:
```java
@CacheBean(layer = {CacheConstant.LAYER_CAFFEINE,CacheConstant.LAYER_REDIS})
private Cache<Long, UserDO> userCache;
@CacheBean
private ListCache<String, UserDO> userListcache;
```
上面的代码创建了一个```Cache``` 实例. ```layer = {CacheConstant.LAYER_CAFFEINE,CacheConstant.LAYER_REDIS}``` 定义了该实例使用两层缓存 (一级缓存为caffeine，二级缓存为redis) . 使用方式如下: 
```java
CacheResult<UserDO> user = userCache.get(12345L).block();
userCache.put(12345L, loadUserFromDataBase(12345L));
userCache.delete(12345L);

userCache.get(1234567L, (key) -> loadUserFromDataBase(1234567L));
```

或者也可以通过手动的方式创建```Cache``` 实例 :
```java
CacheLayerFactory factory = new CacheLayerFactory();
factory.setPenetration(new PenetrateCaffeineCache(new CachePenetrationConfig()));
RedisClient client = RedisClient.create("redis://127.0.0.1:6379/0");

CacheEventPublisher eventPublisher = new CacheEventPublisherImp();
RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
processor.addClient(CacheConstant.AREA_DEFAULT, client);
List<CacheLayerProcessor> layers = new ArrayList<>(1);
layers.add(processor);
factory.setLayers(layers);
CachePolicy policy = new CachePolicy();
policy.setObject(CacheTestModel.class);
policy.setLayer(new String[]{"redis"});
policy.setType(CacheConstant.CACHE_SIMPLE);
policy.duration("30m");
List<CachePolicy> policies = new ArrayList<>();
policies.add(policy);
...
factory.setPolicy(policies);
```

## Spring Boot集成配置

pom:
```xml
<dependency>
    <groupId>cn.flyelf.cache</groupId>
    <artifactId>layer-cache-starter</artifactId>
    <version>${layer.cache.latest.version}</version>
</dependency>
```

App class:
```java
@SpringBootApplication
@EnableCacheBeanAnnotation
public class MySpringBootApp {
    public static void main(String[] args) {
        SpringApplication.run(MySpringBootApp.class);
    }
}
```

spring boot application.yml config:
```yaml
layer-cache:
  penetration:
    enabled: true
    expire: 60000
  monitor:
    interval: 60
  redis:
    default:
      value-serializer: json
      key-serializer: json
      uri: redis://127.0.0.0:6379/0
      limit: 5
      expireAfterWriteInMillis: 10000
      pool:
        min-idle: 4
        max-idle: 16
      resources:
        io-thread: 4
  caffeine:
    default:
      limit: 2
      expireAfterWriteInMillis: 10000
```

## 更多文档
更多文档访问 [wiki](https://gitlab.com/flyelfsky/layer-cache/-/wikis/home)

# 版本变更
## 1.0.1
>1、hash支持单独添加一个map值，而不是整个map一起添加
>2、starter支持生成原生RedissonClient和RedisTemplate