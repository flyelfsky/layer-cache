package cn.flyelf.cache.redis.simple;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.cache.DefaultSimpleCache;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.penetration.caffeine.PenetrateCaffeineCache;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.redis.model.EmbedRedisService;
import cn.flyelf.cache.redis.model.UnitRedisService;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * redis简单对象缓存的单元测试
 *
 * @author wujr
 * 2020/1/3
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/3 1.0 新增]
 */
public class RedisCacheSimpleActionTest {
    private static UnitRedisService redisServer;
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static CacheEventPublisher eventPublisher = new CacheEventPublisherImp();
    private static RedisCacheLayerProcessor processor;
    private static final String[] layer = new String[]{"redis"};

    @BeforeClass
    public static void setUp() throws IOException{
        redisServer = new EmbedRedisService();
        redisServer.start();

        CachePenetrationConfig penetrationConfig = new CachePenetrationConfig();
        penetrationConfig.setEnabled(false);
        factory.setPenetration(new PenetrateCaffeineCache(penetrationConfig));

        processor = new RedisCacheLayerProcessor(eventPublisher);

        CacheLayerConfig config = new CacheLayerConfig();
        config.setUri(redisServer.uri());
        processor.buildAreaClient(CacheConstant.AREA_DEFAULT, config);
        List<CacheLayerProcessor> layers = new ArrayList<>(1);
        layers.add(processor);
        factory.setLayers(layers);
        initPolicy(factory);
    }
    private static void initPolicy(CacheLayerFactory factory) {
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        policy.setFeature("simple");
        List<CachePolicy> policies = new ArrayList<>();
        policies.add(policy);

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        policy.setFeature("null");
        policy.setCacheNull(true);
        policies.add(policy);

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        policy.setFeature("timeout");
        policy.setDuration(20L);
        policy.setTimeUnit(TimeUnit.SECONDS);
        policy.setCacheNull(true);
        policies.add(policy);

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        policy.setFeature("load");
        policy.setDuration(20L);
        policy.setTimeUnit(TimeUnit.SECONDS);
        policy.setCacheNull(true);
        policies.add(policy);

        factory.setPolicy(policies);
    }
    @AfterClass
    public static void tearDown(){
        processor.shutdown();
        if (redisServer != null){
            redisServer.stop();
        }
    }

    @Test
    public void testGetNoExists(){
        DefaultSimpleCache<Long, CacheTestModel> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, "simple");
        Mono<CacheResult<CacheTestModel>> getResult = cache.get(101L);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testGetNull(){
        DefaultSimpleCache<Long, CacheTestModel> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, "simple");
        Mono<CacheResult<Boolean>> putResult = cache.put(100L, null);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<CacheTestModel>> getResult = cache.get(100L);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
    @Test
    public void testPutAndGet(){
        DefaultSimpleCache<Long, CacheTestModel> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, "simple");
        CacheTestModel model = new CacheTestModel();
        model.setId(102L);
        model.setName("model102");
        model.setDate(new Date());
        Mono<CacheResult<Boolean>> putResult = cache.put(102L, model);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<CacheTestModel>> getResult = cache.get(102L);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && 102L == r.getValue().getId()).verifyComplete();
    }
    @Test
    public void testPutAndGetList(){
        DefaultSimpleCache<Long, List<CacheTestModel>> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, ArrayList.class,"simple");
        CacheTestModel model = new CacheTestModel();
        model.setId(102L);
        model.setName("model102");
        model.setDate(new Date());
        List<CacheTestModel> list = new ArrayList<>(2);
        list.add(model);
        model = new CacheTestModel();
        model.setId(103L);
        model.setName("model103");
        model.setIndex(1);
        list.add(model);
        Mono<CacheResult<Boolean>> putResult = cache.put(103L, list);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(103L);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
    @Test
    public void testGetLoad(){
        CacheTestModel model = new CacheTestModel();
        model.setId(104L);
        model.setName("model104");
        model.setDate(new Date());

        DefaultSimpleCache<Long, CacheTestModel> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, "simple");
        Mono<CacheResult<CacheTestModel>> getResult = cache.get(104L, k -> model);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && 104L == r.getValue().getId()).verifyComplete();
    }
    @Test
    public void testGetLoadNull(){
        DefaultSimpleCache<Long, CacheTestModel> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, "simple");
        Mono<CacheResult<CacheTestModel>> getResult = cache.get(105L, k -> null);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testGetLoadNullAndCache(){
        DefaultSimpleCache<Long, CacheTestModel> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, "null");
        Mono<CacheResult<CacheTestModel>> getResult = cache.get(106L, k -> null);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }

    @Test
    public void testPutTimeout(){
        DefaultSimpleCache<Long, CacheTestModel> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, "timeout");
        CacheTestModel model = new CacheTestModel();
        model.setId(107L);
        model.setName("model107");
        model.setDate(new Date());
        Mono<CacheResult<Boolean>> putResult = cache.put(107L, model);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
    @Test
    public void testGetLoadTimeout(){
        CacheTestModel model = new CacheTestModel();
        model.setId(108L);
        model.setName("model108");
        model.setDate(new Date());

        DefaultSimpleCache<Long, CacheTestModel> cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class, "load");
        Mono<CacheResult<CacheTestModel>> getResult = cache.get(108L, k -> model);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
}
