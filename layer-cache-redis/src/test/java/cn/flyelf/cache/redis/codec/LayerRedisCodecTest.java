package cn.flyelf.cache.redis.codec;

import cn.flyelf.cache.core.model.CacheSerializer;
import org.junit.Assert;
import org.junit.Test;

/**
 * 编解码的单元测试
 *
 * @author wujr
 * 2020-01-03
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-03 1.0 新增]
 */
public class LayerRedisCodecTest {
    @Test
    public void testHashCode(){
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        int v = codec.hashCode();
        Assert.assertNotEquals(0, v);
    }

    @Test
    public void testDecodeKey(){
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        long k = codec.decodeKey(codec.encodeKey(20L));
        Assert.assertEquals(20L, k);
    }

    @Test
    public void testEqualThis(){
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        Object object = codec;
        Assert.assertEquals(codec, object);
    }
    @Test
    public void testEqualNot(){
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        Object object = new Object();
        Assert.assertNotEquals(codec, object);
    }
    @Test
    public void testEqual(){
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        LayerRedisCodec<Long, String> codec2 = new LayerRedisCodec<>(Long.class, String.class);
        Assert.assertEquals(codec, codec2);
    }
    @Test
    public void testEqualNo(){
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        LayerRedisCodec<Long, Long> codec2 = new LayerRedisCodec<>(Long.class, Long.class, CacheSerializer.DEFAULT, CacheSerializer.DEFAULT);
        Assert.assertNotEquals(codec, codec2);
    }
}
