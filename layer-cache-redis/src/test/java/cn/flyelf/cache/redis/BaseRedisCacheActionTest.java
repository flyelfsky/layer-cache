package cn.flyelf.cache.redis;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.context.CacheContext;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheRequest;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.server.CacheDefaultExchange;
import cn.flyelf.cache.core.server.CacheExchange;
import io.lettuce.core.AbstractRedisClient;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.cluster.api.reactive.RedisAdvancedClusterReactiveCommands;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.concurrent.TimeUnit;


/**
 * redis与动作无关的测试用例
 *
 * @author wujr
 * 2020-01-04
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-04 1.0 新增]
 */
public class BaseRedisCacheActionTest {
    private static CachePolicy policy;

    @BeforeClass
    public static void setUp() {
        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(new String[]{"redis"});
        policy.setType(CacheConstant.CACHE_SIMPLE);
    }
    @AfterClass
    public static void tearDown(){
    }

    @Test
    public void testOnLoadAfterCodecNull(){
        AbstractRedisClient client = Mockito.mock(AbstractRedisClient.class);
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        Mockito.when(processor.getClient("test")).thenReturn(client);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        Mono<Boolean> result = action.onLoadAfter(null, null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(Boolean.TRUE).verifyComplete();
    }
    @Test
    public void testOnLoadAfterClientNull(){
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "default");
        Mono<Boolean> result = action.onLoadAfter(null, null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(Boolean.TRUE).verifyComplete();
    }
    @Test
    public void testOnLoadAfterConnNull(){
        AbstractRedisClient client = Mockito.mock(AbstractRedisClient.class);
        CacheRequest<Long, CacheTestModel> request = new CacheRequest<>(101L, CacheTestModel.class);
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        Mockito.when(processor.getClient("test")).thenReturn(client);
        Mockito.when(processor.getConnection(Mockito.any(), Mockito.any())).thenReturn(null);
        CacheExchange<Long, CacheTestModel, Long> exchange = new CacheDefaultExchange<>(request, policy);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        action.initCodec(exchange);
        Mono<Boolean> result = action.onLoadAfter(exchange, null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(Boolean.TRUE).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testOnLoadAfterNullCommand(){
        AbstractRedisClient client = Mockito.mock(AbstractRedisClient.class);
        CacheRequest<Long, CacheTestModel> request = new CacheRequest<>(101L, CacheTestModel.class);
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        Mockito.when(processor.getClient("test")).thenReturn(client);
        StatefulConnection<Long, CacheTestModel> connection = Mockito.mock(StatefulConnection.class);
        Mockito.when(processor.getConnection(Mockito.any(), Mockito.any())).thenReturn(connection);
        CacheExchange<Long, CacheTestModel, Long> exchange = new CacheDefaultExchange<>(request, policy);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        action.initCodec(exchange);
        Mono<Boolean> result = action.onLoadAfter(exchange, null);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(Boolean.TRUE).verifyComplete();
    }
    @Test
    public void testInitContextNullConnection(){
        AbstractRedisClient client = Mockito.mock(AbstractRedisClient.class);
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        Mockito.when(processor.getClient("test")).thenReturn(client);
        Mockito.when(processor.getConnection(Mockito.any(), Mockito.any())).thenReturn(null);
        CacheRequest<Long, CacheTestModel> request = new CacheRequest<>(101L, CacheTestModel.class);
        CacheExchange<Long, CacheTestModel, Long> exchange = new CacheDefaultExchange<>(request, policy);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        CacheContext context = action.initContext(exchange);
        Assert.assertNull(context);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testInitContextNullCommand(){
        AbstractRedisClient client = Mockito.mock(AbstractRedisClient.class);
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        Mockito.when(processor.getClient("test")).thenReturn(client);
        StatefulConnection<Long, CacheTestModel> connection = Mockito.mock(StatefulConnection.class);
        Mockito.when(processor.getConnection(Mockito.any(), Mockito.any())).thenReturn(connection);
        CacheRequest<Long, CacheTestModel> request = new CacheRequest<>(101L, CacheTestModel.class);
        CacheExchange<Long, CacheTestModel, Long> exchange = new CacheDefaultExchange<>(request, policy);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        CacheContext context = action.initContext(exchange);
        Assert.assertNull(context);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testReactiveCluster(){
        AbstractRedisClient client = Mockito.mock(RedisClusterClient.class);
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        Mockito.when(processor.getClient("test")).thenReturn(client);
        StatefulConnection<Long, CacheTestModel> connection = Mockito.mock(StatefulRedisClusterConnection.class);
        RedisAdvancedClusterReactiveCommands command = Mockito.mock(RedisAdvancedClusterReactiveCommands.class);
        Mockito.when(processor.getConnection(Mockito.any(), Mockito.any())).thenReturn(connection);
        Mockito.when(((StatefulRedisClusterConnection)connection).reactive()).thenReturn(command);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        Object object = action.reactive(connection);
        Assert.assertNotNull(object);
        Assert.assertTrue(object instanceof RedisAdvancedClusterReactiveCommands);
        Assert.assertEquals(command, object);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testExpireClusterMill(){
        AbstractRedisClient client = Mockito.mock(RedisClusterClient.class);
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        Mockito.when(processor.getClient("test")).thenReturn(client);
        StatefulConnection<Long, CacheTestModel> connection = Mockito.mock(StatefulRedisClusterConnection.class);
        RedisAdvancedClusterReactiveCommands command = Mockito.mock(RedisAdvancedClusterReactiveCommands.class);
        Mono<Boolean> expire = Mono.just(true);
        Mockito.when(command.pexpire(Mockito.any(), Mockito.anyLong())).thenReturn(expire);
        Mockito.when(processor.getConnection(Mockito.any(), Mockito.any())).thenReturn(connection);
        Mockito.when(((StatefulRedisClusterConnection)connection).reactive()).thenReturn(command);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        CacheRequest<Long, CacheTestModel> request = new CacheRequest<>(101L, CacheTestModel.class);
        CachePolicy policy = new CachePolicy();
        policy.setDuration(10L);
        policy.setTimeUnit(TimeUnit.MILLISECONDS);
        policy.setObject(CacheTestModel.class);
        policy.setLayer(new String[]{"redis"});
        CacheExchange<Long, CacheTestModel, Long> exchange = new CacheDefaultExchange<>(request, policy);
        Mono<Long> result = action.expire(command, exchange, 1L);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(1L).verifyComplete();
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testExpireClusterSecond(){
        AbstractRedisClient client = Mockito.mock(RedisClusterClient.class);
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        Mockito.when(processor.getClient("test")).thenReturn(client);
        StatefulConnection<Long, CacheTestModel> connection = Mockito.mock(StatefulRedisClusterConnection.class);
        RedisAdvancedClusterReactiveCommands command = Mockito.mock(RedisAdvancedClusterReactiveCommands.class);
        Mono<Boolean> expire = Mono.just(true);
        Mockito.when(command.expire(Mockito.any(), Mockito.anyLong())).thenReturn(expire);
        Mockito.when(processor.getConnection(Mockito.any(), Mockito.any())).thenReturn(connection);
        Mockito.when(((StatefulRedisClusterConnection)connection).reactive()).thenReturn(command);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        CacheRequest<Long, CacheTestModel> request = new CacheRequest<>(101L, CacheTestModel.class);
        CachePolicy policy = new CachePolicy();
        policy.setDuration(10L);
        policy.setTimeUnit(TimeUnit.SECONDS);
        policy.setObject(CacheTestModel.class);
        policy.setLayer(new String[]{"redis"});
        CacheExchange<Long, CacheTestModel, Long> exchange = new CacheDefaultExchange<>(request, policy);
        Mono<Long> result = action.expire(command, exchange, 1L);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(1L).verifyComplete();
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testHasKeyCluster(){
        RedisAdvancedClusterReactiveCommands command = Mockito.mock(RedisAdvancedClusterReactiveCommands.class);
        Mockito.when(command.exists(Mockito.any())).thenReturn(Mono.just(1L));
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        RedisCacheDeleteAction<Long, CacheTestModel> action = new RedisCacheDeleteAction<>(processor, "test");
        Mono<Boolean> result = action.hasKey(command, 1L);
        Assert.assertNotNull(result);
        StepVerifier.create(result).expectNext(Boolean.TRUE).verifyComplete();
    }
}
