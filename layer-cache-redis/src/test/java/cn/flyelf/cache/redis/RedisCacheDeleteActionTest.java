package cn.flyelf.cache.redis;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.cache.DefaultSimpleCache;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.penetration.caffeine.PenetrateCaffeineCache;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.redis.model.EmbedRedisService;
import cn.flyelf.cache.redis.model.UnitRedisService;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * redis的删除动作的单元测试
 *
 * @author wujr
 * 2020-01-03
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-03 1.0 新增]
 */
public class RedisCacheDeleteActionTest {
    private static CacheTestModel model, model2;
    private static UnitRedisService redisServer;
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static CacheEventPublisher eventPublisher = new CacheEventPublisherImp();
    private static RedisCacheLayerProcessor processor;
    private static DefaultSimpleCache<Long, CacheTestModel> cache;
    private static final String[] layer = new String[]{"redis"};

    @BeforeClass
    public static void setUp() throws IOException {
        redisServer = new EmbedRedisService();
        redisServer.start();

        CachePenetrationConfig penetrationConfig = new CachePenetrationConfig();
        penetrationConfig.setEnabled(false);
        factory.setPenetration(new PenetrateCaffeineCache(penetrationConfig));

        processor = new RedisCacheLayerProcessor(eventPublisher);

        CacheLayerConfig config = new CacheLayerConfig();
        config.setUri(redisServer.uri());
        processor.buildAreaClient(CacheConstant.AREA_DEFAULT, config);
        List<CacheLayerProcessor> layers = new ArrayList<>(1);
        layers.add(processor);
        factory.setLayers(layers);
        initPolicy(factory);

        model = new CacheTestModel();
        model.setId(1L);
        model.setDate(new Date());
        model.setName("model1");
        model.setIndex(1);

        model2 = new CacheTestModel();
        model2.setId(2L);
        model2.setName("model2");
        model2.setIndex(2);

        cache = new DefaultSimpleCache<>(factory, layer, Long.class, CacheTestModel.class);
    }
    private static void initPolicy(CacheLayerFactory factory) {
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        List<CachePolicy> policies = new ArrayList<>();
        policies.add(policy);

        factory.setPolicy(policies);
    }
    @AfterClass
    public static void tearDown(){
        processor.shutdown();
        if (redisServer != null){
            redisServer.stop();
        }
    }
    @Test
    public void testDeleteByList(){
        long key = 201L;
        Mono<CacheResult<Boolean>> putResult = cache.put(key, model2);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        key = 202L;
        putResult = cache.put(key, model);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        List<Long> keys = new ArrayList<>(2);
        keys.add(201L);
        keys.add(202L);
        Mono<CacheResult<Long>> delResult = cache.delete(keys);
        Assert.assertNotNull(delResult);
        StepVerifier.create(delResult).expectNextMatches(r -> r.isSuccess() && 2L == r.getValue()).verifyComplete();

        Mono<CacheResult<CacheTestModel>> getResult = cache.get(key);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testDeleteByArray(){
        long key = 301L;
        Mono<CacheResult<Boolean>> putResult = cache.put(key, model2);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        key = 302L;
        putResult = cache.put(key, model);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<Long>> delResult = cache.delete(301L, 302L);
        Assert.assertNotNull(delResult);
        StepVerifier.create(delResult).expectNextMatches(r -> r.isSuccess() && 2L == r.getValue()).verifyComplete();

        Mono<CacheResult<CacheTestModel>> getResult = cache.get(key);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }

    @Test
    public void testDeleteOne(){
        long key = 401L;
        Mono<CacheResult<Boolean>> putResult = cache.put(key, model2);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        key = 402L;
        putResult = cache.put(key, model);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<Long>> delResult = cache.delete(401L);
        Assert.assertNotNull(delResult);
        StepVerifier.create(delResult).expectNextMatches(r -> r.isSuccess() && 1L == r.getValue()).verifyComplete();

        Mono<CacheResult<CacheTestModel>> getResult = cache.get(401L);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();

        getResult = cache.get(402L);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
}
