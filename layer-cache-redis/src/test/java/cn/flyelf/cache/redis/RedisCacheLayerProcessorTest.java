package cn.flyelf.cache.redis;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheConfigException;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.redis.codec.LayerRedisCodec;
import cn.flyelf.cache.redis.util.RedisUtil;
import io.lettuce.core.AbstractRedisClient;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * redis的缓存处理器的测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class RedisCacheLayerProcessorTest {
    private static CacheEventPublisher eventPublisher = new CacheEventPublisherImp();

    @Test(expected = CacheConfigException.class)
    public void testGetConnectionUnsupported(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        AbstractRedisClient client = Mockito.mock(AbstractRedisClient.class);
        processor.addClient("default", client);
        LayerRedisCodec codec = Mockito.mock(LayerRedisCodec.class);
        processor.getConnection(codec, "default");
        Assert.assertTrue(true);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testGetConnectionCluster(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        RedisClusterClient client = Mockito.mock(RedisClusterClient.class);
        StatefulRedisClusterConnection conn = Mockito.mock(StatefulRedisClusterConnection.class);
        Mockito.when(client.connect(Mockito.any())).thenReturn(conn);
        processor.addClient("default", client);
        LayerRedisCodec codec = Mockito.mock(LayerRedisCodec.class);
        StatefulConnection connection = processor.getConnection(codec, "default");
        Assert.assertNotNull(connection);
        Assert.assertTrue(connection instanceof StatefulRedisClusterConnection);
    }
    @Test
    public void testGetConnectionClusterNull(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        RedisClusterClient client = Mockito.mock(RedisClusterClient.class);
        Mockito.when(client.connect(Mockito.any())).thenReturn(null);
        processor.addClient("default", client);
        LayerRedisCodec codec = Mockito.mock(LayerRedisCodec.class);
        StatefulConnection connection = processor.getConnection(codec, "default");
        Assert.assertNull(connection);
    }

    @Test
    public void testBuildConfigIdleNull(){
        CacheLayerConfig config = new CacheLayerConfig();
        CacheLayerConfig.Pool pool = new CacheLayerConfig.Pool();
        pool.setMaxActive(8);
        pool.setMaxWait(100L);
        config.setPool(pool);
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        GenericObjectPoolConfig result = RedisUtil.buildPoolConfig(config);
        Assert.assertNotNull(result);
        Assert.assertEquals(8, result.getMaxTotal());
        Assert.assertEquals(100L, result.getMaxWaitMillis());
    }
    @Test
    public void testBuildConfigIdleValue(){
        CacheLayerConfig config = new CacheLayerConfig();
        CacheLayerConfig.Pool pool = new CacheLayerConfig.Pool();
        pool.setMaxIdle(10);
        pool.setMinIdle(8);
        pool.setMaxWait(null);
        config.setPool(pool);
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        GenericObjectPoolConfig result = RedisUtil.buildPoolConfig(config);
        Assert.assertNotNull(result);
        Assert.assertEquals(8, result.getMinIdle());
        Assert.assertEquals(10, result.getMaxIdle());
    }

    @Test
    public void testReleaseConnectionNullPool(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        StatefulRedisConnection connection = Mockito.mock(StatefulRedisConnection.class);
        boolean r = processor.releaseConnection(codec, "default", connection);
        Assert.assertFalse(r);
    }
    @Test
    public void testReleaseConnectionClusterNull(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        StatefulRedisClusterConnection connection = Mockito.mock(StatefulRedisClusterConnection.class);
        boolean r = processor.releaseConnection(codec, "default", connection);
        Assert.assertFalse(r);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testReleaseConnectionClusterTrue(){
        RedisClusterClient client = Mockito.mock(RedisClusterClient.class);
        StatefulRedisClusterConnection conn = Mockito.mock(StatefulRedisClusterConnection.class);
        Mockito.when(client.connect(Mockito.any())).thenReturn(conn);
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        processor.addClient("default", client);
        LayerRedisCodec<Long, String> codec = new LayerRedisCodec<>(Long.class, String.class);
        StatefulRedisClusterConnection connection = processor.getConnection(codec, "default");
        boolean r = processor.releaseConnection(codec, "default", connection);
        Assert.assertTrue(r);
    }

    @Test
    public void testAddClientNotDefault(){
        RedisClusterClient client = Mockito.mock(RedisClusterClient.class);
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        processor.addClient("test", client);
        Assert.assertTrue(true);
    }

    @Test
    public void testActionNull(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        CacheAction action = processor.action(ACTION.NONE, "default");
        Assert.assertNull(action);
    }

    @Test(expected = CacheConfigException.class)
    public void testBuildAreaClientUriNull(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        CacheLayerConfig config = new CacheLayerConfig();
        processor.buildAreaClient("default", config);
        Assert.assertTrue(true);
    }
    @Test(expected = CacheConfigException.class)
    public void testBuildAreaClientUriEmpty(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        CacheLayerConfig config = new CacheLayerConfig();
        config.setUri(new String[0]);
        processor.buildAreaClient("default", config);
        Assert.assertTrue(true);
    }
    @Test
    public void testBuildAreaClientResourceIoNull(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        CacheLayerConfig config = new CacheLayerConfig();
        config.setUri(new String[]{"redis://127.0.0.1:6379/0", "redis://127.0.0.1:6380/0"});
        CacheLayerConfig.Resources resources = new CacheLayerConfig.Resources();
        resources.setComputeThread(2);
        config.setResources(resources);
        processor.buildAreaClient("default", config);
        Assert.assertTrue(true);
    }
    @Test
    public void testBuildAreaClientResourceComputeNull(){
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(eventPublisher);
        CacheLayerConfig config = new CacheLayerConfig();
        config.setUri(new String[]{"redis://127.0.0.1:6379/0", "redis://127.0.0.1:6380/0"});
        CacheLayerConfig.Resources resources = new CacheLayerConfig.Resources();
        resources.setIoThread(2);
        config.setResources(resources);
        processor.buildAreaClient("default", config);
        Assert.assertTrue(true);
    }
}
