package cn.flyelf.cache.redis;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.cache.DefaultListCache;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.penetration.caffeine.PenetrateCaffeineCache;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.redis.list.RedisCacheListPutAction;
import cn.flyelf.cache.redis.model.EmbedRedisService;
import cn.flyelf.cache.redis.model.UnitRedisService;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * redis缓存list类型数据的单元测试
 *
 * @author wujr
 * 2020-01-03
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-03 1.0 新增]
 */
public class RedisCacheListActionTest {
    private static List<CacheTestModel> models = new ArrayList<>(2);
    private static List<CacheTestModel> models2 = new ArrayList<>(2);
    private static UnitRedisService redisServer;
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static CacheEventPublisher eventPublisher = new CacheEventPublisherImp();
    private static RedisCacheLayerProcessor processor;
    private static DefaultListCache<Long, CacheTestModel> cache;
    private static final String[] layer = new String[]{"redis"};

    @BeforeClass
    public static void setUp() throws IOException {
        redisServer = new EmbedRedisService();
        redisServer.start();

        CachePenetrationConfig penetrationConfig = new CachePenetrationConfig();
        penetrationConfig.setEnabled(false);
        factory.setPenetration(new PenetrateCaffeineCache(penetrationConfig));

        processor = new RedisCacheLayerProcessor(eventPublisher);

        CacheLayerConfig config = new CacheLayerConfig();
        config.setUri(redisServer.uri());
        processor.buildAreaClient(CacheConstant.AREA_DEFAULT, config);
        List<CacheLayerProcessor> layers = new ArrayList<>(1);
        layers.add(processor);
        factory.setLayers(layers);
        initPolicy(factory);

        cache = new DefaultListCache<>(factory, layer, Long.class, CacheTestModel.class);
        new RedisCacheListPutAction<String, CacheTestModel>(processor, "default");

        CacheTestModel model = new CacheTestModel();
        model.setId(1L);
        model.setDate(new Date());
        model.setName("model1");
        model.setIndex(1);
        models.add(model);

        model = new CacheTestModel();
        model.setId(2L);
        model.setDate(new Date());
        model.setName("model2");
        model.setIndex(2);
        models.add(model);

        model = new CacheTestModel();
        model.setId(3L);
        model.setDate(new Date());
        model.setName("model3");
        model.setIndex(3);
        models2.add(model);

        model = new CacheTestModel();
        model.setId(4L);
        model.setDate(new Date());
        model.setName("model4");
        model.setIndex(4);
        models2.add(model);
    }
    private static void initPolicy(CacheLayerFactory factory) {
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_LIST);
        List<CachePolicy> policies = new ArrayList<>();
        policies.add(policy);

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_LIST);
        policy.setDuration(20L);
        policy.setTimeUnit(TimeUnit.SECONDS);
        policy.setFeature("second");
        policies.add(policy);

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_LIST);
        policy.setDuration(20L);
        policy.setTimeUnit(TimeUnit.MILLISECONDS);
        policy.setFeature("mill");
        policies.add(policy);

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_LIST);
        policy.setDuration(20L);
        policy.setTimeUnit(TimeUnit.MICROSECONDS);
        policy.setFeature("micro");
        policies.add(policy);

        policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_LIST);
        policy.setDuration(20L);
        policy.setTimeUnit(TimeUnit.NANOSECONDS);
        policy.setFeature("nano");
        policies.add(policy);

        factory.setPolicy(policies);
    }
    @AfterClass
    public static void tearDown(){
        processor.shutdown();
        if (redisServer != null){
            redisServer.stop();
        }
    }
    @Test
    public void testPutSecond(){
        DefaultListCache<Long, CacheTestModel> cache2 = new DefaultListCache<>(factory, layer, Long.class, CacheTestModel.class, "second");
        Mono<CacheResult<Boolean>> putResult = cache2.put(101L, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
    @Test
    public void testPutMill(){
        DefaultListCache<Long, CacheTestModel> cache2 = new DefaultListCache<>(factory, layer, Long.class, CacheTestModel.class, "mill");
        Mono<CacheResult<Boolean>> putResult = cache2.put(102L, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
    @Test
    public void testPutMicro(){
        DefaultListCache<Long, CacheTestModel> cache2 = new DefaultListCache<>(factory, layer, Long.class, CacheTestModel.class, "micro");
        Mono<CacheResult<Boolean>> putResult = cache2.put(103L, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
    @Test
    public void testPutNano(){
        DefaultListCache<Long, CacheTestModel> cache2 = new DefaultListCache<>(factory, layer, Long.class, CacheTestModel.class, "nano");
        Mono<CacheResult<Boolean>> putResult = cache2.put(104L, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
    @Test
    public void testGetNoKey(){
        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(201L);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testPutAndGet(){
        Mono<CacheResult<Boolean>> putResult = cache.put(201L, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(201L);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && 2 == r.getValue().size()).verifyComplete();
    }
    @Test
    public void testPutNull(){
        Mono<CacheResult<Boolean>> putResult = cache.put(301L, null);
        StepVerifier.create(putResult).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    public void testPutEmpty(){
        Mono<CacheResult<Boolean>> putResult = cache.put(301L, new ArrayList<>());
        StepVerifier.create(putResult).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    public void testPushLeftAndPopRight(){
        Mono<CacheResult<Boolean>> putResult = cache.put(302L, models);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        List<CacheTestModel> list = new ArrayList<>(1);
        list.add(models2.get(0));
        putResult = cache.lpush(302L, list);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(r -> r.isSuccess() && r.getValue()).verifyComplete();

        Mono<CacheResult<CacheTestModel>> rpop = cache.rpop(302L);
        Assert.assertNotNull(rpop);
        StepVerifier.create(rpop).expectNextMatches(r -> r.isSuccess() && 1L == r.getValue().getId()).verifyComplete();
    }
    @Test
    public void testPushRightAndPopLeft(){
        Mono<CacheResult<Boolean>> putResult = cache.put(303L, models);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        List<CacheTestModel> list = new ArrayList<>(1);
        list.add(models2.get(0));
        putResult = cache.rpush(303L, list);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(r -> r.isSuccess() && r.getValue()).verifyComplete();

        Mono<CacheResult<CacheTestModel>> lpop = cache.lpop(303L);
        Assert.assertNotNull(lpop);
        StepVerifier.create(lpop).expectNextMatches(r -> r.isSuccess() && 2L == r.getValue().getId()).verifyComplete();
    }
    @Test
    public void testPushLeftAndPopRightArray(){
        Mono<CacheResult<Boolean>> putResult = cache.put(304L, models);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        putResult = cache.lpush(304L, models2.get(0));
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(r -> r.isSuccess() && r.getValue()).verifyComplete();

        Mono<CacheResult<CacheTestModel>> rpop = cache.rpop(304L);
        Assert.assertNotNull(rpop);
        StepVerifier.create(rpop).expectNextMatches(r -> r.isSuccess() && 1L == r.getValue().getId()).verifyComplete();
    }
    @Test
    public void testPushRightAndPopLeftArray(){
        Mono<CacheResult<Boolean>> putResult = cache.put(305L, models);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        putResult = cache.rpush(305L, models2.get(0));
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(r -> r.isSuccess() && r.getValue()).verifyComplete();

        Mono<CacheResult<CacheTestModel>> lpop = cache.lpop(305L);
        Assert.assertNotNull(lpop);
        StepVerifier.create(lpop).expectNextMatches(r -> r.isSuccess() && 2L == r.getValue().getId()).verifyComplete();
    }
    @Test
    public void testPushRightNull(){
        Mono<CacheResult<Boolean>> putResult = cache.rpush(306L, (CacheTestModel)null);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    public void testPushRightEmpty(){
        CacheTestModel[] v = new CacheTestModel[0];
        Mono<CacheResult<Boolean>> putResult = cache.rpush(306L, v);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    public void testPushLeftNull(){
        Mono<CacheResult<Boolean>> putResult = cache.lpush(307L, (CacheTestModel)null);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    public void testPushLeftEmpty(){
        CacheTestModel[] v = new CacheTestModel[0];
        Mono<CacheResult<Boolean>> putResult = cache.lpush(308L, v);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectError(InvalidCacheValueException.class).verify();
    }

    @Test
    public void testGetLoadNull(){
        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(309L, k -> null);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testGetLoadEmpty(){
        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(309L, k -> new ArrayList<>());
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testGetLoad(){
        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(310L, k -> models);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
}
