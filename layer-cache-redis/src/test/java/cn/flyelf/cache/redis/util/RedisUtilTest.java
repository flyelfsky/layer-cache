package cn.flyelf.cache.redis.util;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.SetArgs;
import org.apache.commons.lang3.reflect.FieldUtils;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.util.concurrent.TimeUnit;

/**
 * redis辅助工具的单元测试
 *
 * @author wujr
 * 2020/1/3
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/3 1.0 新增]
 */
public class RedisUtilTest {
    @Test
    public void testBuildSecondNull(){
        Assert.assertNull(RedisUtil.buildSecond(null));
    }
    @Test
    public void testBuildSecond() throws Exception {
        SetArgs args = RedisUtil.buildSecond(20L);
        Assert.assertNotNull(args);
        Field field = FieldUtils.getField(SetArgs.class, "ex", true);
        long actual = (long)FieldUtils.readField(field, args);
        Assert.assertEquals(20L, actual);
    }

    @Test(expected = UnsupportedClassVersionError.class)
    public void testActionConstructorException(){
        RedisUtil.actionConstructor(RedisUtilTest.class);
    }

    @Test
    public void testActionNull() throws Exception{
        Constructor constructor = RedisUtilTest.class.getDeclaredConstructor();
        RedisCacheLayerProcessor processor = new RedisCacheLayerProcessor(null);
        CacheAction action = RedisUtil.action(processor, constructor, "default");
        Assert.assertNull(action);
    }

    @Test
    public void testBuildArgNullDuration(){
        CachePolicy policy = new CachePolicy();
        SetArgs args = RedisUtil.build(policy);
        Assert.assertNull(args);
    }
    @Test
    public void testBuildArgNullUnit(){
        CachePolicy policy = new CachePolicy();
        policy.setDuration(10L);
        Assert.assertNull(RedisUtil.build(policy));
    }
    @Test
    public void testBuildArgMicro() throws Exception{
        CachePolicy policy = new CachePolicy();
        policy.setDuration(30000L);
        policy.setTimeUnit(TimeUnit.MICROSECONDS);
        SetArgs args = RedisUtil.build(policy);
        Assert.assertNotNull(args);
        Field field = FieldUtils.getField(SetArgs.class, "px", true);
        long actual = (long)FieldUtils.readField(field, args);
        Assert.assertEquals(30L, actual);
    }
    @Test
    public void testBuildArgMill() throws Exception{
        CachePolicy policy = new CachePolicy();
        policy.setDuration(300L);
        policy.setTimeUnit(TimeUnit.MILLISECONDS);
        SetArgs args = RedisUtil.build(policy);
        Assert.assertNotNull(args);
        Field field = FieldUtils.getField(SetArgs.class, "px", true);
        long actual = (long)FieldUtils.readField(field, args);
        Assert.assertEquals(300L, actual);
    }
    @Test
    public void testBuildArgNano() throws Exception{
        CachePolicy policy = new CachePolicy();
        policy.setDuration(30000000L);
        policy.setTimeUnit(TimeUnit.NANOSECONDS);
        SetArgs args = RedisUtil.build(policy);
        Assert.assertNotNull(args);
        Field field = FieldUtils.getField(SetArgs.class, "px", true);
        long actual = (long)FieldUtils.readField(field, args);
        Assert.assertEquals(30L, actual);
    }
    @Test
    public void testBuildArgSecond() throws Exception{
        CachePolicy policy = new CachePolicy();
        policy.setDuration(30L);
        policy.setTimeUnit(TimeUnit.SECONDS);
        SetArgs args = RedisUtil.build(policy);
        Assert.assertNotNull(args);
        Field field = FieldUtils.getField(SetArgs.class, "ex", true);
        long actual = (long)FieldUtils.readField(field, args);
        Assert.assertEquals(30L, actual);
    }
}
