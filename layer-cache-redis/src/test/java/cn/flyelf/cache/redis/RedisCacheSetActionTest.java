package cn.flyelf.cache.redis;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.cache.DefaultSetCache;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.penetration.caffeine.PenetrateCaffeineCache;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.redis.model.EmbedRedisService;
import cn.flyelf.cache.redis.model.UnitRedisService;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.io.IOException;
import java.util.*;

/**
 * redis的set类型的操作单元测试
 *
 * @author wujr
 * 2020-01-04
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-04 1.0 新增]
 */
public class RedisCacheSetActionTest {
    private static Set<CacheTestModel> models = new HashSet<>(2);
    private static UnitRedisService redisServer;
    private static CacheLayerFactory factory = new CacheLayerFactory();
    private static CacheEventPublisher eventPublisher = new CacheEventPublisherImp();
    private static RedisCacheLayerProcessor processor;
    private static DefaultSetCache<Long, CacheTestModel> cache;
    private static final String[] layer = new String[]{"redis"};

    @BeforeClass
    public static void setUp() throws IOException {
        redisServer = new EmbedRedisService();
        redisServer.start();

        CachePenetrationConfig penetrationConfig = new CachePenetrationConfig();
        penetrationConfig.setEnabled(false);
        factory.setPenetration(new PenetrateCaffeineCache(penetrationConfig));

        processor = new RedisCacheLayerProcessor(eventPublisher);

        CacheLayerConfig config = new CacheLayerConfig();
        config.setUri(redisServer.uri());
        processor.buildAreaClient(CacheConstant.AREA_DEFAULT, config);
        List<CacheLayerProcessor> layers = new ArrayList<>(1);
        layers.add(processor);
        factory.setLayers(layers);
        initPolicy(factory);

        cache = new DefaultSetCache<>(factory, layer, Long.class, CacheTestModel.class);

        CacheTestModel model = new CacheTestModel();
        model.setId(1L);
        model.setDate(new Date());
        model.setName("model1");
        model.setIndex(1);
        models.add(model);

        model = new CacheTestModel();
        model.setId(2L);
        model.setDate(new Date());
        model.setName("model2");
        model.setIndex(2);
        models.add(model);
    }
    private static void initPolicy(CacheLayerFactory factory) {
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(layer);
        policy.setType(CacheConstant.CACHE_SET);
        List<CachePolicy> policies = new ArrayList<>();
        policies.add(policy);

        factory.setPolicy(policies);
    }
    @AfterClass
    public static void tearDown(){
        processor.shutdown();
        if (redisServer != null){
            redisServer.stop();
        }
    }

    @Test
    public void testGetNoKey(){
        Mono<CacheResult<Set<CacheTestModel>>> getResult = cache.get(201L);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testPutAndGet(){
        Mono<CacheResult<Boolean>> putResult = cache.put(201L, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
        Mono<CacheResult<Set<CacheTestModel>>> getResult = cache.get(201L);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && 2 == r.getValue().size()).verifyComplete();
    }
    @Test
    public void testPutNull(){
        Mono<CacheResult<Boolean>> putResult = cache.put(301L, null);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    public void testPutEmpty(){
        Mono<CacheResult<Boolean>> putResult = cache.put(301L, new HashSet<>());
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectError(InvalidCacheValueException.class).verify();
    }
    @Test
    public void testGetLoadNull(){
        Mono<CacheResult<Set<CacheTestModel>>> getResult = cache.get(309L, k -> null);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testGetLoadEmpty(){
        Mono<CacheResult<Set<CacheTestModel>>> getResult = cache.get(309L, k -> new HashSet<>());
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }
    @Test
    public void testGetLoad(){
        Mono<CacheResult<Set<CacheTestModel>>> getResult = cache.get(310L, k -> models);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();
    }
}
