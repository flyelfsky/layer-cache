package cn.flyelf.cache.redis;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.AbstractCacheAction;
import cn.flyelf.cache.core.context.CacheContext;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.codec.LayerRedisCodec;
import cn.flyelf.cache.redis.context.RedisCacheContext;
import io.lettuce.core.AbstractRedisClient;
import io.lettuce.core.RedisClient;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.api.reactive.RedisReactiveCommands;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.cluster.api.reactive.RedisAdvancedClusterReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.concurrent.TimeUnit;

/**
 * redis的基本抽象
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * @param <R>: 缓存动作的返回类型
 * @param <C>: redis的缓存指令类型
 *
 * @author wujr
 * 2019-12-19
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-19 1.0 新增]
 */
@Slf4j
public abstract class BaseRedisCacheAction<K, V, R, C> extends AbstractCacheAction<K, V, R, RedisCacheLayerProcessor> {
    private LayerRedisCodec<K, V> codec;
    private AbstractRedisClient client;

    public BaseRedisCacheAction(ACTION action, RedisCacheLayerProcessor processor, String area){
        super(action, processor, area);
        client = layerProcessor.getClient(area);
    }

    @Override
    public Mono<Boolean> onLoadAfter(CacheExchange<K, V, R> exchange, V value){
        if (null == client || null == codec){
            return super.onLoadAfter(exchange, value);
        }
        StatefulConnection<K, V> connection = processor().getConnection(codec, area);
        if (null == connection){
            log.error("redis回填onLoadAfter，无法连接到redis，处理失败");
            return super.onLoadAfter(exchange, value);
        }
        C commands = reactive(connection);
        if (null != commands){
            return doOnLoadAfter(commands, exchange, value)
                    .doOnSuccess(b -> processor().releaseConnection(codec, area, connection))
                    .doOnError(t -> processor().releaseConnection(codec, area, connection));
        }
        return super.onLoadAfter(exchange, value);
    }

    protected Mono<Boolean> doOnLoadAfter(C commands, CacheExchange<K, V, R> exchange, V value){
        log.debug("commands = {}, exchange = {}, value = {}", commands, exchange, value);
        return Mono.just(true);
    }

    @SuppressWarnings("unchecked")
    protected Mono<Boolean> hasKey(C commands, K key){
        if (commands instanceof RedisAdvancedClusterReactiveCommands){
            return ((RedisAdvancedClusterReactiveCommands<K, ?>) commands).exists(key).map(r -> r.equals(1L));
        }else{
            return ((RedisReactiveCommands<K, ?>) commands).exists(key).map(r -> r.equals(1L));
        }
    }

    @Override
    protected CacheContext initContext(CacheExchange<K, V, R> exchange){
        initCodec(exchange);
        StatefulConnection<?, ?> connection = processor().getConnection(codec, area);
        if (null == connection){
            return null;
        }
        C commands = reactive(connection);
        if (null == commands){
            return null;
        }
        return new RedisCacheContext<>(commands, connection);
    }

    @Override
    protected void destroyContext(){
        RedisCacheContext<?> redisContext = (RedisCacheContext<?>)cacheContext;
        processor().releaseConnection(codec, area, redisContext.getConnection());
    }

    @SuppressWarnings("unchecked")
    protected C reactive(StatefulConnection<?, ?> connection){
        if (client instanceof RedisClient){
            StatefulRedisConnection<?, ?> conn = (StatefulRedisConnection<?, ?>)connection;
            return (C)conn.reactive();
        }else if (client instanceof RedisClusterClient){
            StatefulRedisClusterConnection<?, ?> conn = (StatefulRedisClusterConnection<?, ?>)connection;
            return (C)conn.reactive();
        }else{
            return null;
        }
    }

    void initCodec(CacheExchange<K, V, R> exchange){
        codec = new LayerRedisCodec<>(exchange.getRequest().getKeyClass(), exchange.getRequest().getSerializerClass());
    }

    @SuppressWarnings("unchecked")
    protected <T> Mono<T> expire(C commands, CacheExchange<K, V, R> exchange, T result){
        CachePolicy policy = exchange.getPolicy();
        if (policy.getDuration() == null){
            return Mono.just(result);
        }
        if (policy.getTimeUnit().equals(TimeUnit.MICROSECONDS) ||
                policy.getTimeUnit().equals(TimeUnit.MILLISECONDS) ||
                policy.getTimeUnit().equals(TimeUnit.NANOSECONDS)){
            if (commands instanceof RedisAdvancedClusterReactiveCommands){
                return ((RedisAdvancedClusterReactiveCommands<K, ?>) commands).pexpire(exchange.getRequest().getKey(), policy.getTimeUnit().toMillis(policy.getDuration())).thenReturn(result);
            }else{
                return ((RedisReactiveCommands<K, ?>) commands).pexpire(exchange.getRequest().getKey(), policy.getTimeUnit().toMillis(policy.getDuration())).thenReturn(result);
            }
        }else{
            if (commands instanceof RedisAdvancedClusterReactiveCommands){
                return ((RedisAdvancedClusterReactiveCommands<K, ?>) commands).expire(exchange.getRequest().getKey(), policy.getTimeUnit().toSeconds(policy.getDuration())).thenReturn(result);
            }else{
                return ((RedisReactiveCommands<K, ?>) commands).expire(exchange.getRequest().getKey(), policy.getTimeUnit().toSeconds(policy.getDuration())).thenReturn(result);
            }
        }
    }
}
