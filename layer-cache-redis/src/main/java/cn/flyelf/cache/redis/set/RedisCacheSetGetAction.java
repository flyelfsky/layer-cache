package cn.flyelf.cache.redis.set;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.core.util.TypeUtil;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisSetReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Set;
import java.util.stream.Collectors;

/**
 * redis的set的get动作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-20
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-20 1.0 新增]
 */
@Slf4j
public class RedisCacheSetGetAction<K, V> extends AbstractRedisSetCacheAction<K, V, Set<V>> {
    public RedisCacheSetGetAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.GETSET, processor, area);
    }

    @Override
    protected Mono<Boolean> doOnLoadAfter(RedisSetReactiveCommands<K, V> commands,
                                          CacheExchange<K, Set<V>, Set<V>> exchange, Set<V> value){
        if (null == value || value.isEmpty()){
            log.warn("redis的set数据类型不能存储null和空的数据");
            return Mono.just(false);
        }
        V[] array = TypeUtil.set2Array(value);
        return commands.sadd(exchange.getRequest().getKey(), array)
                .flatMap(r -> expire(commands, exchange, r))
                .map(r -> true);
    }

    @Override
    protected Mono<CacheResult<Set<V>>> doCommand(RedisSetReactiveCommands<K, V> commands, CacheExchange<K, Set<V>, Set<V>> exchange) {
        return commands.smembers(exchange.getRequest().getKey())
                .switchIfEmpty(checkKey(exchange.getRequest().getKey(), commands))
                .collect(Collectors.toSet())
                .map(l -> CacheResult.success(processor().name(), l));
    }

    private Publisher<V> checkKey(K key, RedisSetReactiveCommands<K, V> commands){
        return Flux.from(hasKey(commands, key).flatMap(b -> Mono.error(new CacheNotExistException(processor().name(), key.toString()))));
    }
}
