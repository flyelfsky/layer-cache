package cn.flyelf.cache.redis.set;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.context.RedisCacheContext;
import cn.flyelf.cache.redis.BaseRedisCacheAction;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisSetReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Set;

/**
 * redis的set缓存动作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * @param <R>: 缓存的动作返回类型
 *
 * @author wujr
 * 2019-12-20
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-20 1.0 新增]
 */
@Slf4j
public abstract class AbstractRedisSetCacheAction<K, V, R> extends BaseRedisCacheAction<K, Set<V>, R, RedisSetReactiveCommands<K, V>> {
    AbstractRedisSetCacheAction(ACTION action, RedisCacheLayerProcessor processor, String area){
        super(action, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<R>> doAction(CacheExchange<K, Set<V>, R> exchange) {
        RedisCacheContext<RedisSetReactiveCommands<K, V>> redisContext = (RedisCacheContext<RedisSetReactiveCommands<K, V>>)cacheContext;
        return doCommand(redisContext.getCommands(), exchange);
    }

    /**
     * 执行指令
     * @author wujr
    * 2019-12-17
     * 变更历史
     * [wujr 2019-12-17 1.0 新增]
     *
     * @param commands: 指令
     * @param exchange: 交换
     * @return 执行结果
     */
    protected abstract Mono<CacheResult<R>> doCommand(RedisSetReactiveCommands<K, V> commands, CacheExchange<K, Set<V>, R> exchange);
}
