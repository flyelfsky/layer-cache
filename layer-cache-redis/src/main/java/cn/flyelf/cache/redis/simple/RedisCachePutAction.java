package cn.flyelf.cache.redis.simple;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.redis.model.RedisConstant;
import cn.flyelf.cache.redis.util.RedisUtil;
import io.lettuce.core.SetArgs;
import io.lettuce.core.api.reactive.RedisStringReactiveCommands;
import reactor.core.publisher.Mono;


/**
 * redis的put指令
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-17
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-17 1.0 新增]
 */
public class RedisCachePutAction<K, V> extends AbstractRedisCacheAction<K, V, Boolean> {
    public RedisCachePutAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.PUT, processor, area);
    }

    @Override
    protected Mono<CacheResult<Boolean>> doCommand(RedisStringReactiveCommands<K, V> commands, CacheExchange<K, V, Boolean> exchange){
        SetArgs args = RedisUtil.build(exchange.getPolicy());
        if (args != null){
            return commands.set(exchange.getRequest().getKey(), exchange.getRequest().getValue(), args)
                    .map(r -> CacheResult.success(processor().name(), RedisConstant.SUCCESS.equals(r)));
        }
        return commands.set(exchange.getRequest().getKey(), exchange.getRequest().getValue())
                .map(r -> CacheResult.success(processor().name(), RedisConstant.SUCCESS.equals(r)));
    }
}
