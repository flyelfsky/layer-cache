package cn.flyelf.cache.redis.list;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisListReactiveCommands;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * redis的list类型的pop操作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/20
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/20 1.0 新增]
 */
public class RedisCacheListPopAction<K, V> extends AbstractRedisListCacheAction<K, V, V> {
    public RedisCacheListPopAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.POP, processor, area);
    }

    @Override
    protected Mono<CacheResult<V>> doCommand(RedisListReactiveCommands<K, V> commands, CacheExchange<K, List<V>, V> exchange) {
        String direction = exchange.getRequest().getAttachmentDefault(CacheConstant.ATTACH_LIST_DIRECTION, CacheConstant.LIST_RIGHT);
        Mono<V> mono;
        if (CacheConstant.LIST_RIGHT.equals(direction)){
            mono = commands.rpop(exchange.getRequest().getKey());
        }else{
            mono = commands.lpop(exchange.getRequest().getKey());
        }
        return mono
                .switchIfEmpty(Mono.error(new CacheNotExistException(processor().name(), exchange.getRequest().getKey().toString())))
                .map(r -> CacheResult.success(processor().name(), r));
    }
}
