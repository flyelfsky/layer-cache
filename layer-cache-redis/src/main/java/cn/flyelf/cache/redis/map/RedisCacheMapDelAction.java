package cn.flyelf.cache.redis.map;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisHashReactiveCommands;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * 删除redis的hash类型的部分键值
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的map的key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-21
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-21 1.0 新增]
 */
public class RedisCacheMapDelAction<K, H, V> extends AbstractRedisMapCacheAction<K, H, V, Long> {
    public RedisCacheMapDelAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.DELMAPKEY, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<Long>> doCommand(RedisHashReactiveCommands<K, V> commands, CacheExchange<K, Map<H, V>, Long> exchange) {
        H[] hks = getArrayFromAttachment(exchange, CacheConstant.ATTACH_HASH_HK);
        return commands.hdel(exchange.getRequest().getKey(), (K[])hks).map(l -> CacheResult.success(processor().name(), l));
    }
}
