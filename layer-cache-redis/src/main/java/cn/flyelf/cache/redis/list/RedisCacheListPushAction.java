package cn.flyelf.cache.redis.list;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisListReactiveCommands;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * lish的push操作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-20
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-20 1.0 新增]
 */
public class RedisCacheListPushAction<K, V> extends RedisCacheListPutAction<K, V>{
    public RedisCacheListPushAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.PUSH, processor, area);
    }

    @Override
    protected Mono<CacheResult<Boolean>> doPushCommand(RedisListReactiveCommands<K, V> commands,
                                                       CacheExchange<K, List<V>, Boolean> exchange,
                                                       V[] array){
        String direction = checkDirection(exchange);
        Mono<Long> mono;
        if (CacheConstant.LIST_LEFT.equals(direction)){
            mono = commands.lpushx(exchange.getRequest().getKey(), array);
        }else{
            mono = commands.rpushx(exchange.getRequest().getKey(), array);
        }
        return mono.flatMap(r -> expire(commands, exchange, r))
                .map(r -> CacheResult.success(processor().name(), true));
    }
}
