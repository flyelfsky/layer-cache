package cn.flyelf.cache.redis.list;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.core.util.TypeUtil;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisListReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.Publisher;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;


/**
 * redis的list类型获取动作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-17
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-17 1.0 新增]
 */
@Slf4j
public class RedisCacheListGetAction<K, V> extends AbstractRedisListCacheAction<K, V, List<V>> {
    public RedisCacheListGetAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.GETLIST, processor, area);
    }

    @Override
    protected Mono<Boolean> doOnLoadAfter(RedisListReactiveCommands<K, V> commands,
                                          CacheExchange<K, List<V>, List<V>> exchange, List<V> value){
        if (null == value || value.isEmpty()){
            log.warn("redis的list类型数据不能存储null和空的数据");
            return super.doOnLoadAfter(commands, exchange, value);
        }
        V[] array = TypeUtil.list2Array(value);
        return commands.lpush(exchange.getRequest().getKey(), array)
                .flatMap(r -> expire(commands, exchange, r))
                .map(r -> true);
    }

    @Override
    protected Mono<CacheResult<List<V>>> doCommand(RedisListReactiveCommands<K, V> commands,
                                                   CacheExchange<K, List<V>, List<V>> exchange){
        K key = exchange.getRequest().getKey();
        long start = exchange.getRequest().getAttachmentDefault(CacheConstant.ATTACH_LIST_START, CacheConstant.POS_START);
        long stop = exchange.getRequest().getAttachmentDefault(CacheConstant.ATTACH_LIST_STOP, CacheConstant.POS_STOP);

        return commands.lrange(key, start, stop)
                .switchIfEmpty(checkKey(key, commands))
                .collectList()
                .map(l -> CacheResult.success(processor().name(), l));
    }

    /**
     * 对于list的数据类型，如果没有数据的情况下，key会被删除，所以不存在有key但是没有数据的场景
     * @author wujr
    * 2020-01-04
     * 变更历史
     * [wujr 2020-01-04 1.0 新增]
     *
     * @param key:
     * @param commands:
     * @return 检查结果
     */
    private Publisher<V> checkKey(K key, RedisListReactiveCommands<K, V> commands){
        return Flux.from(hasKey(commands, key).flatMap(b -> Mono.error(new CacheNotExistException(processor().name(), key.toString()))));
    }
}
