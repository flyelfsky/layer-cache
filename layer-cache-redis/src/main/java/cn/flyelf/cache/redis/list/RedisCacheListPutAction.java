package cn.flyelf.cache.redis.list;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.core.util.TypeUtil;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisListReactiveCommands;
import reactor.core.publisher.Mono;

import java.util.List;


/**
 * redis的list类型设置
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-17
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-17 1.0 新增]
 */
public class RedisCacheListPutAction<K, V> extends AbstractRedisListCacheAction<K, V, Boolean> {
    public RedisCacheListPutAction(RedisCacheLayerProcessor processor, String area){
        this(ACTION.PUTLIST, processor, area);
    }
    RedisCacheListPutAction(ACTION action, RedisCacheLayerProcessor processor, String area){
        super(action, processor, area);
    }
    @Override
    protected Mono<CacheResult<Boolean>> doCommand(RedisListReactiveCommands<K, V> commands, CacheExchange<K, List<V>, Boolean> exchange) {
        if (exchange.getRequest().getValue() == null || exchange.getRequest().getValue().isEmpty()){
            return Mono.error(new InvalidCacheValueException(processor().name(), exchange.getRequest().getKey().toString()));
        }
        V[] array = TypeUtil.list2Array(exchange.getRequest().getValue());
        return doPushCommand(commands, exchange, array);
    }

    protected Mono<CacheResult<Boolean>> doPushCommand(RedisListReactiveCommands<K, V> commands,
                                                       CacheExchange<K, List<V>, Boolean> exchange,
                                                       V[] array){
        Mono<Long> mono = commands.lpush(exchange.getRequest().getKey(), array);
        return mono.flatMap(r -> expire(commands, exchange, r))
                .map(r -> CacheResult.success(processor().name(), true));
    }
}
