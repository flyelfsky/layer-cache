package cn.flyelf.cache.redis.model;

/**
 * redis的常量定义
 *
 * @author wujr
 * 2019-12-17
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-17 1.0 新增]
 */
public class RedisConstant {
    /**
     * redis指令执行成功
     */
    public static final String SUCCESS = "OK";
}
