package cn.flyelf.cache.redis.map;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.redis.model.RedisConstant;
import io.lettuce.core.api.reactive.RedisHashReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * redis的hash类型的put操作
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的map的key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-21
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-21 1.0 新增]
 */
@Slf4j
public class RedisCacheMapPutAction<K, H, V> extends AbstractRedisMapCacheAction<K, H, V, Boolean> {
    public RedisCacheMapPutAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.PUTMAP, processor, area);
    }

    @Override
    protected Mono<CacheResult<Boolean>> doCommand(RedisHashReactiveCommands<K, V> commands, CacheExchange<K, Map<H, V>, Boolean> exchange) {
        if (exchange.getRequest().getValue() == null || exchange.getRequest().getValue().isEmpty()){
            return Mono.error(new InvalidCacheValueException(processor().name(), exchange.getRequest().getKey().toString()));
        }
        return doAddCommand(commands, exchange, exchange.getRequest().getValue());
    }

    @SuppressWarnings("unchecked")
    private Mono<CacheResult<Boolean>> doAddCommand(RedisHashReactiveCommands<K, V> commands,
                                                      CacheExchange<K, Map<H, V>, Boolean> exchange,
                                                      Map<H, V> map){
        return commands.hmset(exchange.getRequest().getKey(), (Map<K, V>)map)
                .flatMap(r -> expire(commands, exchange, r))
                .map(r -> CacheResult.success(processor().name(), RedisConstant.SUCCESS.equals(r)));
    }
}
