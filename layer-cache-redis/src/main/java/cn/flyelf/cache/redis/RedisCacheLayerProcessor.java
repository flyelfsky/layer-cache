package cn.flyelf.cache.redis;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.redis.codec.LayerRedisCodec;
import cn.flyelf.cache.redis.list.RedisCacheListPopAction;
import cn.flyelf.cache.redis.list.RedisCacheListPushAction;
import cn.flyelf.cache.redis.map.*;
import cn.flyelf.cache.redis.set.RedisCacheSetGetAction;
import cn.flyelf.cache.redis.set.RedisCacheSetPutAction;
import cn.flyelf.cache.redis.simple.RedisCacheGetAction;
import cn.flyelf.cache.core.action.AbstractCacheLayerProcessor;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheConfigException;
import cn.flyelf.cache.redis.list.RedisCacheListGetAction;
import cn.flyelf.cache.redis.list.RedisCacheListPutAction;
import cn.flyelf.cache.redis.simple.RedisCachePutAction;
import cn.flyelf.cache.redis.util.RedisUtil;
import io.lettuce.core.AbstractRedisClient;
import io.lettuce.core.ClientOptions;
import io.lettuce.core.RedisClient;
import io.lettuce.core.RedisURI;
import io.lettuce.core.api.StatefulConnection;
import io.lettuce.core.api.StatefulRedisConnection;
import io.lettuce.core.cluster.ClusterClientOptions;
import io.lettuce.core.cluster.RedisClusterClient;
import io.lettuce.core.cluster.api.StatefulRedisClusterConnection;
import io.lettuce.core.resource.ClientResources;
import io.lettuce.core.resource.DefaultClientResources;
import io.lettuce.core.support.ConnectionPoolSupport;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPool;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.lang.reflect.Constructor;
import java.net.URI;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * redis的缓存层处理器
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
@Slf4j
public class RedisCacheLayerProcessor extends AbstractCacheLayerProcessor {
    private AbstractRedisClient defaultClient;
    /**
     * redis的客户端链接，支持连接多个redis
     * key : 缓存的区域，等同于jetcache的area，默认值为：default
     */
    private ConcurrentHashMap<String, AbstractRedisClient> clients = new ConcurrentHashMap<>(1);
    /**
     * redis缓存区域的缓存层参数
     * key：缓存区域area
     */
    private final ConcurrentHashMap<String, CacheLayerConfig> layerConfig = new ConcurrentHashMap<>(1);
    private static final ConcurrentHashMap<ACTION, Constructor<?>> CONSTRUCTORS = new ConcurrentHashMap<>(9);
    /**
     * 连接池
     * key：area-RedisCodec.hashCode
     */
    private final ConcurrentHashMap<String, GenericObjectPool<? extends StatefulConnection<?, ?>>> connectionPool = new ConcurrentHashMap<>(10);

    static {
        CONSTRUCTORS.put(ACTION.GET, RedisUtil.actionConstructor(RedisCacheGetAction.class));
        CONSTRUCTORS.put(ACTION.PUT, RedisUtil.actionConstructor(RedisCachePutAction.class));
        CONSTRUCTORS.put(ACTION.DELETE, RedisUtil.actionConstructor(RedisCacheDeleteAction.class));
        CONSTRUCTORS.put(ACTION.PUTLIST, RedisUtil.actionConstructor(RedisCacheListPutAction.class));
        CONSTRUCTORS.put(ACTION.GETLIST, RedisUtil.actionConstructor(RedisCacheListGetAction.class));
        CONSTRUCTORS.put(ACTION.POP, RedisUtil.actionConstructor(RedisCacheListPopAction.class));
        CONSTRUCTORS.put(ACTION.PUSH, RedisUtil.actionConstructor(RedisCacheListPushAction.class));
        CONSTRUCTORS.put(ACTION.PUTSET, RedisUtil.actionConstructor(RedisCacheSetPutAction.class));
        CONSTRUCTORS.put(ACTION.GETSET, RedisUtil.actionConstructor(RedisCacheSetGetAction.class));
        CONSTRUCTORS.put(ACTION.PUTMAP, RedisUtil.actionConstructor(RedisCacheMapPutAction.class));
        CONSTRUCTORS.put(ACTION.GETMAP, RedisUtil.actionConstructor(RedisCacheMapGetAction.class));
        CONSTRUCTORS.put(ACTION.GETMAPKEY, RedisUtil.actionConstructor(RedisCacheMapGetKeyAction.class));
        CONSTRUCTORS.put(ACTION.HASMAPKEY, RedisUtil.actionConstructor(RedisCacheMapExistAction.class));
        CONSTRUCTORS.put(ACTION.DELMAPKEY, RedisUtil.actionConstructor(RedisCacheMapDelAction.class));
        CONSTRUCTORS.put(ACTION.ADDMAPKEY, RedisUtil.actionConstructor(RedisCacheMapAddAction.class));
    }
    public RedisCacheLayerProcessor(CacheEventPublisher eventPublisher){
        super(eventPublisher);
    }

    @SuppressWarnings("unchecked")
    <C extends StatefulConnection<?, ?>> C getConnection(LayerRedisCodec<?, ?> codec, String area){
        GenericObjectPool<? extends StatefulConnection<?, ?>> pool = connectionPool.computeIfAbsent(area + "-" + codec.hashCode(), k -> {
            AbstractRedisClient client = getClient(area);
            CacheLayerConfig config = layerConfig.get(area);
            if (client instanceof RedisClient) {
                GenericObjectPoolConfig<StatefulRedisConnection<?, ?>> poolConfig = RedisUtil.buildPoolConfig(config);
                return ConnectionPoolSupport.createGenericObjectPool(() -> ((RedisClient)client).connect(codec), poolConfig);
            }else if (client instanceof RedisClusterClient){
                GenericObjectPoolConfig<StatefulRedisClusterConnection<?, ?>> poolConfig = RedisUtil.buildPoolConfig(config);
                return ConnectionPoolSupport.createGenericObjectPool(() -> ((RedisClusterClient)client).connect(codec), poolConfig);
            }else{
                throw new CacheConfigException("redis client unknown");
            }
        });
        try {
            return (C)pool.borrowObject();
        }catch (Exception e){
            log.error("redis连接池获取连接对象失败：", e);
            return null;
        }
    }

    @SuppressWarnings("unchecked")
    boolean releaseConnection(LayerRedisCodec<?, ?> codec, String area, StatefulConnection<?, ?> connection){
        if (connection instanceof StatefulRedisConnection) {
            GenericObjectPool<StatefulRedisConnection<?, ?>> pool = (GenericObjectPool<StatefulRedisConnection<?, ?>>)connectionPool.get(area + "-" + codec.hashCode());
            if (null == pool){
                log.warn("归还redis单机连接：没有找到：{}-{}的连接池，无法归还", area, codec.hashCode());
                return false;
            }
            pool.returnObject((StatefulRedisConnection<?, ?>)connection);
        }else{
            GenericObjectPool<StatefulRedisClusterConnection<?, ?>> pool = (GenericObjectPool<StatefulRedisClusterConnection<?, ?>>)connectionPool.get(area + "-" + codec.hashCode());
            if (null == pool){
                log.warn("归还redis集群连接：没有找到：{}-{}的连接池，无法归还", area, codec.hashCode());
                return false;
            }
            pool.returnObject((StatefulRedisClusterConnection<?, ?>)connection);
        }
        return true;
    }
    void addClient(String area, AbstractRedisClient client){
        clients.put(area, client);
        if (CacheConstant.AREA_DEFAULT.equalsIgnoreCase(area)){
            defaultClient = client;
        }
    }
    public void shutdown(){
        clients.forEach((k, c) -> c.shutdown());
    }
    public AbstractRedisClient getClient(String area){
        return clients.getOrDefault(area, defaultClient);
    }
    @Override
    public CacheAction<?, ?, ?> action(ACTION action, String area) {
        Constructor<?> constructor = CONSTRUCTORS.get(action);
        if (null != constructor){
            return RedisUtil.action(this, constructor, area);
        }
        return null;
    }
    public void buildAreaClient(String area, CacheLayerConfig config){
        if (config.getUri() == null || config.getUri().length == 0){
            throw new CacheConfigException("redis uri is required");
        }

        DefaultClientResources.Builder builder = DefaultClientResources.builder();
        if (config.getResources() != null){
            if (null != config.getResources().getIoThread()){
                builder.ioThreadPoolSize(config.getResources().getIoThread());
            }
            if (null != config.getResources().getComputeThread()){
                builder.computationThreadPoolSize(config.getResources().getComputeThread());
            }
        }
        ClientResources resources = builder.build();

        List<RedisURI> uris = Stream.of(config.getUri()).map(k -> RedisURI.create(URI.create(k))).collect(Collectors.toList());
        AbstractRedisClient client;
        if (uris.size() == 1){
            client = RedisClient.create(resources, uris.get(0));
            ((RedisClient) client).setOptions(ClientOptions.builder().
                    disconnectedBehavior(ClientOptions.DisconnectedBehavior.REJECT_COMMANDS).build());
        }else{
            client = RedisClusterClient.create(resources, uris);
            ((RedisClusterClient) client).setOptions(ClusterClientOptions.builder().
                    disconnectedBehavior(ClientOptions.DisconnectedBehavior.REJECT_COMMANDS).build());
        }
        this.layerConfig.put(area, config);

        addClient(area, client);
    }
}
