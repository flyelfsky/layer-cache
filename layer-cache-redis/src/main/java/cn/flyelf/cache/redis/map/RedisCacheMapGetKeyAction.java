package cn.flyelf.cache.redis.map;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.Value;
import io.lettuce.core.api.reactive.RedisHashReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Map;

/**
 * 根据redis的hash类型的内容key进行获取内容并且转换为list
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的map的key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-21
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-21 1.0 新增]
 */
@Slf4j
public class RedisCacheMapGetKeyAction<K, H, V> extends AbstractRedisMapCacheAction<K, H, V, List<V>> {
    public RedisCacheMapGetKeyAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.GETMAPKEY, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<List<V>>> doCommand(RedisHashReactiveCommands<K, V> commands,
                                                    CacheExchange<K, Map<H, V>, List<V>> exchange) {
        H[] hks = getArrayFromAttachment(exchange, CacheConstant.ATTACH_HASH_HK);

        return commands.hmget(exchange.getRequest().getKey(), (K[])hks).filter(Value::hasValue)
                .flatMap(kv -> Mono.just(kv.getValue())).collectList()
                .map(l -> CacheResult.success(processor().name(), l));
    }
}
