package cn.flyelf.cache.redis.context;

import cn.flyelf.cache.core.context.CacheContext;
import io.lettuce.core.api.StatefulConnection;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * redis的缓存上下文
 * @param <C>: redis的指令类
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
@Data
@EqualsAndHashCode(callSuper = false)
@AllArgsConstructor
public class RedisCacheContext<C> extends CacheContext {
    /**
     * 指令
     */
    private C commands;
    /**
     * 链接
     */
    private StatefulConnection<?, ?> connection;
}
