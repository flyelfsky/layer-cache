package cn.flyelf.cache.redis.set;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.InvalidCacheValueException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.core.util.TypeUtil;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisSetReactiveCommands;
import reactor.core.publisher.Mono;

import java.util.Set;

/**
 * redis的set数据类型的put操作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-20
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-20 1.0 新增]
 */
public class RedisCacheSetPutAction<K, V> extends AbstractRedisSetCacheAction<K, V, Boolean> {
    public RedisCacheSetPutAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.PUTSET, processor, area);
    }

    @Override
    protected Mono<CacheResult<Boolean>> doCommand(RedisSetReactiveCommands<K, V> commands, CacheExchange<K, Set<V>, Boolean> exchange) {
        if (exchange.getRequest().getValue() == null || exchange.getRequest().getValue().isEmpty()){
            return Mono.error(new InvalidCacheValueException(processor().name(), exchange.getRequest().getKey().toString()));
        }
        V[] array = TypeUtil.set2Array(exchange.getRequest().getValue());
        return doAddCommand(commands, exchange, array);
    }

    private Mono<CacheResult<Boolean>> doAddCommand(RedisSetReactiveCommands<K, V> commands,
                                                      CacheExchange<K, Set<V>, Boolean> exchange,
                                                      V[] array){
        return commands.sadd(exchange.getRequest().getKey(), array)
                .flatMap(r -> expire(commands, exchange, r))
                .map(r -> CacheResult.success(processor().name(), true));
    }
}
