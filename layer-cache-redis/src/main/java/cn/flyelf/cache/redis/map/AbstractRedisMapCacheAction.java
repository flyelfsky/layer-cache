package cn.flyelf.cache.redis.map;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.BaseRedisCacheAction;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.redis.context.RedisCacheContext;
import io.lettuce.core.api.reactive.RedisHashReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * redis的map类型操作
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的map的key类型
 * @param <V>: 缓存内容类型
 * @param <R>: 缓存的处理结果类型
 *
 * @author wujr
 * 2019-12-20
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-20 1.0 新增]
 */
@Slf4j
public abstract class AbstractRedisMapCacheAction<K, H, V, R> extends BaseRedisCacheAction<K, Map<H, V>, R, RedisHashReactiveCommands<K, V>> {
    AbstractRedisMapCacheAction(ACTION action, RedisCacheLayerProcessor processor, String area){
        super(action, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<R>> doAction(CacheExchange<K, Map<H, V>, R> exchange) {
        RedisCacheContext<RedisHashReactiveCommands<K, V>> redisContext = (RedisCacheContext<RedisHashReactiveCommands<K, V>>)cacheContext;
        return doCommand(redisContext.getCommands(), exchange);
    }

    /**
     * 执行指令
     * @author wujr
     * 2019-12-17
     * 变更历史
     * [wujr 2019-12-17 1.0 新增]
     *
     * @param commands: 指令
     * @param exchange: 交换
     * @return 执行结果
     */
    protected abstract Mono<CacheResult<R>> doCommand(RedisHashReactiveCommands<K, V> commands, CacheExchange<K, Map<H, V>, R> exchange);
}
