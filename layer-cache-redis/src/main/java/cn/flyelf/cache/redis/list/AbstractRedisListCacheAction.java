package cn.flyelf.cache.redis.list;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.redis.context.RedisCacheContext;
import cn.flyelf.cache.redis.BaseRedisCacheAction;
import io.lettuce.core.api.reactive.RedisListReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.List;

/**
 * redis的list类型的动作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * @param <R>: 缓存动作的返回结果类型
 *
 * @author wujr
 * 2019-12-17
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-17 1.0 新增]
 */
@Slf4j
public abstract class AbstractRedisListCacheAction<K, V, R> extends BaseRedisCacheAction<K, List<V>, R, RedisListReactiveCommands<K, V>> {
    AbstractRedisListCacheAction(ACTION action, RedisCacheLayerProcessor processor, String area){
        super(action, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<R>> doAction(CacheExchange<K, List<V>, R> exchange) {
        RedisCacheContext<RedisListReactiveCommands<K, V>> redisContext = (RedisCacheContext<RedisListReactiveCommands<K, V>>)cacheContext;
        return doCommand(redisContext.getCommands(), exchange);
    }

    String checkDirection(CacheExchange<K, List<V>, Boolean> exchange){
        return exchange.getRequest().getAttachmentDefault(CacheConstant.ATTACH_LIST_DIRECTION, CacheConstant.LIST_LEFT);
    }

    /**
     * 执行指令
     * @author wujr
    * 2019-12-17
     * 变更历史
     * [wujr 2019-12-17 1.0 新增]
     *
     * @param commands: 指令
     * @param exchange: 交换
     * @return 执行结果
     */
    protected abstract Mono<CacheResult<R>> doCommand(RedisListReactiveCommands<K, V> commands, CacheExchange<K, List<V>, R> exchange);
}
