package cn.flyelf.cache.redis.map;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.redis.model.RedisConstant;
import io.lettuce.core.api.reactive.RedisHashReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * redis的hash类型的get操作
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的map的key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-21
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-21 1.0 新增]
 */
@Slf4j
public class RedisCacheMapGetAction<K, H, V> extends AbstractRedisMapCacheAction<K, H, V, Map<H, V>> {
    public RedisCacheMapGetAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.GETMAP, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<Boolean> doOnLoadAfter(RedisHashReactiveCommands<K, V> commands,
                                          CacheExchange<K, Map<H, V>, Map<H, V>> exchange,
                                          Map<H, V> value){
        if (null == value || value.size() == 0){
            log.warn("redis的hash类型不能存储null和空数据");
            return super.doOnLoadAfter(commands, exchange, value);
        }

        return commands.hmset(exchange.getRequest().getKey(), (Map)value)
                .flatMap(r -> expire(commands, exchange, r))
                .map(RedisConstant.SUCCESS::equals);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<Map<H, V>>> doCommand(RedisHashReactiveCommands<K, V> commands,
                                                       CacheExchange<K, Map<H, V>, Map<H, V>> exchange) {
        return commands.hgetall(exchange.getRequest().getKey())
                .flatMap(r -> {
                    if (r.isEmpty()){
                        return checkKey(exchange.getRequest().getKey(), commands);
                    }else{
                        return Mono.just(CacheResult.success(processor().name(), (Map) r));
                    }
                });
    }

    private Mono<CacheResult<Map<H, V>>> checkKey(K key, RedisHashReactiveCommands<K, V> commands){
        return hasKey(commands, key).flatMap(b -> Mono.error(new CacheNotExistException(processor().name(), key.toString())));
    }
}
