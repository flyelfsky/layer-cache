package cn.flyelf.cache.redis.simple;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.redis.model.RedisConstant;
import cn.flyelf.cache.redis.util.RedisUtil;
import io.lettuce.core.SetArgs;
import io.lettuce.core.api.reactive.RedisStringReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Optional;

/**
 * redis的get操作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-14
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-14 1.0 新增]
 */
@Slf4j
public class RedisCacheGetAction<K, V> extends AbstractRedisCacheAction<K, V, V> {
    public RedisCacheGetAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.GET, processor, area);
    }

    @Override
    protected Mono<Boolean> doOnLoadAfter(RedisStringReactiveCommands<K, V> commands, CacheExchange<K, V, V> exchange, V value){
        SetArgs args;
        if (null == value){
            if (!exchange.getPolicy().isCacheNull()){
                log.info("redis缓存不对null对象进行缓存");
                return super.doOnLoadAfter(commands, exchange, null);
            }
            args = RedisUtil.buildSecond(exchange.getPolicy().getNullExpire());
        }else {
            args = RedisUtil.build(exchange.getPolicy());
        }
        if (args != null){
            return commands.set(exchange.getRequest().getKey(), value, args).map(RedisConstant.SUCCESS::equals);
        }
        return commands.set(exchange.getRequest().getKey(), value).map(RedisConstant.SUCCESS::equals);
    }
    @Override
    protected Mono<CacheResult<V>> doCommand(RedisStringReactiveCommands<K, V> commands, CacheExchange<K, V, V> exchange){
        return commands.get(exchange.getRequest().getKey())
                .switchIfEmpty(checkKey(exchange.getRequest().getKey(), commands))
                .flatMap(v -> Mono.just(Optional.of(v)))
                .defaultIfEmpty(Optional.empty())
                .map(r -> r.map(v -> CacheResult.success(processor().name(), v)).orElse(CacheResult.success(processor().name(), null)));
    }
    private Mono<V> checkKey(K key, RedisStringReactiveCommands<K, V> commands){
        return Mono.from(hasKey(commands, key).flatMap(b -> Boolean.TRUE.equals(b) ? Mono.empty() : Mono.error(new CacheNotExistException(processor().name(), key.toString()))));
    }
}
