package cn.flyelf.cache.redis.codec;

import cn.flyelf.cache.core.model.CacheSerializer;
import cn.flyelf.cache.core.support.ObjectSerializer;
import io.lettuce.core.codec.RedisCodec;
import java.nio.ByteBuffer;

/**
 * redis的编解码处理器
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-15
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-15 1.0 新增]
 */
public class LayerRedisCodec<K, V> implements RedisCodec<K, V> {
    private final ObjectSerializer keySerializer;
    private final ObjectSerializer valueSerializer;
    private Class keyClass;
    private Class valueClass;

    LayerRedisCodec(Class keyClass, Class valueClass, ObjectSerializer keySerializer, ObjectSerializer valueSerializer){
        this.keySerializer = keySerializer;
        this.valueSerializer = valueSerializer;
        this.keyClass = keyClass;
        this.valueClass = valueClass;
    }
    public LayerRedisCodec(Class keyClass, Class valueClass){
        this(keyClass, valueClass, CacheSerializer.UTF8, CacheSerializer.DEFAULT);
    }
    @Override
    public int hashCode(){
        return (keySerializer.getClass().getName() + valueSerializer.getClass().getName()).hashCode();
    }
    @Override
    public boolean equals(Object object){
        if (this == object){
            return true;
        }
        if (!(object instanceof LayerRedisCodec)){
            return false;
        }
        return object.hashCode() == hashCode();
    }

    @Override
    public K decodeKey(ByteBuffer bytes) {
        return keySerializer.decode(bytes, keyClass);
    }

    @Override
    public V decodeValue(ByteBuffer bytes) {
        return valueSerializer.decode(bytes, valueClass);
    }

    @Override
    public ByteBuffer encodeKey(K key) {
        return keySerializer.encode(key);
    }

    @Override
    public ByteBuffer encodeValue(V value) {
        return valueSerializer.encode(value);
    }
}
