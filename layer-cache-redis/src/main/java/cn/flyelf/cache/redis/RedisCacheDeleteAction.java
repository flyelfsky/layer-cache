package cn.flyelf.cache.redis;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.core.util.TypeUtil;
import cn.flyelf.cache.redis.context.RedisCacheContext;
import io.lettuce.core.cluster.api.reactive.RedisClusterReactiveCommands;
import reactor.core.publisher.Mono;

import java.util.Collections;


/**
 * 删除redis的缓存
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-21
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-21 1.0 新增]
 */
public class RedisCacheDeleteAction<K, V> extends BaseRedisCacheAction<K, V, Long, RedisClusterReactiveCommands<K, V>> {
    public RedisCacheDeleteAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.DELETE, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<Long>> doAction(CacheExchange<K, V, Long> exchange) {
        RedisCacheContext<RedisClusterReactiveCommands<K, V>> redisContext = (RedisCacheContext<RedisClusterReactiveCommands<K,V>>)cacheContext;

        RedisClusterReactiveCommands<K, V> commands = redisContext.getCommands();
        K[] keys = getArrayFromAttachment(exchange, CacheConstant.ATTACH_KEY);
        if (keys.length == 0){
            keys = TypeUtil.list2Array(Collections.singletonList(exchange.getRequest().getKey()));
        }

        return commands.del(keys).map(l -> CacheResult.success(processor().name(), l));
    }
}
