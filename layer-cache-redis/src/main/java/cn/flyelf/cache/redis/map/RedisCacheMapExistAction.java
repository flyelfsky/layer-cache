package cn.flyelf.cache.redis.map;


import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.api.reactive.RedisHashReactiveCommands;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Mono;

import java.util.Map;

/**
 * 判定redis的hash类型的某个key是否存在
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的map的key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019-12-21
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-21 1.0 新增]
 */
@Slf4j
public class RedisCacheMapExistAction<K, H, V> extends AbstractRedisMapCacheAction<K, H, V, Boolean> {
    public RedisCacheMapExistAction(RedisCacheLayerProcessor processor, String area){
        super(ACTION.HASMAPKEY, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<Boolean>> doCommand(RedisHashReactiveCommands<K, V> commands, CacheExchange<K, Map<H, V>, Boolean> exchange) {
        H key = exchange.getRequest().getAttachment(CacheConstant.ATTACH_HASH_HK);
        return commands.hexists(exchange.getRequest().getKey(), (K)key).map(b -> CacheResult.success(processor().name(), b));
    }
}
