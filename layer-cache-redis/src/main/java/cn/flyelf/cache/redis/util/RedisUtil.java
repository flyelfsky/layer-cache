package cn.flyelf.cache.redis.util;

import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import io.lettuce.core.SetArgs;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;

import java.lang.reflect.Constructor;
import java.util.concurrent.TimeUnit;

/**
 * 辅助工具
 *
 * @author wujr
 * 2019/12/19
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/19 1.0 新增]
 */
@Slf4j
public class RedisUtil {
    private RedisUtil(){}
    public static SetArgs buildSecond(Long expire){
        if (null == expire){
            return null;
        }
        return SetArgs.Builder.ex(expire);
    }
    public static SetArgs build(CachePolicy policy){
        if (policy.getDuration() == null || policy.getTimeUnit() == null){
            return null;
        }
        if (policy.getTimeUnit().equals(TimeUnit.MICROSECONDS) ||
                policy.getTimeUnit().equals(TimeUnit.MILLISECONDS) ||
                policy.getTimeUnit().equals(TimeUnit.NANOSECONDS)){
            // 毫秒
            return SetArgs.Builder.px(policy.getTimeUnit().toMillis(policy.getDuration()));
        }else{
            return SetArgs.Builder.ex(policy.getTimeUnit().toSeconds(policy.getDuration()));
        }
    }
    public static <T> GenericObjectPoolConfig<T> buildPoolConfig(CacheLayerConfig config){
        GenericObjectPoolConfig<T> poolConfig = new GenericObjectPoolConfig<>();
        if (null != config && null != config.getPool()){
            if (config.getPool().getMinIdle() != null){
                poolConfig.setMinIdle(config.getPool().getMinIdle());
            }
            if (config.getPool().getMaxIdle() != null){
                poolConfig.setMaxIdle(config.getPool().getMaxIdle());
            }
            if (config.getPool().getMaxActive() != null){
                poolConfig.setMaxTotal(config.getPool().getMaxActive());
            }
            if (config.getPool().getMaxWait() != null){
                poolConfig.setMaxWaitMillis(config.getPool().getMaxWait());
            }
        }
        return poolConfig;
    }

    public static <T> Constructor<?> actionConstructor(Class<T> clz){
        try {
            return clz.getDeclaredConstructor(RedisCacheLayerProcessor.class, String.class);
        } catch (NoSuchMethodException e) {
            throw new UnsupportedClassVersionError("CacheAction's constructor not valid");
        }
    }
    public static CacheAction<?, ?, ?> action(RedisCacheLayerProcessor processor, Constructor<?> constructor, String area){
        try {
            return (CacheAction<?, ?, ?>)constructor.newInstance(processor, area);
        } catch (Exception e) {
            log.warn("redis CacheAction construct error: ", e);
        }
        return null;
    }
}
