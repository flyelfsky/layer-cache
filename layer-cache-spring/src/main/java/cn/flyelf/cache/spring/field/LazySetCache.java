package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.SetCache;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.lang.reflect.Field;
import java.util.Set;

/**
 * set数据类型的懒加载缓存
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2020/1/10
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/10 1.0 新增]
 */
public class LazySetCache<K, V> extends LazyCache<K, Set<V>, SetCache<K, V>> implements SetCache<K, V> {
    LazySetCache(ConfigurableListableBeanFactory beanFactory, CacheBean cacheBean, Field field){
        super(beanFactory, cacheBean, field);
    }

    @Override
    String cacheType(){
        return CacheConstant.CACHE_SET;
    }
}
