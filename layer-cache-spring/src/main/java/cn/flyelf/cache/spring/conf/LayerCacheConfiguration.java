package cn.flyelf.cache.spring.conf;

import cn.flyelf.cache.caffeine.CaffeineCacheLayerProcessor;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.conf.CacheGlobalConfig;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.core.monitor.AbstractCacheMonitor;
import cn.flyelf.cache.core.monitor.CacheMonitorService;
import cn.flyelf.cache.penetration.PenetrateCache;
import cn.flyelf.cache.penetration.caffeine.PenetrateCaffeineCache;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.spring.CacheManager;
import cn.flyelf.cache.spring.manager.SpringCacheManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;

/**
 * 多层缓存的配置
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
@Configuration
@ConditionalOnClass(CacheManager.class)
@ConditionalOnMissingBean(CacheManager.class)
public class LayerCacheConfiguration {
    @Autowired
    private CacheGlobalConfig globalConfig;

    @Bean("redisLayerProcessor")
    @ConditionalOnMissingBean(RedisCacheLayerProcessor.class)
    public RedisCacheLayerProcessor redisLayerProcessor(CacheEventPublisher eventPublisher){
        return new RedisCacheLayerProcessor(eventPublisher);
    }
    @Bean("caffeineLayerProcessor")
    @ConditionalOnMissingBean(CaffeineCacheLayerProcessor.class)
    public CaffeineCacheLayerProcessor caffeineLayerProcessor(CacheEventPublisher eventPublisher){
        return new CaffeineCacheLayerProcessor(eventPublisher);
    }

    @Bean
    @ConditionalOnMissingBean(CacheLayerFactory.class)
    public CacheLayerFactory cacheLayerFactory(CacheEventPublisher eventPublisher, PenetrateCache penetration){
        List<CacheLayerProcessor> processors = new ArrayList<>(2);
        processors.add(redisLayerProcessor(eventPublisher));
        processors.add(caffeineLayerProcessor(eventPublisher));
        CacheLayerFactory factory = new CacheLayerFactory();
        factory.setLayers(processors);
        factory.setPenetration(penetration);
        ///factory.setPolicy(politics);
        return factory;
    }

    @Bean
    public CacheManager cacheManager(){
        return new SpringCacheManager();
    }

    @Bean
    public CacheEventPublisher eventPublisher(){
        return new CacheEventPublisherImp();
    }

    @Bean
    public AbstractCacheMonitor cacheMonitor(CacheEventPublisher eventPublisher){
        AbstractCacheMonitor monitor = new CacheMonitorService(globalConfig);
        eventPublisher.subscribe(monitor);
        return monitor;
    }

    @Bean
    @ConditionalOnMissingBean
    public PenetrateCache penetrateCache(){
        return new PenetrateCaffeineCache(globalConfig.getPenetrationConfig());
    }
}
