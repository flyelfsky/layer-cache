package cn.flyelf.cache.spring.conf;

import cn.flyelf.cache.spring.field.CacheBeanAnnotationBeanPostProcessor;
import org.springframework.context.annotation.Import;

import java.lang.annotation.*;

/**
 * 使得@CacheBean的注解生效
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Import({CacheBeanAnnotationBeanPostProcessor.class})
public @interface EnableCacheBeanAnnotation {
}
