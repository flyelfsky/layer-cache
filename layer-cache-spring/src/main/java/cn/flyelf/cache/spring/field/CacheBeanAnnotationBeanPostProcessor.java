package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.core.HashCache;
import cn.flyelf.cache.core.ListCache;
import cn.flyelf.cache.core.SetCache;
import lombok.extern.slf4j.Slf4j;
import org.joor.Reflect;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.beans.factory.annotation.InjectionMetadata;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Component;
import org.springframework.util.ReflectionUtils;
import org.springframework.util.StringUtils;

import javax.annotation.Nonnull;
import java.lang.reflect.*;
import java.util.LinkedList;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * CacheBean注解的处理器
 *
 * @author wujr
 * 2019-12-23
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-23 1.0 新增]
 */
@Component
@Slf4j
public class CacheBeanAnnotationBeanPostProcessor extends AutowiredAnnotationBeanPostProcessor {
    private volatile boolean clearInit = false;
    private Method cleanMethod;
    private final Map<String, InjectionMetadata> injectionMetadataCache = new ConcurrentHashMap<>(1);
    private ConfigurableListableBeanFactory configurableFactory;

    @Override
    public void setBeanFactory(BeanFactory beanFactory){
        super.setBeanFactory(beanFactory);
        configurableFactory = (ConfigurableListableBeanFactory)beanFactory;
    }

    @Override
    public PropertyValues postProcessProperties(@Nonnull PropertyValues pvs, Object bean, @Nonnull String beanName) {
        InjectionMetadata metadata = searchAutowiringMetadata(beanName, bean.getClass(), pvs);
        return doPostProcessProperties(metadata, pvs, bean, beanName);
    }
    private PropertyValues doPostProcessProperties(InjectionMetadata metadata,
                                           @Nonnull PropertyValues pvs, Object bean, @Nonnull String beanName){
        try {
            metadata.inject(bean, beanName, pvs);
        } catch (BeanCreationException ex) {
            throw ex;
        } catch (Throwable ex) {
            throw new BeanCreationException(beanName, "Injection of autowired dependencies failed", ex);
        }
        return pvs;
    }
    private InjectionMetadata getOrBuildAutowiringMetadata(String cacheKey, Class<?> clazz, PropertyValues pvs){
        InjectionMetadata metadata = injectionMetadataCache.get(cacheKey);
        if (InjectionMetadata.needsRefresh(metadata, clazz)) {
            clear(metadata, pvs);
            metadata = makeAutowiringMetadata(clazz);
            injectionMetadataCache.put(cacheKey, metadata);
        }
        return metadata;
    }
    private InjectionMetadata searchAutowiringMetadata(String beanName, Class<?> clazz, PropertyValues pvs) {
        // Fall back to class name as cache key, for backwards compatibility with custom callers.
        String cacheKey = (StringUtils.hasLength(beanName) ? beanName : clazz.getName());
        // Quick check on the concurrent map first, with minimal locking.
        InjectionMetadata metadata = injectionMetadataCache.get(cacheKey);
        if (InjectionMetadata.needsRefresh(metadata, clazz)) {
            synchronized (injectionMetadataCache) {
                metadata = getOrBuildAutowiringMetadata(cacheKey, clazz, pvs);
            }
        }
        return metadata;
    }
    static Method findCleanMethod(Class<?> clazz){
        try {
            return clazz.getMethod("clear", PropertyValues.class);
        } catch (NoSuchMethodException e) {
            log.warn("没有找到clear的方法");
            return null;
        }
    }
    private static void doClean(Method method, InjectionMetadata obj, PropertyValues param){
        if (method != null) {
            try {
                method.invoke(obj, param);
            } catch (IllegalAccessException | InvocationTargetException e) {
                throw new IllegalStateException(e);
            }
        }
    }
    private void initCleanMethod(){
        if(!clearInit){
            cleanMethod = findCleanMethod(InjectionMetadata.class);
            clearInit = true;
        }
    }
    private void clear(InjectionMetadata obj, PropertyValues param) {
        if (null == obj){
            return;
        }
        initCleanMethod();

        doClean(cleanMethod, obj, param);
    }

    private void addCacheBeanField(CacheBean cacheBean, Field field, LinkedList<InjectionMetadata.InjectedElement> currElements){
        if (cacheBean != null) {
            if (Modifier.isStatic(field.getModifiers())) {
                log.warn("Autowired annotation is not supported on static fields: {}", field);
                return;
            }
            currElements.add(new AutowiredCacheFieldElement(configurableFactory, field, cacheBean));
        }
    }

    private InjectionMetadata makeAutowiringMetadata(final Class<?> clazz) {
        LinkedList<InjectionMetadata.InjectedElement> elements = new LinkedList<>();
        Class<?> targetClass = clazz;

        do {
            final LinkedList<InjectionMetadata.InjectedElement> currElements = new LinkedList<>();

            doWithLocalFields(targetClass, field -> {
                CacheBean cacheBean = field.getAnnotation(CacheBean.class);
                addCacheBeanField(cacheBean, field, currElements);
            });

            elements.addAll(0, currElements);
            targetClass = targetClass.getSuperclass();
        }
        while (isClassNeedDoWith(targetClass));

        return new InjectionMetadata(clazz, elements);
    }
    private static boolean isClassNeedDoWith(Class<?> clazz){
        return clazz != null && clazz != Object.class;
    }

    private static void doWithLocalFields(Class<?> clazz, ReflectionUtils.FieldCallback fieldCallback) {
        Field[] fields = clazz.getDeclaredFields();
        for (Field field : fields) {
            try {
                fieldCallback.doWith(field);
            } catch (IllegalAccessException ex) {
                throw new IllegalStateException("Not allowed to access field '" + field.getName() + "': " + ex);
            }
        }
    }

    static class AutowiredCacheFieldElement extends InjectionMetadata.InjectedElement {
        private Field field;
        private CacheBean cacheBean;
        private ConfigurableListableBeanFactory configurableFactory;

        AutowiredCacheFieldElement(ConfigurableListableBeanFactory configurableFactory, Field field, CacheBean cacheBean) {
            super(field, null);
            this.configurableFactory = configurableFactory;
            this.field = field;
            this.cacheBean = cacheBean;
        }

        @Override
        public boolean equals(@Nullable Object other){
            return super.equals(other);
        }
        @Override
        public int hashCode(){
            return super.hashCode();
        }

        @Override
        protected void inject(@Nonnull Object bean, String beanName, PropertyValues pvs) {
            configurableFactory.registerDependentBean(beanName, "cacheLayerFactory");
            Type fieldType = field.getType();
            Class<?> fieldClass = (Class<?>)fieldType;
            LazyCache<?, ?, ?> cache;
            if (ListCache.class.isAssignableFrom(fieldClass)) {
                cache = new LazyListCache<>(configurableFactory, cacheBean, field);
            }else if (SetCache.class.isAssignableFrom(fieldClass)) {
                cache = new LazySetCache<>(configurableFactory, cacheBean, field);
            }else if (HashCache.class.isAssignableFrom(fieldClass)){
                cache = new LazyHashCache<>(configurableFactory, cacheBean, field);
            }else{
                cache = new LazySimpleCache<>(configurableFactory, cacheBean, field);
            }
            Reflect.on(bean).set(field.getName(), cache);
        }
    }
}
