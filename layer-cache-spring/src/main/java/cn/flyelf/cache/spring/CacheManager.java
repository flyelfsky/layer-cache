package cn.flyelf.cache.spring;

import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CachePolicy;

/**
 * 缓存管理器接口定义
 *
 * @author wujr
 * 2019-12-23
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-23 1.0 新增]
 */
public interface CacheManager {
    /**
     * 根据缓存的名称获取缓存对象
     * @author wujr
     * 2019-12-23
     * 变更历史
     * [wujr 2019-12-23 1.0 新增]
     *
     * @param name: 缓存名称
     * @param <K>: 缓存key类型
     * @param <V>: 缓存对象类型
     * @return 缓存对象
     */
    <K, V> Cache<K, V> getCache(String name);

    /**
     * 获取或者创建一个缓存对象
     * @author wujr
     * 2019/12/24
     * 变更历史
     * [wujr 2019/12/24 1.0 新增]
     *
     * @param factory: 缓存工厂
     * @param policy: 缓存策略
     * @param keyClass: 缓存key类
     * @param serializeClass: 序列化类
     * @return 缓存对象
     */
    Cache<?, ?> getOrCreate(CacheLayerFactory factory, CachePolicy policy, Class<?> keyClass, Class<?> serializeClass);

    /**
     * 加入一个新的缓存
     * @author wujr
     * 2019-12-23
     * 变更历史
     * [wujr 2019-12-23 1.0 新增]
     *
     * @param cache: 缓存
     */
    void addCache(Cache<?, ?> cache);
}
