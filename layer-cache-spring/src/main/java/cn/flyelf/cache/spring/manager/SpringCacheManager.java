package cn.flyelf.cache.spring.manager;

import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.cache.DefaultHashCache;
import cn.flyelf.cache.core.cache.DefaultListCache;
import cn.flyelf.cache.core.cache.DefaultSetCache;
import cn.flyelf.cache.core.cache.DefaultSimpleCache;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.spring.CacheManager;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 缓存管理器具体实现
 *
 * @author wujr
 * 2019-12-23
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-23 1.0 新增]
 */
public class SpringCacheManager implements CacheManager {
    /**
     * 缓存bean对象映射
     * key：缓存的策略名称，具有全局唯一性
     */
    private final ConcurrentHashMap<String, Cache<?, ?>> caches = new ConcurrentHashMap<>();

    @Override
    @SuppressWarnings("unchecked")
    public <K, V> Cache<K, V> getCache(String name) {
        return (Cache<K, V>)caches.get(name);
    }

    @Override
    public void addCache(Cache<?, ?> cache){
        caches.put(cache.name(), cache);
    }

    @Override
    public Cache<?, ?> getOrCreate(CacheLayerFactory factory, CachePolicy policy, Class<?> keyClass, Class<?> serializeClass){
        return caches.computeIfAbsent(policy.name(), k -> {
            if (CacheConstant.CACHE_LIST.equals(policy.getType())){
                return new DefaultListCache<>(factory, policy.getLayer(), keyClass, policy.getObject(), policy.getFeature());
            }else if (CacheConstant.CACHE_SET.equals(policy.getType())){
                return new DefaultSetCache<>(factory, policy.getLayer(), keyClass, policy.getObject(), policy.getFeature());
            }else if (CacheConstant.CACHE_MAP.equals(policy.getType())){
                return new DefaultHashCache<>(factory, policy.getLayer(), keyClass, policy.getObject(), policy.getFeature());
            }else{
                return new DefaultSimpleCache<>(factory, policy.getLayer(), keyClass, policy.getObject(), serializeClass, policy.getFeature());
            }
        });
    }
}
