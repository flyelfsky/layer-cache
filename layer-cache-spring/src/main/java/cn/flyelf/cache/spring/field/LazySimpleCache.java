package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.core.Cache;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;

import java.lang.reflect.Field;

/**
 * 简单对象的缓存懒加载
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2020/1/10
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/10 1.0 新增]
 */
public class LazySimpleCache<K, V> extends LazyCache<K, V, Cache<K, V>>{
    LazySimpleCache(ConfigurableListableBeanFactory beanFactory, CacheBean cacheBean, Field field){
        super(beanFactory, cacheBean, field);
    }
}