package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.ListCache;
import cn.flyelf.cache.core.model.CacheResult;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.util.List;

/**
 * ListCache的懒加载
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2020-01-09
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-09 1.0 新增]
 */
public class LazyListCache<K, V> extends LazyCache<K, List<V>, ListCache<K, V>> implements ListCache<K, V> {
    LazyListCache(ConfigurableListableBeanFactory beanFactory, CacheBean cacheBean, Field field){
        super(beanFactory, cacheBean, field);
    }
    @Override
    String cacheType(){
        return CacheConstant.CACHE_LIST;
    }

    @Override
    public Mono<CacheResult<V>> rpop(K key) {
        check();
        return cache.rpop(key);
    }

    @Override
    public Mono<CacheResult<V>> lpop(K key) {
        check();
        return cache.lpop(key);
    }

    @Override
    public Mono<CacheResult<Boolean>> lpush(K key, List<V> value) {
        check();
        return cache.lpush(key, value);
    }

    @SafeVarargs
    @Override
    public final Mono<CacheResult<Boolean>> lpush(K key, V... value) {
        check();
        return cache.lpush(key, value);
    }

    @Override
    public Mono<CacheResult<Boolean>> rpush(K key, List<V> value) {
        check();
        return cache.rpush(key, value);
    }

    @SafeVarargs
    @Override
    public final Mono<CacheResult<Boolean>> rpush(K key, V... value) {
        check();
        return cache.rpush(key, value);
    }
}
