package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.HashCache;
import cn.flyelf.cache.core.model.CacheResult;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * hash类型的缓存懒加载
 * @param <K>: 缓存key类型
 * @param <H>: 缓存的map的key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2020/1/10
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/10 1.0 新增]
 */
public class LazyHashCache<K, H, V> extends LazyCache<K, Map<H, V>, HashCache<K, H, V>> implements HashCache<K, H, V> {
    LazyHashCache(ConfigurableListableBeanFactory beanFactory, CacheBean cacheBean, Field field){
        super(beanFactory, cacheBean, field);
    }

    @Override
    String cacheType(){
        return CacheConstant.CACHE_MAP;
    }

    @Override
    public Mono<CacheResult<Boolean>> exist(K key, H hk) {
        check();
        return cache.exist(key, hk);
    }

    @Override
    public Mono<CacheResult<Long>> remove(K key, Collection<H> hks) {
        check();
        return cache.remove(key, hks);
    }

    @SafeVarargs
    @Override
    public final Mono<CacheResult<Long>> remove(K key, H... hks) {
        check();
        return cache.remove(key, hks);
    }

    @Override
    public Mono<CacheResult<List<V>>> get(K key, Collection<H> hks) {
        check();
        return cache.get(key, hks);
    }

    @SafeVarargs
    @Override
    public final Mono<CacheResult<List<V>>> get(K key, H... hks) {
        check();
        return cache.get(key, hks);
    }

    @Override
    public Mono<CacheResult<Boolean>> put(K key, Map<H, V> value) {
        check();
        return cache.put(key, value);
    }
}
