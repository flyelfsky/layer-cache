package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheObjectLoader;
import cn.flyelf.cache.core.util.TypeUtil;
import cn.flyelf.cache.spring.CacheManager;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.Collection;

/**
 * 延迟加载的缓存
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * @param <T>: 缓存实例类型
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
@Slf4j
public class LazyCache<K, V, T extends Cache<K, V>> implements Cache<K, V> {
    protected T cache;
    private ConfigurableListableBeanFactory beanFactory;
    private CacheBean cacheBean;
    private Field field;

    LazyCache(ConfigurableListableBeanFactory beanFactory, CacheBean cacheBean, Field field){
        this.beanFactory = beanFactory;
        this.cacheBean = cacheBean;
        this.field = field;
    }
    @Override
    public String name() {
        check();
        return cache.name();
    }

    @Override
    public Cache actual(){
        check();
        return cache;
    }

    @Override
    public Mono<CacheResult<V>> get(K key, CacheObjectLoader<K, V> loader) {
        check();
        return cache.get(key, loader);
    }

    @Override
    public Mono<CacheResult<Boolean>> put(K key, V value) {
        check();
        return cache.put(key, value);
    }

    @Override
    public Mono<CacheResult<Long>> delete(K key) {
        check();
        return cache.delete(key);
    }

    @SafeVarargs
    @Override
    public final Mono<CacheResult<Long>> delete(K... keys) {
        check();
        return cache.delete(keys);
    }

    @Override
    public Mono<CacheResult<Long>> delete(Collection<K> keys) {
        check();
        return cache.delete(keys);
    }

    void check(){
        if (null == cache){
            synchronized (this){
                init();
            }
        }
    }
    String cacheType(){
        return CacheConstant.UNDEFINED_STRING;
    }
    @SuppressWarnings("unchecked")
    private void init(){
        if (null != cache){
            return;
        }
        CacheLayerFactory factory = beanFactory.getBean(CacheLayerFactory.class);
        CachePolicy policy = new CachePolicy();
        policy.setLayer(cacheBean.layer());
        if (!CacheConstant.isUndefined(cacheType())){
            policy.setType(cacheType());
        }
        if (StringUtils.isBlank(policy.getType()) && !CacheConstant.isUndefined(cacheBean.type())){
            policy.setType(cacheBean.type());
        }

        if (!CacheConstant.isUndefined(cacheBean.expire())){
            policy.setDuration(cacheBean.expire());
        }
        policy.setTimeUnit(cacheBean.timeUnit());
        if (!CacheConstant.isUndefined(cacheBean.feature())){
            policy.setFeature(cacheBean.feature());
        }
        if (!CacheConstant.isUndefined(cacheBean.limit())){
            policy.setLimit(cacheBean.limit());
        }
        ParameterizedType type = (ParameterizedType)field.getGenericType();
        Type[] types = type.getActualTypeArguments();
        if (!CacheConstant.isUndefined(cacheBean.object())) {
            policy.setObject(cacheBean.object());
        }else{
            // 获取Cache<K, V>中的V的类
            policy.setObject(TypeUtil.resolverTypeClass(types[1]));
        }
        Class serializeClass = policy.getObject();
        if (StringUtils.isBlank(policy.getType()) && CacheConstant.isUndefined(cacheBean.type())){
            policy.setType(TypeUtil.resolveCacheType(types[1]));
        }
        if (CacheConstant.CACHE_SIMPLE.equals(policy.getType())){
            serializeClass = TypeUtil.resolverRawType(types[1]);
        }
        policy.setCacheNull(cacheBean.nullable());
        if (cacheBean.nullExpire() != Long.MAX_VALUE){
            policy.setNullExpire(cacheBean.nullExpire());
        }

        policy = factory.addPolicy(policy);
        CacheManager cacheManager = beanFactory.getBean(CacheManager.class);
        cache = (T)cacheManager.getOrCreate(factory, policy, (Class)types[0], serializeClass);
        log.info("@CacheBean：{}，缓存：{}，策略：{} - {}", field.getName(), cache, policy.name(), policy);
    }
}
