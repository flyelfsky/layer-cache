package cn.flyelf.cache.spring;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
 * 用于reflect调用的诊断匹配器
 *
 * @author wujr
 * 2020/1/9
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/9 1.0 新增]
 */
public class ReflectCauseMatcher extends TypeSafeMatcher<Throwable> {
    private final Class<? extends Throwable> type;

    public ReflectCauseMatcher(Class<? extends Throwable> type){
        this.type = type;
    }
    @Override
    protected boolean matchesSafely(Throwable item) {
        Throwable cause = item.getCause();
        if (null == cause){
            return item.getClass().isAssignableFrom(type);
        }
        return cause.getClass().isAssignableFrom(type);
    }

    @Override
    public void describeTo(Description description) {
        description.appendText("expects type ")
                .appendValue(type);
    }
}
