package cn.flyelf.cache.spring.model;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.core.HashCache;
import cn.flyelf.cache.core.ListCache;
import cn.flyelf.cache.core.SetCache;
import cn.flyelf.cache.core.model.CacheTestModel;
import lombok.Data;

import java.util.List;

/**
 * 用于测试@CacheBean的子模型
 *
 * @author wujr
 * 2020-01-08
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-08 1.0 新增]
 */
@Data
public class CacheBeanAutowireChild extends CacheBeanAutowireSuper{
    @CacheBean
    private Cache<String, List<CacheTestModel>> cacheList;
    @CacheBean
    private ListCache<String, CacheTestModel> cacheList2;
    @CacheBean
    private SetCache<String, CacheTestModel> cacheSet2;
    @CacheBean
    private HashCache<String, Long, CacheTestModel> cacheHash2;
}
