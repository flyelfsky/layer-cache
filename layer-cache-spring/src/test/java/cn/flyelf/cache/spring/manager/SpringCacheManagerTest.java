package cn.flyelf.cache.spring.manager;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheTestModel;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

/**
 * SpringCacheManager的测试用例
 *
 * @author wujr
 * 2020/1/8
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/8 1.0 新增]
 */
public class SpringCacheManagerTest {
    @Test
    public void testAddAndGetCache(){
        Cache cache = Mockito.mock(Cache.class);
        Mockito.when(cache.name()).thenReturn("test");

        SpringCacheManager manager = new SpringCacheManager();
        manager.addCache(cache);
        Cache result = manager.getCache("test");
        Assert.assertNotNull(result);

        result = manager.getCache("no");
        Assert.assertNull(result);
    }

    @Test
    public void testGetOrDefaultList(){
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_LIST);
        policy.setLayer(new String[]{"test"});

        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(policy.getLayer(), CacheTestModel.class, CacheConstant.CACHE_LIST, null)).thenReturn(policy);

        SpringCacheManager manager = new SpringCacheManager();
        Cache result = manager.getOrCreate(factory, policy, String.class, CacheTestModel.class);
        Assert.assertNotNull(result);

        Cache cache = manager.getOrCreate(factory, policy, String.class, CacheTestModel.class);
        Assert.assertEquals(result, cache);
    }
    @Test
    public void testGetOrDefaultSet(){
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_SET);
        policy.setLayer(new String[]{"test"});

        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(policy.getLayer(), CacheTestModel.class, CacheConstant.CACHE_SET, null)).thenReturn(policy);

        SpringCacheManager manager = new SpringCacheManager();
        Cache result = manager.getOrCreate(factory, policy, String.class, CacheTestModel.class);
        Assert.assertNotNull(result);

        Cache cache = manager.getOrCreate(factory, policy, String.class, CacheTestModel.class);
        Assert.assertEquals(result, cache);
    }
    @Test
    public void testGetOrDefaultMap(){
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_MAP);
        policy.setLayer(new String[]{"test"});

        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(policy.getLayer(), CacheTestModel.class, CacheConstant.CACHE_MAP, null)).thenReturn(policy);

        SpringCacheManager manager = new SpringCacheManager();
        Cache result = manager.getOrCreate(factory, policy, String.class, CacheTestModel.class);
        Assert.assertNotNull(result);

        Cache cache = manager.getOrCreate(factory, policy, String.class, CacheTestModel.class);
        Assert.assertEquals(result, cache);
    }
    @Test
    public void testGetOrDefaultSimple(){
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setType(CacheConstant.CACHE_SIMPLE);
        policy.setLayer(new String[]{"test"});

        CacheLayerFactory factory = Mockito.mock(CacheLayerFactory.class);
        Mockito.when(factory.findPolicy(policy.getLayer(), CacheTestModel.class, CacheConstant.CACHE_SIMPLE, null)).thenReturn(policy);

        SpringCacheManager manager = new SpringCacheManager();
        Cache result = manager.getOrCreate(factory, policy, String.class, CacheTestModel.class);
        Assert.assertNotNull(result);

        Cache cache = manager.getOrCreate(factory, policy, String.class, CacheTestModel.class);
        Assert.assertEquals(result, cache);
    }
}
