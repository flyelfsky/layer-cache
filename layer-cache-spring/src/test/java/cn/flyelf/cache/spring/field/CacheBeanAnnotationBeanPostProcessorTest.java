package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.spring.ReflectCauseMatcher;
import cn.flyelf.cache.spring.model.CacheBeanAutowireChild;
import org.joor.Reflect;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.mockito.Mockito;
import org.springframework.beans.PropertyValues;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.InjectionMetadata;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.util.ReflectionUtils;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.lang.reflect.Modifier;
import java.util.Collection;
import java.util.LinkedList;


/**
 * CacheBeanAnnotationBeanPostProcessor的测试用例
 *
 * @author wujr
 * 2020/1/8
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/8 1.0 新增]
 */
public class CacheBeanAnnotationBeanPostProcessorTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private static CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();

    @Test
    public void testSetBeanFactory(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();
        postProcessor.setBeanFactory(beanFactory);
        Assert.assertTrue(true);
    }
    @Test
    public void testFindCleanMethodNull(){
        Method result = CacheBeanAnnotationBeanPostProcessor.findCleanMethod(CacheTestModel.class);
        Assert.assertNull(result);
    }

    @Test
    public void testDoCleanMethodNull(){
        InjectionMetadata metadata = Mockito.mock(InjectionMetadata.class);
        PropertyValues propertyValues = Mockito.mock(PropertyValues.class);
        Reflect.onClass(CacheBeanAnnotationBeanPostProcessor.class).call("doClean", null, metadata, propertyValues);
        Assert.assertTrue(true);
    }
    @Test
    public void testDoCleanMethodExp() throws Exception{
        thrown.expectCause(new ReflectCauseMatcher(IllegalStateException.class));
        InjectionMetadata metadata = Mockito.mock(InjectionMetadata.class);
        PropertyValues propertyValues = Mockito.mock(PropertyValues.class);
        Method method = Mockito.mock(Method.class);
        Mockito.when(method.invoke(metadata, propertyValues)).thenThrow(new IllegalAccessException());
        Reflect.onClass(CacheBeanAnnotationBeanPostProcessor.class).call("doClean", method, metadata, propertyValues);
        Assert.assertTrue(true);
    }
    @Test
    public void testInitCleanMethod(){
        CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();
        Reflect.on(postProcessor).call("initCleanMethod");
        Reflect.on(postProcessor).call("initCleanMethod");
        Assert.assertTrue(true);
    }

    @Test
    public void testCleanNullMeta(){
        CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();
        PropertyValues propertyValues = Mockito.mock(PropertyValues.class);
        Reflect.on(postProcessor).call("clear", null, propertyValues);
        Assert.assertTrue(true);
    }
    @Test
    public void testClear(){
        CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();
        InjectionMetadata metadata = Mockito.mock(InjectionMetadata.class);
        PropertyValues propertyValues = Mockito.mock(PropertyValues.class);
        Reflect.on(postProcessor).call("clear", metadata, propertyValues);
        Assert.assertTrue(true);
    }

    @Test
    public void testDoWithLocalFields(){
        ReflectionUtils.FieldCallback callback = Mockito.mock(ReflectionUtils.FieldCallback.class);
        Class clazz = CacheTestModel.class;
        Reflect.onClass(CacheBeanAnnotationBeanPostProcessor.class).call("doWithLocalFields", clazz, callback);
        Assert.assertTrue(true);
    }
    @Test
    public void testDoWithLocalFieldsExp() throws Exception{
        thrown.expectCause(new ReflectCauseMatcher(IllegalStateException.class));
        ReflectionUtils.FieldCallback callback = Mockito.mock(ReflectionUtils.FieldCallback.class);
        Mockito.doThrow(new IllegalAccessException()).when(callback).doWith(Mockito.any());
        Class clazz = CacheTestModel.class;
        Reflect.onClass(CacheBeanAnnotationBeanPostProcessor.class).call("doWithLocalFields", clazz, callback).as(IllegalStateException.class);
        Assert.assertTrue(true);
    }

    @Test
    public void testAddCacheBeanFieldNullBean(){
        Reflect.on(postProcessor).call("addCacheBeanField", null, null, null);
        Assert.assertTrue(true);
    }
    @Test
    public void testAddCacheBeanFieldStatic(){
        LinkedList<InjectionMetadata.InjectedElement> currElements = new LinkedList<>();
        Field field = Mockito.mock(Field.class);
        Mockito.when(field.getModifiers()).thenReturn(Modifier.STATIC);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Reflect.on(postProcessor).call("addCacheBeanField", bean, field, currElements);
        Assert.assertTrue(true);
    }
    @Test
    public void testAddCacheBeanField(){
        LinkedList<InjectionMetadata.InjectedElement> currElements = new LinkedList<>();
        Field field = Mockito.mock(Field.class);
        Mockito.when(field.getModifiers()).thenReturn(Modifier.PRIVATE);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Reflect.on(postProcessor).call("addCacheBeanField", bean, field, currElements);
        Assert.assertFalse(currElements.isEmpty());
    }

    @Test
    public void testMakeAutowiringMetadata(){
        Reflect reflect = Reflect.on(postProcessor).call("makeAutowiringMetadata", CacheBeanAutowireChild.class);
        InjectionMetadata metadata = reflect.get();
        Assert.assertNotNull(metadata);
        Field field = ReflectionUtils.findField(InjectionMetadata.class, "injectedElements");
        Assert.assertNotNull(field);
        ReflectionUtils.makeAccessible(field);
        Object elements = ReflectionUtils.getField(field, metadata);
        Assert.assertNotNull(elements);
        Assert.assertTrue(elements instanceof Collection);
        Assert.assertEquals(5, ((Collection)elements).size());
    }
    @Test
    public void testGetOrBuildAutowiringMetadata(){
        PropertyValues pvs = Mockito.mock(PropertyValues.class);
        CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();
        Class clazz = CacheBeanAutowireChild.class;
        Reflect reflect = Reflect.on(postProcessor).call("getOrBuildAutowiringMetadata", "key", clazz, pvs);
        InjectionMetadata metadata = reflect.get();
        Assert.assertNotNull(metadata);
        reflect = Reflect.on(postProcessor).call("getOrBuildAutowiringMetadata", "key", clazz, pvs);
        InjectionMetadata result = reflect.get();
        Assert.assertEquals(metadata, result);
    }

    @Test
    public void testSearchAutowiringMetadataBeanNameNull(){
        PropertyValues pvs = Mockito.mock(PropertyValues.class);
        CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();
        Reflect reflect = Reflect.on(postProcessor).call("searchAutowiringMetadata", null, CacheBeanAutowireChild.class, pvs);
        InjectionMetadata metadata = reflect.get();
        Assert.assertNotNull(metadata);
        reflect = Reflect.on(postProcessor).call("searchAutowiringMetadata", null, CacheBeanAutowireChild.class, pvs);
        InjectionMetadata result = reflect.get();
        Assert.assertEquals(metadata, result);
    }
    @Test
    public void testSearchAutowiringMetadata(){
        PropertyValues pvs = Mockito.mock(PropertyValues.class);
        CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();
        Reflect reflect = Reflect.on(postProcessor).call("searchAutowiringMetadata", "bean", CacheBeanAutowireChild.class, pvs);
        InjectionMetadata metadata = reflect.get();
        Assert.assertNotNull(metadata);
        reflect = Reflect.on(postProcessor).call("searchAutowiringMetadata", "bean", CacheBeanAutowireChild.class, pvs);
        InjectionMetadata result = reflect.get();
        Assert.assertEquals(metadata, result);
    }

    @Test
    public void testDoPostProcessPropertiesBeanExp() throws Throwable{
        thrown.expectCause(new ReflectCauseMatcher(BeanCreationException.class));
        PropertyValues pvs = Mockito.mock(PropertyValues.class);
        InjectionMetadata metadata = Mockito.mock(InjectionMetadata.class);
        Mockito.doThrow(new BeanCreationException("")).when(metadata).inject(Mockito.any(), Mockito.anyString(), Mockito.any());
        Object bean = new CacheBeanAutowireChild();
        Reflect.on(postProcessor).call("doPostProcessProperties", metadata, pvs, bean, "bean");
        Assert.assertTrue(true);
    }
    @Test
    public void testDoPostProcessPropertiesThrowableExp() throws Throwable{
        thrown.expectCause(new ReflectCauseMatcher(BeanCreationException.class));
        PropertyValues pvs = Mockito.mock(PropertyValues.class);
        InjectionMetadata metadata = Mockito.mock(InjectionMetadata.class);
        Mockito.doThrow(new Throwable("")).when(metadata).inject(Mockito.any(), Mockito.anyString(), Mockito.any());
        Object bean = new CacheBeanAutowireChild();
        Reflect.on(postProcessor).call("doPostProcessProperties", metadata, pvs, bean, "bean");
        Assert.assertTrue(true);
    }

    @Test
    public void testPostProcessProperties(){
        CacheBeanAnnotationBeanPostProcessor postProcessor = new CacheBeanAnnotationBeanPostProcessor();
        PropertyValues pvs = Mockito.mock(PropertyValues.class);
        ConfigurableListableBeanFactory factory = Mockito.mock(ConfigurableListableBeanFactory.class);
        postProcessor.setBeanFactory(factory);
        PropertyValues result = postProcessor.postProcessProperties(pvs, new CacheBeanAutowireChild(), "bean");
        Assert.assertEquals(pvs, result);
    }

    @Test
    public void testIsClassNeedDoWithNull(){
        Class clazz = null;
        Reflect reflect = Reflect.onClass(CacheBeanAnnotationBeanPostProcessor.class).call("isClassNeedDoWith", clazz);
        Assert.assertFalse(reflect.get());
    }
    @Test
    public void testIsClassNeedDoWith(){
        Class clazz = Object.class;
        Reflect reflect = Reflect.onClass(CacheBeanAnnotationBeanPostProcessor.class).call("isClassNeedDoWith", clazz);
        Assert.assertFalse(reflect.get());
    }

    @Test
    public void testAutowiredCacheFieldElementEqualAndCode(){
        Field field = Mockito.mock(Field.class);
        Mockito.when(field.getName()).thenReturn("field");
        CacheBean cacheBean = Mockito.mock(CacheBean.class);
        ConfigurableListableBeanFactory configurableFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBeanAnnotationBeanPostProcessor.AutowiredCacheFieldElement element =
                new CacheBeanAnnotationBeanPostProcessor.AutowiredCacheFieldElement(configurableFactory, field, cacheBean);
        CacheBeanAnnotationBeanPostProcessor.AutowiredCacheFieldElement other =
                new CacheBeanAnnotationBeanPostProcessor.AutowiredCacheFieldElement(configurableFactory, field, cacheBean);
        Assert.assertEquals(element, other);
        Assert.assertEquals(element.hashCode(), other.hashCode());
    }
    @Test
    public void testAutowiredCacheFieldElementEqualAndCodeNot(){
        Field field = Mockito.mock(Field.class);
        Mockito.when(field.getName()).thenReturn("field");
        CacheBean cacheBean = Mockito.mock(CacheBean.class);
        ConfigurableListableBeanFactory configurableFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBeanAnnotationBeanPostProcessor.AutowiredCacheFieldElement element =
                new CacheBeanAnnotationBeanPostProcessor.AutowiredCacheFieldElement(configurableFactory, field, cacheBean);
        CacheTestModel other = new CacheTestModel();
        Assert.assertNotEquals(element, other);
        Assert.assertNotEquals(element.hashCode(), other.hashCode());
    }
}
