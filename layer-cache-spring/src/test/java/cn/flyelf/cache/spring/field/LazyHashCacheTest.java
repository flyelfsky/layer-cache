package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.HashCache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.spring.CacheManager;
import org.joor.Reflect;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * LazyHashCache的测试用例
 *
 * @author wujr
 * 2020/1/10
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/10 1.0 新增]
 */
public class LazyHashCacheTest {
    private static final String[] layer = new String[]{"test"};
    @Test
    public void testCacheType(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        LazyCache lazyCache = new LazyHashCache(beanFactory, bean, field);
        String name = lazyCache.cacheType();
        Assert.assertEquals(CacheConstant.CACHE_MAP, name);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testExist(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        HashCache cache = Mockito.mock(HashCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", true));
        Mockito.when(cache.exist("key", "test")).thenReturn(mono);
        LazyHashCache lazyCache = new LazyHashCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.exist("key", "test");
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRemoveList(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        HashCache cache = Mockito.mock(HashCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", 1L));
        List<String> records = new ArrayList<>(1);
        records.add("test");
        Mockito.when(cache.remove("key", records)).thenReturn(mono);
        LazyHashCache lazyCache = new LazyHashCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.remove("key", records);
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRemoveArray(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        HashCache cache = Mockito.mock(HashCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", 1L));
        Mockito.when(cache.remove("key", "test")).thenReturn(mono);
        LazyHashCache lazyCache = new LazyHashCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.remove("key", "test");
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testGetList(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        HashCache cache = Mockito.mock(HashCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", 1L));
        List<String> records = new ArrayList<>(1);
        records.add("test");
        Mockito.when(cache.get("key", records)).thenReturn(mono);
        LazyHashCache lazyCache = new LazyHashCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.get("key", records);
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testGetArray(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        HashCache cache = Mockito.mock(HashCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", 1L));
        Mockito.when(cache.get("key", "test")).thenReturn(mono);
        LazyHashCache lazyCache = new LazyHashCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.get("key", "test");
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testPutMap(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        HashCache cache = Mockito.mock(HashCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", 1L));
        Map<Long, String> records = new HashMap<>(1);
        records.put(1L, "test");
        Mockito.when(cache.put("key", records)).thenReturn(mono);
        LazyHashCache lazyCache = new LazyHashCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.put("key", records);
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testInit(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Mockito.when(bean.type()).thenReturn(CacheConstant.UNDEFINED_STRING);
        Mockito.when(bean.expire()).thenReturn(CacheConstant.UNDEFINED_LONG);
        Mockito.when(bean.feature()).thenReturn(CacheConstant.UNDEFINED_STRING);
        Mockito.when(bean.limit()).thenReturn(CacheConstant.UNDEFINED_LONG);
        Mockito.when(bean.object()).thenReturn((Class) CacheTestModel.class);
        Mockito.when(bean.nullExpire()).thenReturn(Long.MAX_VALUE);
        Mockito.when(bean.layer()).thenReturn(layer);

        Field field = Mockito.mock(Field.class);

        CacheLayerFactory factory = new CacheLayerFactory();
        Mockito.when(beanFactory.getBean(CacheLayerFactory.class)).thenReturn(factory);

        ParameterizedType parameterizedType = Mockito.mock(ParameterizedType.class);
        Mockito.when(field.getGenericType()).thenReturn(parameterizedType);

        Type type0 = String.class;
        Type type1 = Mockito.mock(Type.class);
        Type[] types = new Type[]{type0, type1};
        Mockito.when(parameterizedType.getActualTypeArguments()).thenReturn(types);

        CacheManager cacheManager = Mockito.mock(CacheManager.class);
        Mockito.when(beanFactory.getBean(CacheManager.class)).thenReturn(cacheManager);

        LazyCache cache = new LazyHashCache(beanFactory, bean, field);
        Reflect.on(cache).call("init");
        Assert.assertTrue(true);
    }
}
