package cn.flyelf.cache.spring.model;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.core.model.CacheTestModel;
import lombok.Data;

/**
 * 用于测试的CacheBean注入的模型
 *
 * @author wujr
 * 2020-01-08
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-08 1.0 新增]
 */
@Data
public class CacheBeanAutowireSuper {
    @CacheBean
    private Cache<String, CacheTestModel> cacheSimple;
}
