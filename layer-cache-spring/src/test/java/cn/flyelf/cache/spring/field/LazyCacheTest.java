package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.spring.CacheManager;
import org.joor.Reflect;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * LazyCache的测试用例
 *
 * @author wujr
 * 2020-01-08
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-08 1.0 新增]
 */
public class LazyCacheTest {
    @Test
    @SuppressWarnings("unchecked")
    public void testInit1(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Mockito.when(bean.object()).thenReturn((Class)CacheConstant.CacheUndefined.class);
        Field field = Mockito.mock(Field.class);

        CacheLayerFactory factory = new CacheLayerFactory();
        Mockito.when(beanFactory.getBean(CacheLayerFactory.class)).thenReturn(factory);

        ParameterizedType parameterizedType = Mockito.mock(ParameterizedType.class);
        Mockito.when(field.getGenericType()).thenReturn(parameterizedType);

        Type type0 = String.class;
        Type type1 = Mockito.mock(Type.class);
        Type[] types = new Type[]{type0, type1};
        Mockito.when(parameterizedType.getActualTypeArguments()).thenReturn(types);

        CacheManager cacheManager = Mockito.mock(CacheManager.class);
        Mockito.when(beanFactory.getBean(CacheManager.class)).thenReturn(cacheManager);

        LazyCache cache = new LazyCache(beanFactory, bean, field);
        Reflect.on(cache).call("init");
        Assert.assertTrue(true);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testInit2(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Mockito.when(bean.type()).thenReturn(CacheConstant.UNDEFINED_STRING);
        Mockito.when(bean.expire()).thenReturn(CacheConstant.UNDEFINED_LONG);
        Mockito.when(bean.feature()).thenReturn(CacheConstant.UNDEFINED_STRING);
        Mockito.when(bean.limit()).thenReturn(CacheConstant.UNDEFINED_LONG);
        Mockito.when(bean.object()).thenReturn((Class)CacheTestModel.class);
        Mockito.when(bean.nullExpire()).thenReturn(Long.MAX_VALUE);

        Field field = Mockito.mock(Field.class);

        CacheLayerFactory factory = new CacheLayerFactory();
        Mockito.when(beanFactory.getBean(CacheLayerFactory.class)).thenReturn(factory);

        ParameterizedType parameterizedType = Mockito.mock(ParameterizedType.class);
        Mockito.when(field.getGenericType()).thenReturn(parameterizedType);

        Type type0 = String.class;
        Type type1 = Mockito.mock(Type.class);
        Type[] types = new Type[]{type0, type1};
        Mockito.when(parameterizedType.getActualTypeArguments()).thenReturn(types);

        CacheManager cacheManager = Mockito.mock(CacheManager.class);
        Mockito.when(beanFactory.getBean(CacheManager.class)).thenReturn(cacheManager);

        LazyCache cache = new LazyCache(beanFactory, bean, field);
        Reflect.on(cache).call("init");
        Assert.assertTrue(true);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testCheck(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Mockito.when(bean.object()).thenReturn((Class)CacheConstant.CacheUndefined.class);
        Field field = Mockito.mock(Field.class);

        CacheLayerFactory factory = new CacheLayerFactory();
        Mockito.when(beanFactory.getBean(CacheLayerFactory.class)).thenReturn(factory);

        ParameterizedType parameterizedType = Mockito.mock(ParameterizedType.class);
        Mockito.when(field.getGenericType()).thenReturn(parameterizedType);

        Type type0 = String.class;
        Type type1 = Mockito.mock(Type.class);
        Type[] types = new Type[]{type0, type1};
        Mockito.when(parameterizedType.getActualTypeArguments()).thenReturn(types);

        CacheManager cacheManager = Mockito.mock(CacheManager.class);
        Mockito.when(beanFactory.getBean(CacheManager.class)).thenReturn(cacheManager);

        LazyCache cache = new LazyCache(beanFactory, bean, field);
        Reflect.on(cache).call("check");
        Reflect.on(cache).call("check");
        Assert.assertTrue(true);
    }

    @Test
    public void testInit3(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        Cache cache = Mockito.mock(Cache.class);
        LazyCache lazyCache = new LazyCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Reflect.on(lazyCache).call("init");
        Assert.assertTrue(true);
    }

    @Test
    public void testName(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        Cache cache = Mockito.mock(Cache.class);
        Mockito.when(cache.name()).thenReturn("test");
        LazyCache lazyCache = new LazyCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);
        String result = lazyCache.name();
        Assert.assertEquals("test", result);
    }

    @Test
    public void testActual(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        Cache cache = Mockito.mock(Cache.class);
        LazyCache lazyCache = new LazyCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);
        Cache result = lazyCache.actual();
        Assert.assertEquals(cache, result);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testGet(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        Cache cache = Mockito.mock(Cache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", "value"));
        Mockito.when(cache.get("key", null)).thenReturn(mono);
        LazyCache lazyCache = new LazyCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);
        Mono<CacheResult> result = lazyCache.get("key", null);
        Assert.assertEquals(mono, result);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testPut(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        Cache cache = Mockito.mock(Cache.class);
        Mono<CacheResult<Boolean>> mono = Mono.just(CacheResult.success("test", true));
        Mockito.when(cache.put("key", "value")).thenReturn(mono);
        LazyCache lazyCache = new LazyCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);
        Mono<CacheResult<Boolean>> result = lazyCache.put("key", "value");
        Assert.assertEquals(mono, result);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteOne(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        Cache cache = Mockito.mock(Cache.class);
        Mono<CacheResult<Long>> mono = Mono.just(CacheResult.success("test", 1L));
        Mockito.when(cache.delete("key")).thenReturn(mono);
        LazyCache lazyCache = new LazyCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);
        Mono<CacheResult<Long>> result = lazyCache.delete("key");
        Assert.assertEquals(mono, result);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteArray(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        Cache cache = Mockito.mock(Cache.class);
        Mono<CacheResult<Long>> mono = Mono.just(CacheResult.success("test", 1L));
        Mockito.when(cache.delete("key1", "key2")).thenReturn(mono);
        LazyCache lazyCache = new LazyCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);
        Mono<CacheResult<Long>> result = lazyCache.delete("key1", "key2");
        Assert.assertEquals(mono, result);
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testDeleteList(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        Cache cache = Mockito.mock(Cache.class);
        Mono<CacheResult<Long>> mono = Mono.just(CacheResult.success("test", 1L));
        List keys = new ArrayList<>(1);
        keys.add("key");
        Mockito.when(cache.delete(keys)).thenReturn(mono);
        LazyCache lazyCache = new LazyCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);
        Mono<CacheResult<Long>> result = lazyCache.delete(keys);
        Assert.assertEquals(mono, result);
    }
}
