package cn.flyelf.cache.spring.field;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.ListCache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.spring.CacheManager;
import org.joor.Reflect;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import reactor.core.publisher.Mono;

import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

/**
 * LazyListCache的测试用例
 *
 * @author wujr
 * 2020/1/10
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/10 1.0 新增]
 */
public class LazyListCacheTest {
    private static final String[] layer = new String[]{"test"};
    @Test
    public void testCacheType(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        LazyCache lazyCache = new LazyListCache(beanFactory, bean, field);
        String name = lazyCache.cacheType();
        Assert.assertEquals(CacheConstant.CACHE_LIST, name);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRpop(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        ListCache cache = Mockito.mock(ListCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", "object"));
        Mockito.when(cache.rpop("key")).thenReturn(mono);
        LazyListCache lazyCache = new LazyListCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.rpop("key");
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLpop(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        ListCache cache = Mockito.mock(ListCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", "object"));
        Mockito.when(cache.lpop("key")).thenReturn(mono);
        LazyListCache lazyCache = new LazyListCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.lpop("key");
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLpushList(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        ListCache cache = Mockito.mock(ListCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", true));
        List<String> records = new ArrayList<>();
        Mockito.when(cache.lpush("key", records)).thenReturn(mono);
        LazyListCache lazyCache = new LazyListCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.lpush("key", records);
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testLpushArray(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        ListCache cache = Mockito.mock(ListCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", true));
        Mockito.when(cache.lpush("key", "object")).thenReturn(mono);
        LazyListCache lazyCache = new LazyListCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.lpush("key", "object");
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRpushList(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        ListCache cache = Mockito.mock(ListCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", true));
        List<String> records = new ArrayList<>();
        Mockito.when(cache.rpush("key", records)).thenReturn(mono);
        LazyListCache lazyCache = new LazyListCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.rpush("key", records);
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testRpushArray(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Field field = Mockito.mock(Field.class);
        ListCache cache = Mockito.mock(ListCache.class);
        Mono<CacheResult> mono = Mono.just(CacheResult.success("test", true));
        Mockito.when(cache.rpush("key", "object")).thenReturn(mono);
        LazyListCache lazyCache = new LazyListCache(beanFactory, bean, field);
        Reflect.on(lazyCache).set("cache", cache);

        Mono<CacheResult> result = lazyCache.rpush("key", "object");
        Assert.assertEquals(mono, result);
    }
    @Test
    @SuppressWarnings("unchecked")
    public void testInit(){
        ConfigurableListableBeanFactory beanFactory = Mockito.mock(ConfigurableListableBeanFactory.class);
        CacheBean bean = Mockito.mock(CacheBean.class);
        Mockito.when(bean.type()).thenReturn(CacheConstant.UNDEFINED_STRING);
        Mockito.when(bean.expire()).thenReturn(CacheConstant.UNDEFINED_LONG);
        Mockito.when(bean.feature()).thenReturn(CacheConstant.UNDEFINED_STRING);
        Mockito.when(bean.limit()).thenReturn(CacheConstant.UNDEFINED_LONG);
        Mockito.when(bean.object()).thenReturn((Class) CacheTestModel.class);
        Mockito.when(bean.nullExpire()).thenReturn(Long.MAX_VALUE);
        Mockito.when(bean.layer()).thenReturn(layer);

        Field field = Mockito.mock(Field.class);

        CacheLayerFactory factory = new CacheLayerFactory();
        Mockito.when(beanFactory.getBean(CacheLayerFactory.class)).thenReturn(factory);

        ParameterizedType parameterizedType = Mockito.mock(ParameterizedType.class);
        Mockito.when(field.getGenericType()).thenReturn(parameterizedType);

        Type type0 = String.class;
        Type type1 = Mockito.mock(Type.class);
        Type[] types = new Type[]{type0, type1};
        Mockito.when(parameterizedType.getActualTypeArguments()).thenReturn(types);

        CacheManager cacheManager = Mockito.mock(CacheManager.class);
        Mockito.when(beanFactory.getBean(CacheManager.class)).thenReturn(cacheManager);

        LazyCache cache = new LazyListCache(beanFactory, bean, field);
        Reflect.on(cache).call("init");
        Assert.assertTrue(true);
    }
}
