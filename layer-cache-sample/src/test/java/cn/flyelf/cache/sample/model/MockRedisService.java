package cn.flyelf.cache.sample.model;

import ai.grakn.redismock.RedisServer;

import java.io.IOException;

/**
 * 利用redis-mock进行模拟redis
 *
 * @author wujr
 * 2020-01-04
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-04 1.0 新增]
 */
public class MockRedisService implements UnitRedisService {
    private RedisServer redisServer;

    @Override
    public void start() throws IOException {
        redisServer = RedisServer.newRedisServer(62379);
        redisServer.start();
    }

    @Override
    public void stop() {
        if (null != redisServer){
            redisServer.stop();
        }
    }

    @Override
    public String[] uri() {
        String host = redisServer.getHost();
        int port = redisServer.getBindPort();
        return new String[]{"redis://" + host + ":" + port + "/0"};
    }
}
