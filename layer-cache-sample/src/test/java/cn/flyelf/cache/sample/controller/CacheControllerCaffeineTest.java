package cn.flyelf.cache.sample.controller;

import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.sample.SampleApplication;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * CacheController的采用caffeine的测试用例
 *
 * @author wujr
 * 2020-01-10
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-10 1.0 新增]
 */
@SpringBootTest(classes = SampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class CacheControllerCaffeineTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testCaffeinePutAndGetSimple() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/simple/put/simple:object:1?id=1&name=object1&index=101")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.instanceOf(CacheTestModel.class)))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data.id").value(1));

        requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/simple/get/simple:object:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.instanceOf(CacheTestModel.class)))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data.id").value(1));
    }
    @Test
    public void testCaffeineGetSimpleNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/simple/get/simple:object:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }
    @Test
    public void testCaffeinePutAndGetList() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/list/put/list:object:1?id=1,2&name=object1,object2&index=101,102")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));

        requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/list/get/list:object:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));
    }
    @Test
    public void testCaffeineGetListNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/list/get/list:object:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(jsonPath("$.code").value(101));
    }

    @Test
    public void testCaffeinePutAndGetSet() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/set/put/set:object:1?id=1,2&name=object1,object2&index=101,102")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));

        requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/set/get/set:object:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));
    }
    @Test
    public void testCaffeineGetSetNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/set/get/set:object:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }

    @Test
    public void testCaffeinePutAndGetMap() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/map/put/map:object:1?id=1,2&name=object1,object2&index=101,102")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));

        requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/map/get/map:object:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));
    }
    @Test
    public void testCaffeineGetMapNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/caffeine/map/get/map:object:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }
}
