package cn.flyelf.cache.sample.model;

import java.io.IOException;

/**
 * 单元测试的redis模拟服务
 *
 * @author wujr
 * 2020-01-04
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-04 1.0 新增]
 */
public interface UnitRedisService {
    void start() throws IOException;
    void stop();
    String[] uri();
}
