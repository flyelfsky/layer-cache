package cn.flyelf.cache.sample.model;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.IOException;

/**
 * redis的模拟服务
 *
 * @author wujr
 * 2020-01-11
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-11 1.0 新增]
 */
@Component
public class RedisMockService {
    private UnitRedisService redisServer;

    @PostConstruct
    public void startRedis() throws IOException {
        redisServer = new EmbedRedisService();
        redisServer.start();
    }

    @PreDestroy
    public void stopRedis() {
        if (null != redisServer) {
            redisServer.stop();
        }
    }
}
