package cn.flyelf.cache.sample.controller;

import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.sample.SampleApplication;
import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * CacheController的同时缓存的测试用例
 *
 * @author wujr
 * 2020-01-12
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-12 1.0 新增]
 */
@SpringBootTest(classes = SampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class CacheControllerBothTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testBuildModelNotSameSize(){
        CacheController controller = new CacheController();
        CacheTestModel model = controller.buildModel(1, new long[]{1L, 2L}, new String[]{"name"}, new Integer[]{11});
        Assert.assertNotNull(model);
        Assert.assertTrue(model.getName().startsWith("匿名"));
        Assert.assertNull(model.getIndex());
    }

    @Test
    public void testRedisPutAndGetList() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/both/list/put/list:both:1?id=1,2&name=object1,object2&index=101,102")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));

        requestBuilder = MockMvcRequestBuilders.get("/cache/both/list/get/list:both:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));
    }
    @Test
    public void testRedisGetListNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/both/list/get/list:both:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }
}
