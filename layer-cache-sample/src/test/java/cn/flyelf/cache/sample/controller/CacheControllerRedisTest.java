package cn.flyelf.cache.sample.controller;

import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.sample.SampleApplication;
import org.hamcrest.CoreMatchers;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.asyncDispatch;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

/**
 * CacheController的redis缓存的测试用例
 *
 * @author wujr
 * 2020-01-11
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-11 1.0 新增]
 */
@SpringBootTest(classes = SampleApplication.class, webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringJUnit4ClassRunner.class)
@AutoConfigureMockMvc
public class CacheControllerRedisTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    public void testRedisPutAndGetSimple() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/simple/put/simple:object:1?id=1&name=object1&index=101")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.instanceOf(CacheTestModel.class)))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data.id").value(1));

        requestBuilder = MockMvcRequestBuilders.get("/cache/redis/simple/get/simple:object:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.instanceOf(CacheTestModel.class)))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data.id").value(1));

        requestBuilder = MockMvcRequestBuilders.get("/cache/redis/simple/block/simple:object:1")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.code").value(0))
                .andReturn();
    }
    @Test
    public void testRedisGetSimpleNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/simple/get/simple:object:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }
    @Test
    public void testRedisLoadSimple() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/simple/load/simple:object:3?id=3&name=object3&index=103")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.instanceOf(CacheTestModel.class)))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data.id").value(3));
    }
    @Test
    public void testRedisLoadSimpleNull() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/simple/load/simple:object:4")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }
    @Test
    public void testRedisBlockSimpleNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/simple/block/simple:object:4")
                .accept(MediaType.APPLICATION_JSON);
        mockMvc.perform(requestBuilder)
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.code").value(101))
                .andReturn();
    }
    @Test
    public void testRedisPutAndGetList() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/list/put/list:object:1?id=1,2&name=object1,object2&index=101,102")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));

        requestBuilder = MockMvcRequestBuilders.get("/cache/redis/list/get/list:object:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));
    }
    @Test
    public void testRedisGetListNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/list/get/list:object:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }

    @Test
    public void testRedisPutAndGetSet() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/set/put/set:object:1?id=1,2&name=object1,object2&index=101,102")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));

        requestBuilder = MockMvcRequestBuilders.get("/cache/redis/set/get/set:object:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));
    }
    @Test
    public void testRedisGetSetNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/set/get/set:object:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }

    @Test
    public void testRedisPutAndGetMap() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/map/put/map:object:1?id=1,2&name=object1,object2&index=101,102")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));

        requestBuilder = MockMvcRequestBuilders.get("/cache/redis/map/get/map:object:1")
                .accept(MediaType.APPLICATION_JSON);
        result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andExpect(request().asyncResult(CoreMatchers.everyItem(CoreMatchers.instanceOf(CacheTestModel.class))))
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.data[*].id").value(CoreMatchers.hasItems(1, 2)));
    }
    @Test
    public void testRedisGetMapNotExist() throws Exception{
        RequestBuilder requestBuilder = MockMvcRequestBuilders.get("/cache/redis/map/get/map:object:2")
                .accept(MediaType.APPLICATION_JSON);
        MvcResult result = mockMvc.perform(requestBuilder)
                .andExpect(request().asyncStarted())
                .andReturn();
        mockMvc.perform(asyncDispatch(result))
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$.code").value(101));
    }
}
