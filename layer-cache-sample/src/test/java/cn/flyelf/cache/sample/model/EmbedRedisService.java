package cn.flyelf.cache.sample.model;

import redis.embedded.RedisServer;

import java.io.IOException;

/**
 * 模拟redis的嵌入式服务
 *
 * @author wujr
 * 2020-01-04
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-04 1.0 新增]
 */
public class EmbedRedisService implements UnitRedisService {
    private RedisServer redisServer = null;
    private int port = 62379;

    @Override
    public void start() throws IOException {
        redisServer = new RedisServer(port);
        redisServer.start();
    }

    @Override
    public void stop(){
        if (null != redisServer){
            redisServer.stop();
        }
    }

    @Override
    public String[] uri(){
        return new String[]{"redis://localhost:" + port + "/0"};
    }
}
