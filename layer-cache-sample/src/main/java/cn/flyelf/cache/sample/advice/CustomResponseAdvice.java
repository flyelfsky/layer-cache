package cn.flyelf.cache.sample.advice;

import cn.flyelf.cache.sample.model.OutputDTO;
import cn.flyelf.cache.sample.model.OutputListDTO;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.MethodParameter;
import org.springframework.core.convert.support.GenericConversionService;
import org.springframework.http.MediaType;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import javax.annotation.Nonnull;
import java.util.List;

/**
 * rest响应结果定制封装
 *
 * @author wujr
 * @version 1.0
 * 2019-07-23
 */
@ControllerAdvice
@Slf4j
public class CustomResponseAdvice implements ResponseBodyAdvice<Object> {
    private static final DateConverter DATE_CONVERTER = new DateConverter();
    private static final String METHOD_CONTROLLER = "Controller";
    private static final String SPRING_PREFIX = "org.springframework.";
    private static final String SWAGGER_PREFIX = "springfox.documentation.";
    /**
     * 对于自定义的接口，可以设置哪些接口需要进行定制输出
     * 例如：cn.flyelf.*，cn.flyelf的所有接口进行定制输出
     */
    @Value("${spring.rest.advice.output.regex:#null}")
    private String[] customRegs;
    /**
     * 是否支持以Controller结尾的接口，默认为true
     */
    @Value("${spring.rest.advice.output.controller:true}")
    private boolean matchController;
    /**
     * 设置接口成功返回的code值，默认为0
     */
    @Value("${spring.rest.advice.output.success:0}")
    private int successCode;

    @InitBinder
    public void initBinder(WebDataBinder binder) {
        GenericConversionService genericConversionService = (GenericConversionService) binder.getConversionService();
        if (genericConversionService != null) {
            genericConversionService.addConverter(DATE_CONVERTER);
        }
    }

    @Override
    public boolean supports(MethodParameter returnType, @Nonnull Class<? extends HttpMessageConverter<?>> converterType) {
        String className = returnType.getExecutable().getDeclaringClass().getName();
        if (className.startsWith(SPRING_PREFIX) || className.startsWith(SWAGGER_PREFIX)){
            // 针对spring框架的返回结果不进行重写
            // 针对swagger的返回结果不进行重写
            return false;
        }
        if (matchController && className.endsWith(METHOD_CONTROLLER)){
            // 判断接口的控制器是否为以：Controller结尾的接口
            return true;
        }
        if (customRegs == null || customRegs.length == 0){
            return false;
        }
        for (String reg : customRegs){
            if (className.matches(reg)){
                return true;
            }
        }
        return false;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Object beforeBodyWrite(Object body, @Nonnull MethodParameter returnType,
                                  @Nonnull MediaType selectedContentType,
                                  @Nonnull Class<? extends HttpMessageConverter<?>> selectedConverterType,
                                  @Nonnull ServerHttpRequest request,
                                  @Nonnull ServerHttpResponse response) {
        Object result = body;
        if (!(body instanceof OutputDTO)){
            if (body instanceof List){
                result = OutputListDTO.of(successCode, (List)body);
            }else{
                result = OutputDTO.ok(successCode, body);
                Class clz = returnType.getParameterType();
                if (String.class.isAssignableFrom(clz)){
                    result = new JSONObject(result).toString();
                }
            }
        }
        log.info("result body: {}", result);
        return result;
    }
}
