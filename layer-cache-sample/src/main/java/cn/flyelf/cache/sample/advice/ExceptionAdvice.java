package cn.flyelf.cache.sample.advice;

import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.sample.model.ExceptionClass;
import cn.flyelf.cache.sample.model.OutputDTO;
import lombok.extern.slf4j.Slf4j;
import org.joor.Reflect;
import org.springframework.http.HttpStatus;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

/**
 * spring cloud的统一异常处理
 *
 * @author wujr
 * 2019/9/26
 * @version 1.0
 * 变更历史
 * [wujr 2019/9/26 1.0 新增]
 */
@RestControllerAdvice
@Slf4j
public class ExceptionAdvice {
    private final ExceptionConfiguration configuration;

    public ExceptionAdvice(ExceptionConfiguration configuration){
        this.configuration = configuration;
    }

    @ExceptionHandler(value = HttpRequestMethodNotSupportedException.class)
    @ResponseStatus(value = HttpStatus.NOT_FOUND)
    public void methodNotSupport(HttpRequestMethodNotSupportedException e){
        log.error("请求接口：{} 没有找到", e.getMethod());
    }
    @ExceptionHandler(value = BindException.class)
    public OutputDTO<Void> argumentMissingHandle(BindException e){
        StringBuilder sb = new StringBuilder();
        BindingResult bindingResult = e.getBindingResult();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        boolean first = true;
        for (FieldError fieldError : fieldErrors){
            if (first){
                first = false;
            }else{
                sb.append(", ");
            }
            sb.append(fieldError.getField())
                    .append(": ")
                    .append(fieldError.getDefaultMessage());
        }
        return OutputDTO.error(getCode(e), sb.toString());
    }
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public OutputDTO<Void> argumentTypeHandler(MethodArgumentNotValidException e){
        List<FieldError> errors = e.getBindingResult().getFieldErrors();
        String msg = "";
        if (!errors.isEmpty()){
            StringBuilder sb = new StringBuilder();
            String split = ", ";
            for (FieldError error : errors){
                sb.append(split).append(error.getDefaultMessage());
            }
            msg = sb.toString();
            if (msg.startsWith(split)){
                msg = msg.substring(split.length());
            }
        }
        return OutputDTO.error(getCode(e), msg);
    }
    @ExceptionHandler(value = CacheNotExistException.class)
    public OutputDTO<Void> noCacheHandler(CacheNotExistException e){
        return OutputDTO.error(101, "缓存key：[" + e.getMessage() + "]不存在");
    }

    @ExceptionHandler(value = Exception.class)
    public OutputDTO<Void> exceptionHandle(Exception e){
        ExceptionClass clz = findException(e);
        if (null != clz){
            return OutputDTO.error(getCode(clz), clz.getMessage());
        }

        try {
            Reflect reflect = Reflect.on(e).field(configuration.getCodeName());
            if (null != reflect && reflect.get() instanceof Integer) {
                return OutputDTO.error(reflect.get(), e.getMessage());
            }
        }catch (Exception ex){
            log.debug("异常中没有找到错误码：{}", configuration.getCodeName());
        }
        log.error("未知异常堆栈：", e);
        return OutputDTO.error(configuration.getUnknownError(), null);
    }
    private ExceptionClass findException(Exception e){
        if (null == configuration){
            return null;
        }
        ExceptionClass[] classes = configuration.getClasses();
        if (null == classes || classes.length == 0){
            return null;
        }
        for (ExceptionClass clz : classes){
            if (clz.isClass(e, configuration.getMaxDepth())){
                return clz;
            }
        }
        return null;
    }
    private int getCode(ExceptionClass clz){
        if (null == clz){
            return configuration.getUnknownError();
        }
        return clz.getCode();
    }
    private int getCode(Exception e){
        return getCode(findException(e));
    }
}
