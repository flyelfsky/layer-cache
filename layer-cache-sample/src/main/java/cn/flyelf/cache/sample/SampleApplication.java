package cn.flyelf.cache.sample;

import cn.flyelf.cache.spring.conf.EnableCacheBeanAnnotation;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * 例子入口
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
@SpringBootApplication
@EnableCacheBeanAnnotation
public class SampleApplication {
    public static void main(String[] args) {
        SpringApplication application = new SpringApplication(SampleApplication.class);
        application.run(args);
    }
}
