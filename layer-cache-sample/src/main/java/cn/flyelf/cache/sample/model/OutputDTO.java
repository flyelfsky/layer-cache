package cn.flyelf.cache.sample.model;

import lombok.Data;

/**
 * 统一的结果输出模型
 *
 * @author wujr
 * 2019/9/26
 * @version 1.0
 * 变更历史
 * [wujr 2019/9/26 1.0 新增]
 */
@Data
public class OutputDTO<T> {
    /**
     * 结果响应码
     * 0表示成功
     */
    private Integer code;
    /**
     * 具体的结果消息
     */
    private String message;
    /**
     * 响应的具体内容
     */
    private T data;

    OutputDTO(T data){
        this.data = data;
        this.code = 0;
        this.message = "SUCCESS";
    }

    /**
     * 自定义错误类型
     * @param code 为非0值
     * @param msg 为错误信息
     */
    private OutputDTO(int code, String msg) {
        this.code = code;
        this.message = msg;
    }

    public static <T> OutputDTO<T> ok(T data){
        return new OutputDTO<>(data);
    }
    public static <T> OutputDTO<T> ok(int success, T data){
        OutputDTO<T> result = new OutputDTO<>(data);
        result.setCode(success);
        return result;
    }
    public static OutputDTO<Void> error(int code, String msg){
        return new OutputDTO<>(code, msg);
    }
}
