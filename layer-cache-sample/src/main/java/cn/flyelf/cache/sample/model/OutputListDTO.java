package cn.flyelf.cache.sample.model;


import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.List;

/**
 * 统一的列表结果输出模型
 *
 * @author wujr
 * 2019/9/26
 * @version 1.0
 * 变更历史
 * [wujr 2019/9/26 1.0 新增]
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class OutputListDTO<T> extends OutputDTO<List<T>> {
    /**
     * 总记录数量
     */
    private Long total;

    private OutputListDTO(List<T> data){
        super(data);
        if(null == data) {
            this.total = 0L;
        } else {
            this.total = (long)data.size();
        }
    }
    private OutputListDTO(List<T> data, long total){
        super(data);
        this.total = total;
    }

    public static <T> OutputListDTO<T> of(List<T> data){
        return new OutputListDTO<>(data);
    }
    public static <T> OutputListDTO<T> of(int success, List<T> data){
        OutputListDTO<T> result = new OutputListDTO<>(data);
        result.setCode(success);
        return result;
    }

    public static <T> OutputListDTO<T> ok(List<T> data, long total){
        return new OutputListDTO<>(data, total);
    }
}
