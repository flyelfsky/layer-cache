package cn.flyelf.cache.sample.advice;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.convert.converter.Converter;

import javax.annotation.Nonnull;
import java.util.Date;

/**
 * 把格式化的时间字符串转换为日期对象
 *
 * @author wujr
 * @version 1.0
 * 2019-07-17
 */
public class DateConverter implements Converter<String, Date> {
    @Override
    public Date convert(@Nonnull String source) {
        if (StringUtils.isBlank(source)) {
            return null;
        }
        Date date = TimeConverter.time(source);
        if (null == date){
            throw new IllegalArgumentException("无效的时间格式： '" + source + "'");
        }

        return date;
    }
}
