package cn.flyelf.cache.sample.advice;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.BiFunction;
import java.util.regex.Pattern;

/**
 * 日期时间的格式转换器
 *
 * @author wujr
 * @version 1.0
 * 2019-07-15
 */
public class TimeConverter {
    /**
     * 日期的格式化转换器: "yyyy 分隔符 MM 分隔符 dd"
     * key: 分隔符，例如：- / 空白（非空格）
     */
    private static final ConcurrentHashMap<String, DateTimeFormatter> DAY_FORMATTER = new ConcurrentHashMap<>(3);
    /**
     * 日期到月份的格式化转换器: "yyyy 分隔符 MM"
     * key：分隔符，例如：- / 空白（非空格）
     */
    private static final ConcurrentHashMap<String, DateTimeFormatter> MONTH_FORMATTER = new ConcurrentHashMap<>(3);
    /**
     * 日期的格式化转换器: "yyyy 分隔符 MM 分隔符 dd HH 分隔符 mm 分隔符 ss"
     * key: 分隔符，例如：- / 空白（非空格）
     */
    private static final ConcurrentHashMap<String, DateTimeFormatter> SECOND_FORMATTER = new ConcurrentHashMap<>(3);
    /**
     * 日期的格式化转换器: "yyyy 分隔符 MM 分隔符 dd HH 分隔符 mm"
     * key: 分隔符，例如：- / 空白（非空格）
     */
    private static final ConcurrentHashMap<String, DateTimeFormatter> MINUTE_FORMATTER = new ConcurrentHashMap<>(3);
    /**
     * 日期的格式化转换器: "yyyy 分隔符 MM 分隔符 dd HH"
     * key: 分隔符，例如：- / 空白（非空格）
     */
    private static final ConcurrentHashMap<String, DateTimeFormatter> HOUR_FORMATTER = new ConcurrentHashMap<>(3);

    private static final ConcurrentHashMap<String, DateTimeFormatter> TIME_FORMATTER = new ConcurrentHashMap<>(1);
    /**
     * 时间格式正则
     */
    private static final Pattern[] PATTERN_FORMAT = {
            Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}:\\d{1,2}$"),
            Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}:\\d{1,2}$"),
            Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2} {1}\\d{1,2}$"),
            Pattern.compile("^\\d{4}-\\d{1,2}-\\d{1,2}$"),
            Pattern.compile("^\\d{4}-\\d{1,2}$")
    };
    private static final List<BiFunction<String, String, Date>> FORMATTERS = Arrays.asList(
            TimeConverter::second, TimeConverter::minute, TimeConverter::hour, TimeConverter::day, TimeConverter::month
    );

    private static String format(DateTimeFormatter formatter, Date date){
        return formatter.format(LocalDateTime.ofInstant(date.toInstant(), ZoneId.systemDefault()));
    }

    /**
     * 把日期对象转换为格式化的日期，分隔符可以自定义
     * @param date 时间
     * @param separator 分隔符
     * @return 格式化的日期 yyyy + separator + MM + separator + dd
     */
    public static String day(Date date, String separator){
        DateTimeFormatter formatter = DAY_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM" + separator + "dd"));
        return format(formatter, date);
    }

    /**
     * 把格式化的日期字符串转换为时间
     * @param date 格式化的日期字符串 yyyy + separator + MM + separator + dd
     * @param separator 分隔符
     * @return 时间对象
     */
    public static Date day(String date, String separator){
        DateTimeFormatter formatter = DAY_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM" + separator + "dd"));
        LocalDate localDate = LocalDate.parse(date, formatter);
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 把日期对象转换为格式化的日期，分隔符可以自定义
     * @param date 时间
     * @param separator 分隔符
     * @return 格式化的日期 yyyy + separator + MM + separator + dd
     */
    public static String month(Date date, String separator){
        DateTimeFormatter formatter = MONTH_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM"));
        return format(formatter, date);
    }
    /**
     * 把格式化的日期字符串转换为时间
     * @param date 格式化的日期字符串 yyyy + separator + MM
     * @param separator 分隔符
     * @return 时间对象
     */
    public static Date month(String date, String separator){
        String[] values = date.split(separator);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, Integer.parseInt(values[0]));
        calendar.set(Calendar.MONTH, Integer.parseInt(values[1]) - 1);
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        return calendar.getTime();
    }

    /**
     * 把日期对象转换为格式化的日期，分隔符可以自定义
     * @param date 时间
     * @param separator 分隔符
     * @return 格式化的日期 yyyy + separator + MM + separator + dd + 空格 + HH:mm:ss
     */
    public static String second(Date date, String separator){
        DateTimeFormatter formatter = SECOND_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM" + separator + "dd HH:mm:ss"));
        return format(formatter, date);
    }
    /**
     * 把格式化的日期字符串转换为时间
     * @param date 格式化的日期字符串 yyyy + separator + MM + separator + dd + 空格 + HH:mm:ss
     * @param separator 分隔符
     * @return 时间对象
     */
    public static Date second(String date, String separator){
        DateTimeFormatter formatter = SECOND_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM" + separator + "dd HH:mm:ss"));
        LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    /**
     * 把日期对象转换为格式化的日期，分隔符可以自定义
     * @param date 时间
     * @param separator 分隔符
     * @return 格式化的日期 yyyy + separator + MM + separator + dd + 空格 + HH:mm
     */
    public static String minute(Date date, String separator){
        DateTimeFormatter formatter = MINUTE_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM" + separator + "dd HH:mm"));
        return format(formatter, date);
    }
    /**
     * 把格式化的日期字符串转换为时间
     * @param date 格式化的日期字符串 yyyy + separator + MM + separator + dd + 空格 + HH:mm
     * @param separator 分隔符
     * @return 时间对象
     */
    public static Date minute(String date, String separator){
        DateTimeFormatter formatter = MINUTE_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM" + separator + "dd HH:mm"));
        LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }
    /**
     * 把日期对象转换为格式化的日期，分隔符可以自定义
     * @param date 时间
     * @param separator 分隔符
     * @return 格式化的日期 yyyy + separator + MM + separator + dd + 空格 + HH
     */
    public static String hour(Date date, String separator){
        DateTimeFormatter formatter = HOUR_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM" + separator + "dd HH"));
        return format(formatter, date);
    }
    /**
     * 把格式化的日期字符串转换为时间
     * @param date 格式化的日期字符串 yyyy + separator + MM + separator + dd + 空格 + HH
     * @param separator 分隔符
     * @return 时间对象
     */
    public static Date hour(String date, String separator){
        DateTimeFormatter formatter = HOUR_FORMATTER.computeIfAbsent(separator, k -> DateTimeFormatter.ofPattern("yyyy" + separator + "MM" + separator + "dd HH"));
        LocalDateTime localDateTime = LocalDateTime.parse(date, formatter);
        return Date.from(localDateTime.atZone(ZoneId.systemDefault()).toInstant());
    }

    /**
     * 根据具体的转换格式转换时间
     * @param date 需要转换的时间
     * @param format 格式化字符串
     * @return 格式化的时间
     */
    public static String time(Date date, String format){
        DateTimeFormatter formatter = TIME_FORMATTER.computeIfAbsent(format, DateTimeFormatter::ofPattern);
        return format(formatter, date);
    }

    /**
     * 把格式化的时间字符串转换为时间对象
     * @param time 格式化的时间字符串，仅支持："-" 作为分隔符
     * @return 时间对象
     */
    public static Date time(String time){
        int index = 0;
        for (; index < PATTERN_FORMAT.length; ++ index){
            Pattern pattern = PATTERN_FORMAT[index];
            if (pattern.matcher(time).matches()){
                BiFunction<String, String, Date> function = FORMATTERS.get(index);
                return function.apply(time, "-");
            }
        }
        return null;
    }

    /**
     * 取明天的时间
     * @return 明天的时间
     */
    public static Calendar nextDay(){
        Calendar calendar = Calendar.getInstance();
        nextDay(calendar);
        return calendar;
    }
    public static void nextDay(Calendar calendar){
        calendar.add(Calendar.DAY_OF_MONTH, 1);
        clearDay(calendar);
    }

    /**
     * 清除该天的具体时间：小时、分、秒、毫秒
     * @param calendar 需要清除的日历
     */
    public static void clearDay(Calendar calendar){
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    /**
     * 清理某个日期的具体时间
     * @param date 时间对象
     * @return 清理后的时间
     */
    public static Date clearDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        clearDay(calendar);
        return calendar.getTime();
    }

    /**
     * 取某个日期的最后1秒
     * @param date 时间对象
     * @return 该日期的最大值
     */
    public static Date lastDate(Date date){
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
    }
}
