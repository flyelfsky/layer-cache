package cn.flyelf.cache.sample.advice;

import cn.flyelf.cache.sample.model.ExceptionClass;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

/**
 * 异常统一处理配置
 * spring:
 *   rest:
 *     advice:
 *       exception:
 *         code-name: code
 *         classes:
 *         - name: org.springframework.web.HttpRequestMethodNotSupportedException
 *           code: 404
 *           message: 接口不存在
 *         - name: org.springframework.web.bind.MethodArgumentNotValidException
 *           code: -2
 *         - name: org.springframework.web.bind.MissingRequestHeaderException
 *           code: -2
 *           message: 缺少头部参数
 *         - name: org.springframework.web.method.annotation.MethodArgumentTypeMismatchException
 *           code: -2
 *           message: 参数数据类型不匹配
 *         - name: org.springframework.http.converter.HttpMessageNotReadableException
 *           code: -2
 *           message: 不能识别的参数
 *         - name: org.springframework.validation.BindException
 *           code: -2
 * @author wujr
 * 2019/11/19
 * @version 1.0
 * 变更历史
 * [wujr 2019/11/19 1.0 新增]
 */
@Component
@ConfigurationProperties(prefix = "spring.rest.advice.exception")
@Data
public class ExceptionConfiguration {
    /**
     * 支持的需要统一转化的异常类
     */
    private ExceptionClass[] classes;
    /**
     * 未知的异常的响应码
     */
    private int unknownError = -1;
    /**
     * 递归throwable最大层数
     */
    private int maxDepth = 50;
    /**
     * 未知异常类中的错误码的属性名称
     */
    private String codeName = "code";
}
