package cn.flyelf.cache.sample.model;

import lombok.Data;

/**
 * 定义需要进行转化的异常类参数模型
 *
 * @author wujr
 * 2019/11/19
 * @version 1.0
 * 变更历史
 * [wujr 2019/11/19 1.0 新增]
 */
@Data
public class ExceptionClass {
    /**
     * 异常类的全路径
     */
    private String name;
    /**
     * 异常类的类模型，通过Class.fromName得到
     */
    private Class clz;
    /**
     * 该异常类的错误码
     */
    private int code;
    /**
     * 该异常类的错误描述
     */
    private String message;

    public boolean isClass(Throwable throwable, int depth){
        if (null == clz){
            try{
                clz = Class.forName(name);
            }catch (ClassNotFoundException e){
                return name.equals(throwable.getClass().getName());
            }
        }
        int counter = 0;
        Throwable cause = throwable;
        while (null != cause && counter++ < depth){
            if (clz.isInstance(cause)){
                return true;
            }
            cause = cause.getCause();
        }
        return false;
    }
}
