package cn.flyelf.cache.sample.controller;

import cn.flyelf.cache.annotation.CacheBean;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.Cache;
import cn.flyelf.cache.core.HashCache;
import cn.flyelf.cache.core.ListCache;
import cn.flyelf.cache.core.SetCache;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.redisson.api.RAtomicLong;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.*;

/**
 * 缓存接口例子
 *
 * @author wujr
 * 2019-12-24
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-24 1.0 新增]
 */
@RestController
@RequestMapping("/cache")
@Slf4j
@RequiredArgsConstructor
public class CacheController {
    @CacheBean(layer = {CacheConstant.LAYER_CAFFEINE})
    private Cache<String, CacheTestModel> cacheCaffeine;
    @CacheBean
    private Cache<String, CacheTestModel> cacheRedis;

    @CacheBean(layer = {CacheConstant.LAYER_CAFFEINE})
    private ListCache<String, CacheTestModel> cacheListCaffeine;
    @CacheBean
    private ListCache<String, CacheTestModel> cacheListRedis;

    @CacheBean(layer = {CacheConstant.LAYER_CAFFEINE})
    private SetCache<String, CacheTestModel> cacheSetCaffeine;
    @CacheBean
    private SetCache<String, CacheTestModel> cacheSetRedis;

    @CacheBean(layer = {CacheConstant.LAYER_CAFFEINE})
    private HashCache<String, Long, CacheTestModel> cacheMapCaffeine;
    @CacheBean
    private HashCache<String, Long, CacheTestModel> cacheMapRedis;

    @CacheBean(layer = {CacheConstant.LAYER_CAFFEINE,CacheConstant.LAYER_REDIS})
    private Cache<String, List<CacheTestModel>> cacheListBoth;

    @Autowired
    private RedissonClient redisson;

    @GetMapping("/caffeine/simple/put/{key}")
    public Mono<CacheTestModel> putSimpleCaffeine(@PathVariable("key") String key,
                                                  @RequestParam("id") long id,
                                                  @RequestParam("name") String name,
                                                  @RequestParam("index") Integer index){
        return putModel(key, cacheCaffeine, id, name, index);
    }
    @GetMapping("/caffeine/simple/get/{key}")
    public Mono<CacheTestModel> getSimpleCaffeine(@PathVariable("key") String key){
        return cacheCaffeine.get(key).map(CacheResult::getValue);
    }

    @GetMapping("/counter/{key}")
    public long getNextCounter(@PathVariable("key") String key){
        String redisKey = "support:counter:sample:" + key;
        RAtomicLong atomic = redisson.getAtomicLong(redisKey);
        return atomic.getAndIncrement();
    }

    private Mono<CacheTestModel> putModel(String key, Cache<String, CacheTestModel> cache,
                                          long id,
                                          String name,
                                          Integer index){
        CacheTestModel model = new CacheTestModel();
        model.setId(id);
        model.setName(name);
        model.setIndex(index);
        model.setDate(new Date());
        Mono<CacheResult<Boolean>> result = cache.put(key, model);
        return result.map(r -> model);
    }
    @GetMapping("/redis/simple/put/{key}")
    public Mono<CacheTestModel> putRedis(@PathVariable("key") String key,
                                   @RequestParam("id") long id,
                                   @RequestParam("name") String name,
                                   @RequestParam("index") Integer index){
        return putModel(key, cacheRedis, id, name, index);
    }
    @GetMapping("/redis/simple/get/{key}")
    public Mono<CacheTestModel> getRedis(@PathVariable("key") String key){
        return cacheRedis.get(key).map(CacheResult::getValue);
    }
    @GetMapping("/redis/simple/block/{key}")
    public CacheTestModel blockRedis(@PathVariable("key") String key){
        Mono<CacheResult<CacheTestModel>> result = cacheRedis.get(key);
        CacheResult<CacheTestModel> r = result.block();
        return r.getValue();
    }

    @GetMapping("/redis/simple/load/{key}")
    public Mono<CacheTestModel> getRedisNullAndLoad(@PathVariable("key") String key,
                                                    @RequestParam(value = "id", required = false) Long id,
                                                    @RequestParam(value = "name", required = false) String name,
                                                    @RequestParam(value = "index", required = false) Integer index){
        CacheTestModel model = null;
        if (null != id){
            model = new CacheTestModel();
            model.setId(id);
            model.setName(name);
            model.setIndex(index);
            model.setDate(new Date());
        }
        CacheTestModel finalModel = model;
        Mono<CacheResult<CacheTestModel>> result = cacheRedis.get(key, k -> finalModel);
        return result.map(CacheResult::getValue);
    }

    @GetMapping("/caffeine/list/put/{key}")
    public Flux<CacheTestModel> putCaffeineList(@PathVariable("key") String key,
                                                @RequestParam("id") long[] id,
                                                @RequestParam("name") String[] name,
                                                @RequestParam("index") Integer[] index){
        return putList(cacheListCaffeine, key, id, name, index);
    }
    private Flux<CacheTestModel> putList(ListCache<String, CacheTestModel> cache,
                                         String key, long[] id, String[] name, Integer[] index){
        List<CacheTestModel> models = new ArrayList<>();
        return putCollection(cache, models, key, id, name, index);
    }
    @GetMapping("/caffeine/list/get/{key}")
    public Flux<CacheTestModel> getCaffeineList(@PathVariable("key") String key){
        Mono<CacheResult<List<CacheTestModel>>> result = cacheListCaffeine.get(key);
        return result.flatMapIterable(CacheResult::getValue);
    }

    @GetMapping("/redis/list/put/{key}")
    public Flux<CacheTestModel> putRedisList(@PathVariable("key") String key,
                                                @RequestParam("id") long[] id,
                                                @RequestParam("name") String[] name,
                                                @RequestParam("index") Integer[] index){
        return putList(cacheListRedis, key, id, name, index);
    }
    @GetMapping("/redis/list/get/{key}")
    public Flux<CacheTestModel> getRedisList(@PathVariable("key") String key){
        Mono<CacheResult<List<CacheTestModel>>> result = cacheListRedis.get(key);
        return result.flatMapIterable(CacheResult::getValue);
    }

    @SuppressWarnings("unchecked")
    private Flux<CacheTestModel> putCollection(Cache cache,
                                               Collection<CacheTestModel> records,
                                               String key, long[] id, String[] name, Integer[] index){
        for (int i = 0; i < id.length; ++ i){
            CacheTestModel model = buildModel(i, id, name, index);

            records.add(model);
        }
        Mono<CacheResult<Boolean>> result = cache.put(key, records);
        return result.flatMapIterable(r -> records);
    }
    @GetMapping("/caffeine/set/put/{key}")
    public Flux<CacheTestModel> putCaffeineSet(@PathVariable("key") String key,
                                                @RequestParam("id") long[] id,
                                                @RequestParam("name") String[] name,
                                                @RequestParam("index") Integer[] index){
        return putSet(cacheSetCaffeine, key, id, name, index);
    }
    private Flux<CacheTestModel> putSet(SetCache<String, CacheTestModel> cache,
                                         String key, long[] id, String[] name, Integer[] index){
        Set<CacheTestModel> models = new HashSet<>();
        return putCollection(cache, models, key, id, name, index);
    }
    @GetMapping("/caffeine/set/get/{key}")
    public Flux<CacheTestModel> getCaffeineSet(@PathVariable("key") String key){
        log.info("缓存接口：caffeine/set/get, key = {}", key);
        Mono<CacheResult<Set<CacheTestModel>>> result = cacheSetCaffeine.get(key);
        return result.flatMapIterable(CacheResult::getValue);
    }

    @GetMapping("/redis/set/put/{key}")
    public Flux<CacheTestModel> putRedisSet(@PathVariable("key") String key,
                                             @RequestParam("id") long[] id,
                                             @RequestParam("name") String[] name,
                                             @RequestParam("index") Integer[] index){
        return putSet(cacheSetRedis, key, id, name, index);
    }
    @GetMapping("/redis/set/get/{key}")
    public Flux<CacheTestModel> getRedisSet(@PathVariable("key") String key){
        Mono<CacheResult<Set<CacheTestModel>>> result = cacheSetRedis.get(key);
        return result.flatMapIterable(CacheResult::getValue);
    }

    CacheTestModel buildModel(int i, long[] id, String[] name, Integer[] index){
        CacheTestModel model = new CacheTestModel();
        model.setId(id[i]);
        if (i < name.length){
            model.setName(name[i]);
        }else{
            model.setName("匿名" + i);
        }
        if (i < index.length){
            model.setIndex(index[i]);
        }
        model.setDate(new Date());
        return model;
    }

    private Flux<CacheTestModel> putMap(HashCache<String, Long, CacheTestModel> cache,
                                        String key, long[] id, String[] name, Integer[] index){
        Map<Long, CacheTestModel> models = new HashMap<>(id.length);
        for (int i = 0; i < id.length; ++ i){
            CacheTestModel model = buildModel(i, id, name, index);

            models.put(model.getId(), model);
        }
        return cache.put(key, models).flatMapIterable(r -> models.values());
    }
    @GetMapping("/caffeine/map/put/{key}")
    public Flux<CacheTestModel> putCaffeineHash(@PathVariable("key") String key,
                                               @RequestParam("id") long[] id,
                                               @RequestParam("name") String[] name,
                                               @RequestParam("index") Integer[] index){
        return putMap(cacheMapCaffeine, key, id, name, index);
    }
    @GetMapping("/caffeine/map/get/{key}")
    public Flux<CacheTestModel> getCaffeineHash(@PathVariable("key") String key){
        return cacheMapCaffeine.get(key).flatMapIterable(r -> r.getValue().values());
    }
    @GetMapping("/redis/map/put/{key}")
    public Flux<CacheTestModel> putRedisHash(@PathVariable("key") String key,
                                                @RequestParam("id") long[] id,
                                                @RequestParam("name") String[] name,
                                                @RequestParam("index") Integer[] index){
        return putMap(cacheMapRedis, key, id, name, index);
    }
    @GetMapping("/redis/map/get/{key}")
    public Flux<CacheTestModel> getRedisHash(@PathVariable("key") String key){
        return cacheMapRedis.get(key).flatMapIterable(r -> r.getValue().values());
    }
    @GetMapping("/redis/map/key/{key}/{id}")
    public List<CacheTestModel> getRedisHashList(@PathVariable("key") String key,
                                                 @PathVariable("id") long id){
        Mono<CacheResult<List<CacheTestModel>>> mono = cacheMapRedis.get(key, id);
        return mono.map(CacheResult::getValue).block();
    }

    @GetMapping("/both/list/put/{key}")
    public Flux<CacheTestModel> putBothList(@PathVariable("key") String key,
                                             @RequestParam("id") long[] id,
                                             @RequestParam("name") String[] name,
                                             @RequestParam("index") Integer[] index){
        List<CacheTestModel> models = new ArrayList<>(id.length);
        return putCollection(cacheListBoth, models, key, id, name, index);
    }
    @GetMapping("/both/list/get/{key}")
    public Flux<CacheTestModel> getBothList(@PathVariable("key") String key){
        Mono<CacheResult<List<CacheTestModel>>> result = cacheListBoth.get(key);
        return result.flatMapIterable(CacheResult::getValue);
    }
}
