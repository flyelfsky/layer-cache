package cn.flyelf.cache.annotation;

import lombok.Data;

/**
 * 缓存的常量定义
 *
 * @author wujr
 * 2019/12/12
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/12 1.0 新增]
 */
@Data
public class CacheConstant {
    public static final String LAYER_REDIS = "redis";
    public static final String LAYER_CAFFEINE = "caffeine";
    public static final int UNDEFINED_INT = Integer.MIN_VALUE;
    public static final long UNDEFINED_LONG = Long.MIN_VALUE;
    public static final String UNDEFINED_STRING = "$$undefined$$";

    public static final long POS_START = 0L;
    public static final long POS_STOP = -1L;

    public static boolean isUndefined(String s){
        return UNDEFINED_STRING.equals(s);
    }
    public static boolean isUndefined(int value){
        return UNDEFINED_INT == value;
    }
    public static boolean isUndefined(long value){
        return UNDEFINED_LONG == value;
    }
    public static class CacheUndefined{}

    public static boolean isUndefined(Class clz){
        return CacheUndefined.class.equals(clz);
    }

    public enum RESULT{
        /**
         * 未知结果
         */
        UNKNOWN,
        /**
         * 成功
         */
        OK,
        /**
         * 不存在key
         */
        NO_KEY
        ;
        public boolean success(){
            return this.equals(OK);
        }
    }
    /**
     * 默认缓存区域
     */
    public static final String AREA_DEFAULT = "default";

    /**
     * 缓存类型：简单对象
     */
    public static final String CACHE_SIMPLE = "simple";
    /**
     * 缓存类型：列表
     */
    public static final String CACHE_LIST = "list";
    /**
     * 缓存类型：集合
     */
    public static final String CACHE_SET = "set";
    /**
     * 缓存类型：映射
     */
    public static final String CACHE_MAP = "hash";

    // 定义序列化支持的类型
    /**
     * 序列化策略：json
     */
    public static final String SERIALIZER_JSON = "json";
    /**
     * 序列化策略：java
     */
    public static final String SERIALIZER_JAVA = "java";
    /**
     * 序列化策略：kryo
     */
    public static final String SERIALIZER_KRYO = "kryo";

    /**
     * list的右边
     */
    public static final String LIST_RIGHT = "right";

    /**
     * list的左边
     */
    public static final String LIST_LEFT = "left";

    /**
     * list的get参数：开始索引
     */
    public static final String ATTACH_LIST_START = "start";
    /**
     * list的get参数：结束索引
     */
    public static final String ATTACH_LIST_STOP = "stop";
    /**
     * list的执行方向
     */
    public static final String ATTACH_LIST_DIRECTION = "direction";
    /**
     * hash的map的key
     */
    public static final String ATTACH_HASH_HK = "hk";
    /**
     * 缓存的key
     */
    public static final String ATTACH_KEY = "key";
}
