package cn.flyelf.cache.annotation;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 定义一个缓存bean对象
 *
 * @author wujr
 * 2019-12-23
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-23 1.0 新增]
 */
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface CacheBean {
    /**
     * 该缓存支持的缓存层，默认为redis
     * @return 缓存层数组
     */
    String[] layer() default {CacheConstant.LAYER_REDIS};
    /**
     * 存储的数据类型，默认为简单对象
     * @return 数据类型
     */
    String type() default CacheConstant.UNDEFINED_STRING;

    /**
     * 有效时长的时间单位
     * @return the time unit of expire
     */
    TimeUnit timeUnit() default TimeUnit.SECONDS;

    /**
     * 默认的有效时长
     * @return 默认的有效时长
     */
    long expire() default CacheConstant.UNDEFINED_LONG;

    /**
     * 缓存的特征描述
     * @return 后缀
     */
    String feature() default CacheConstant.UNDEFINED_STRING;

    /**
     * 缓存的类
     * @return 类
     */
    Class<?> object() default CacheConstant.CacheUndefined.class;

    /**
     * 缓存区域
     * @return 区域
     */
    String area() default CacheConstant.AREA_DEFAULT;

    /**
     * 缓存数量的限制
     * @return 默认不限制
     */
    long limit() default CacheConstant.UNDEFINED_LONG;

    /**
     * 是否缓存null对象
     * @return true表示缓存null对象
     */
    boolean nullable() default false;

    /**
     * 缓存null的有效时长，
     * @return 毫秒，默认为60秒
     */
    long nullExpire() default Long.MAX_VALUE;
}
