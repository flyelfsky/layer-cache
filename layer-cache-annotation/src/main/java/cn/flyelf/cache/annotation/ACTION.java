package cn.flyelf.cache.annotation;

/**
 * 定义支持的缓存动作
 *
 * @author wujr
 * 2020-01-10
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-10 1.0 新增]
 */
public enum ACTION {
    /**
     * 无效动作
     */
    NONE,
    /**
     * 获取内容
     */
    GET,
    /**
     * 设置内容
     */
    PUT,
    /**
     * 删除内容
     */
    DELETE,
    /**
     * 获取list类型内容
     */
    GETLIST,
    /**
     * 获取list类型内容
     */
    GETSET,
    /**
     * 获取hash类型内容
     */
    GETMAP,
    /**
     * 设置list类型内容
     */
    PUTLIST,
    /**
     * 设置set类型内容
     */
    PUTSET,
    /**
     * 设置hash类型内容
     */
    PUTMAP,
    /**
     * 弹出一个内容
     */
    POP,
    /**
     * 压入一个内容
     */
    PUSH,
    /**
     * 根据hash的key获取hash类型中的对应内容
     */
    GETMAPKEY,
    /**
     * 判定hash中是否存在对应的key内容
     */
    HASMAPKEY,
    /**
     * 删除hash类型中的对应key内容
     */
    DELMAPKEY,
    /**
     * 添加一个map对象
     */
    ADDMAPKEY
    ;

    public boolean allowLoad(){
        return this.equals(GET) || this.equals(GETLIST) || this.equals(GETSET) || this.equals(GETMAP);
    }
    public boolean needSynchronize(){
        return this.equals(GET) || this.equals(GETLIST) || this.equals(GETSET) || this.equals(GETMAP);
    }
    public boolean ignoreResult(){
        return this.equals(DELETE) || this.equals(DELMAPKEY) ||
                this.equals(PUT) || this.equals(PUTLIST) || this.equals(PUTSET) || this.equals(PUTMAP);
    }
    public ACTION owner(){
        if (this.equals(GET) || this.equals(GETLIST) || this.equals(GETSET) || this.equals(GETMAP) ||
            this.equals(GETMAPKEY) || this.equals(POP) || this.equals(HASMAPKEY)){
            return GET;
        }else if (this.equals(PUT) || this.equals(PUTLIST) || this.equals(PUTMAP) || this.equals(PUSH)){
            return PUT;
        }else if (this.equals(DELETE) || this.equals(DELMAPKEY)){
            return DELETE;
        }else{
            return GET;
        }
    }
}
