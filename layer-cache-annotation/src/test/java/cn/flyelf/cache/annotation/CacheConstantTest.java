package cn.flyelf.cache.annotation;

import org.junit.Assert;
import org.junit.Test;

/**
 * CacheConstant的测试用例
 *
 * @author wujr
* 2020-01-10
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-10 1.0 新增]
 */
public class CacheConstantTest {
    @Test
    public void testIsUndefinedInt(){
        boolean b = CacheConstant.isUndefined(Integer.MIN_VALUE);
        Assert.assertTrue(b);
    }
    @Test
    public void testIsUndefinedIntFalse(){
        boolean b = CacheConstant.isUndefined(1);
        Assert.assertFalse(b);
    }
}
