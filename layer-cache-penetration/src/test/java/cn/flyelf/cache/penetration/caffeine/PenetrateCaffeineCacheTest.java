package cn.flyelf.cache.penetration.caffeine;

import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.penetration.model.PenetrateLock;
import cn.flyelf.cache.penetration.model.PenetrateLockWrapper;
import org.joor.Reflect;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

/**
 * caffeine的穿透缓存测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class PenetrateCaffeineCacheTest {
    private static CachePenetrationConfig config;
    private static PenetrateCaffeineCache cache;
    @BeforeClass
    public static void setUp(){
        config = new CachePenetrationConfig();
        config.setExpire(60 * 1000L);
        config.setPenetrateTimeout(100L);

        cache = new PenetrateCaffeineCache(config);
    }
    @Test
    public void testTimeout(){
        Assert.assertEquals(100L, cache.timeout().longValue());
    }
    @Test
    public void testExpire(){
        Assert.assertEquals(config.getExpire(), cache.expire());
    }
    @Test
    public void testEnabled(){
        Assert.assertTrue(cache.enabled());
    }

    @Test
    public void testPutNull(){
        cache.put("101", null, null);
        Object object = cache.get("101");
        Assert.assertNotNull(object);
        Assert.assertTrue(cache.isNull(object));
    }
    @Test
    public void testPut(){
        cache.put("102", 111, 30 * 1000L);
        Object object = cache.get("102");
        Assert.assertNotNull(object);
        Assert.assertFalse(cache.isNull(object));
        Assert.assertTrue(object instanceof Integer);
        Assert.assertEquals(111, object);
    }

    @Test
    public void testGetNoCache(){
        Object object = cache.get("noKey");
        Assert.assertNull(object);
    }
    @Test
    public void testGetNoFuture(){
        cache.put("103", null, null);
        Object object = cache.get("104");
        Assert.assertNull(object);
    }

    @Test
    public void testGetLocker(){
        PenetrateLockWrapper wrapper = new PenetrateLockWrapper();
        cache.getLocker("105", wrapper);
        Assert.assertTrue(wrapper.isOwner());
        Assert.assertTrue(wrapper.isCurrent());
        Assert.assertNotNull(wrapper.getLocker());
    }
    @Test
    public void testGetLockerExist(){
        PenetrateLockWrapper wrapper = new PenetrateLockWrapper();
        cache.getLocker("106", new PenetrateLockWrapper());
        cache.getLocker("106", wrapper);
        Assert.assertFalse(wrapper.isOwner());
        Assert.assertNotNull(wrapper.getLocker());
    }

    @Test
    public void testReleaseLockerNull(){
        cache.releaseLocker(null);
        Assert.assertTrue(true);
    }
    @Test
    public void testReleaseLockerNotOwnerAndCurrent(){
        PenetrateLockWrapper wrapper = new PenetrateLockWrapper();
        wrapper.setOwner(false);
        cache.releaseLocker(wrapper);
        Assert.assertTrue(true);
    }
    @Test
    public void testReleaseLockerOwner(){
        PenetrateLockWrapper wrapper = new PenetrateLockWrapper();
        wrapper.setOwner(true);
        cache.releaseLocker(wrapper);
        Assert.assertTrue(true);
    }
    @Test
    public void testReleaseLockerCurrent(){
        PenetrateLockWrapper wrapper = new PenetrateLockWrapper();
        wrapper.setOwner(false);
        wrapper.setLocker(new PenetrateLock("key"));
        cache.releaseLocker(wrapper);
        Assert.assertTrue(true);
    }
    @Test
    public void testReleaseLocker(){
        PenetrateLockWrapper wrapper = new PenetrateLockWrapper();
        wrapper.setOwner(true);
        wrapper.setLocker(new PenetrateLock("key"));
        cache.releaseLocker(wrapper);
        Assert.assertTrue(true);
    }

    @Test
    public void testFutureExp() throws Exception{
        CompletableFuture future = Mockito.mock(CompletableFuture.class);
        Mockito.when(future.get()).thenThrow(new ExecutionException(new Throwable("test")));
        Reflect result = Reflect.on(cache).call("future", future);
        Assert.assertNull(result.get());
    }
}
