package cn.flyelf.cache.penetration;

import cn.flyelf.cache.penetration.model.PenetrateLockWrapper;
import org.junit.Assert;
import org.junit.Test;

/**
 * 穿透缓存的测试用例
 *
 * @author wujr
 * 2020/1/6
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/6 1.0 新增]
 */
public class PenetrateCacheTest {
    private static PenetrateCache cache = new PenetrateCache() {
        @Override
        public Long timeout() {
            return null;
        }

        @Override
        public long expire() {
            return 0;
        }

        @Override
        public <K> void put(K key, Object value, Long expire) {

        }

        @Override
        public <K> Object get(K key) {
            return null;
        }

        @Override
        public boolean isNull(Object object) {
            return false;
        }

        @Override
        public <K> void getLocker(K key, PenetrateLockWrapper locker) {

        }

        @Override
        public void releaseLocker(PenetrateLockWrapper locker) {

        }
    };

    @Test
    public void testEnabled(){
        Assert.assertTrue(cache.enabled());
    }
    @Test
    public void testName(){
        Assert.assertEquals("penetration", cache.name());
    }
    @Test
    public void testPut(){
        cache.put("put");
        Assert.assertTrue(true);
    }
    @Test
    public void testPutExpire(){
        cache.put("putExpire", 10L);
        Assert.assertTrue(true);
    }
}
