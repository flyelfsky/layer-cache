package cn.flyelf.cache.penetration;

import cn.flyelf.cache.penetration.model.PenetrateLockWrapper;


/**
 * 缓存穿透服务
 *
 * @author wujr
 * 2019/12/30
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/30 1.0 新增]
 */
public interface PenetrateCache {
    /**
     * 穿透等待超时时长，毫秒
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @return 等待超时时长，毫秒
     */
    Long timeout();
    /**
     * 缓存穿透默认的有效期，毫秒
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @return 有效时长，毫秒
     */
    long expire();
    /**
     * 是否启用缓存穿透处理
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @return true表示启用
     */
    default boolean enabled(){
        return true;
    }
    /**
     * 缓存穿透处理的名称
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @return 名称
     */
    default String name(){
        return "penetration";
    }
    /**
     * 加入到穿透服务中
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param key: 缓存key
     * @param value: 缓存模型实体
     * @param <K>: 缓存key类型
     * @param expire: 有效期
     */
    <K> void put(K key, Object value, Long expire);

    /**
     * 设置一个默认有效时长的null实体到缓存穿透
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param key: 缓存key
     * @param <K>: 缓存key类型
     */
    default <K> void put(K key){
        put(key, null, expire());
    }

    /**
     * 把无效的模型实体保存到缓存穿透中
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param key: 缓存key
     * @param <K>: 缓存key类型
     * @param expire: 有效期
     */
    default <K> void put(K key, Long expire){
        put(key, null, expire);
    }

    /**
     * 获取某个key的实体
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param key: 缓存key
     * @param <K>: 缓存key类型
     * @return 实体
     */
    <K> Object get(K key);

    /**
     * 判断某个对象是否为null
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param object: 对象实体
     * @return true表示为null实体
     */
    boolean isNull(Object object);

    /**
     * 获取穿透锁
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param key: 缓存key
     * @param <K>: 缓存key类型
     * @param locker: 穿透锁封装
     */
    <K> void getLocker(K key, PenetrateLockWrapper locker);

    /**
     * 释放穿透锁
     * @author wujr
     * 2019/12/30
     * 变更历史
     * [wujr 2019/12/30 1.0 新增]
     *
     * @param locker: 穿透锁
     */
    void releaseLocker(PenetrateLockWrapper locker);
}
