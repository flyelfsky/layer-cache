package cn.flyelf.cache.penetration.caffeine;

import cn.flyelf.cache.penetration.model.PenetrateLockWrapper;
import cn.flyelf.cache.penetration.PenetrateCache;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.penetration.model.PenetrateLock;
import com.github.benmanes.caffeine.cache.AsyncCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;

import javax.annotation.Nonnull;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * 利用caffeine实现对缓存的穿透处理
 *
 * @author wujr
 * 2019/12/30
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/30 1.0 新增]
 */
@Slf4j
public class PenetrateCaffeineCache implements PenetrateCache {
    /**
     * 缓存的null对象
     */
    private static final String NIL = "nil";
    private final CachePenetrationConfig config;
    /**
     * cache实体，不同的缓存有效期设置不同的实体
     * key：缓存的key的类型, Key.class-expire
     */
    private final ConcurrentHashMap<String, AsyncCache> caches = new ConcurrentHashMap<>(1);
    /**
     * 穿透锁
     * key：缓存key
     */
    private final ConcurrentHashMap<String, PenetrateLock> lockers = new ConcurrentHashMap<>(16);

    public PenetrateCaffeineCache(@Nonnull CachePenetrationConfig config){
        this.config = config;
    }

    @Override
    public Long timeout(){
        return config.getPenetrateTimeout();
    }
    @Override
    public long expire(){
        return config.getExpire();
    }
    @Override
    public boolean enabled(){
        return config.isEnabled();
    }
    @Override
    public <K> void put(K key, Object value, Long expire) {
        if (null == expire){
            expire = expire();
        }
        AsyncCache<K, Object> cache = getCache(key.getClass(), expire);
        Object object = value;
        if (null == value){
            object = NIL;
        }
        CompletableFuture<Object> future = CompletableFuture.completedFuture(object);
        cache.put(key, future);
    }

    @Override
    @SuppressWarnings("unchecked")
    public <K> Object get(K key) {
        for (Map.Entry<String, AsyncCache> entry : caches.entrySet()){
            AsyncCache<K, Object> cache = entry.getValue();
            CompletableFuture<Object> future = cache.getIfPresent(key);
            if (null == future){
                continue;
            }
            return future(future);
        }
        return null;
    }
    private Object future(CompletableFuture<Object> future){
        try {
            return future.get();
        } catch (InterruptedException | ExecutionException e) {
            log.warn("caffeine缓存穿透处理异常：", e);
            Thread.currentThread().interrupt();
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    private <K,V> AsyncCache<K, V> getCache(Class keyClass, long expire){
        return caches.computeIfAbsent(keyClass.getName() + "-" + expire, k -> Caffeine.newBuilder().expireAfterWrite(expire, TimeUnit.MILLISECONDS).buildAsync());
    }

    @Override
    public boolean isNull(Object object){
        if (object instanceof String){
            return NIL.equals(object);
        }
        return false;
    }

    @Override
    public <K> void getLocker(K key, @Nonnull PenetrateLockWrapper wrapper){
        wrapper.setOwner(false);
        PenetrateLock locker = lockers.computeIfAbsent(key.toString(), k -> {
            PenetrateLock lock = new PenetrateLock(k);
            wrapper.setOwner(true);
            return lock;
        });
        wrapper.setLocker(locker);
    }
    @Override
    public void releaseLocker(PenetrateLockWrapper locker){
        if (null == locker){
            return;
        }
        if (locker.isOwner() || locker.isCurrent()) {
            locker.release();
            if (locker.isOwner() && null != locker.key()){
                lockers.remove(locker.key());
            }
        }
    }
}
