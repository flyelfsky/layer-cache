package cn.flyelf.cache.penetration.conf;

import lombok.Data;

/**
 * 缓存穿透雪崩的配置模型
 *
 * @author wujr
 * 2019/12/30
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/30 1.0 新增]
 */
@Data
public class CachePenetrationConfig {
    /**
     * 是否开启穿透雪崩的防范处理
     */
    private boolean enabled = true;
    /**
     * 默认情况下，null模型的缓存时长
     * 单位：毫秒，默认为60秒
     */
    private long expire = 60 * 1000L;
    /**
     * 穿透等待超时时长，默认为5毫秒
     */
    private long penetrateTimeout = 5L;
}
