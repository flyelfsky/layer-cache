package cn.flyelf.cache.penetration.model;

import lombok.Data;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

/**
 * 穿刺预防锁
 * 本锁并非为分布式锁
 *
 * @author wujr
 * 2019/12/30
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/30 1.0 新增]
 */
@Data
public class PenetrateLock {
    private Thread lockThread = Thread.currentThread();
    private CountDownLatch signal = new CountDownLatch(1);
    private boolean success = false;
    private Object value = null;
    private String key;

    public PenetrateLock(String key){
        this.key = key;
    }

    public boolean isCurrent(){
        return lockThread == Thread.currentThread();
    }

    public void release(){
        signal.countDown();
    }

    public boolean await(Long duration) throws InterruptedException {
        if (null == duration){
            signal.await();
            return true;
        }
        return signal.await(duration, TimeUnit.MILLISECONDS);
    }
}
