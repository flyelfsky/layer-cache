package cn.flyelf.cache.penetration.model;

import lombok.Data;

/**
 * 穿透锁的封装
 *
 * @author wujr
 * 2019/12/30
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/30 1.0 新增]
 */
@Data
public class PenetrateLockWrapper {
    private PenetrateLock locker;
    /**
     * 当前是否为拥有者
     */
    private boolean owner = false;

    public boolean isCurrent(){
        return locker != null && locker.isCurrent();
    }

    public void release(){
        if (null != locker){
            locker.release();
        }
    }

    public String key(){
        if (null == locker){
            return null;
        }
        return locker.getKey();
    }
}
