package cn.flyelf.cache.starter.conf;

import lombok.Data;

/**
 * 缓存监控配置参数
 *
 * @author wujr
 * 2019-12-27
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-27 1.0 新增]
 */
@Data
public class MonitorConfig {
    /**
     * 缓存监控的时间间隔：秒
     */
    private long interval = 60L;
}
