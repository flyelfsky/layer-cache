package cn.flyelf.cache.starter.exception;

/**
 * 缺乏缓存层的配置参数
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
public class LackOfCacheLayerPropertiesException extends Exception {
    private static final long serialVersionUID = -1933316798996441387L;

    public LackOfCacheLayerPropertiesException(String msg){
        super(msg);
    }
}
