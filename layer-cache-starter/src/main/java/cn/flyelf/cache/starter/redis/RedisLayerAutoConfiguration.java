package cn.flyelf.cache.starter.redis;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import cn.flyelf.cache.redis.util.RedisUtil;
import cn.flyelf.cache.spring.conf.LayerCacheConfiguration;
import cn.flyelf.cache.starter.LayerCacheProperties;
import cn.flyelf.cache.starter.init.AbstractCacheLayerInitializer;
import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.lettuce.core.RedisURI;
import org.apache.commons.pool2.impl.GenericObjectPoolConfig;
import org.redisson.Redisson;
import org.redisson.api.RedissonClient;
import org.redisson.config.ClusterServersConfig;
import org.redisson.config.Config;
import org.redisson.config.SingleServerConfig;
import org.springframework.boot.autoconfigure.AutoConfigureAfter;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisClusterConfiguration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisNode;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceClientConfiguration;
import org.springframework.data.redis.connection.lettuce.LettuceConnectionFactory;
import org.springframework.data.redis.connection.lettuce.LettucePoolingClientConfiguration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;

import java.text.SimpleDateFormat;
import java.time.Duration;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * redis的缓存层的自动配置
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
@Configuration
@AutoConfigureAfter(LayerCacheConfiguration.class)
public class RedisLayerAutoConfiguration {
    private static final String AUTO_BEAN_NAME = "redisLayerAutoInitializer";

    @Bean(name = AUTO_BEAN_NAME)
    public RedisCacheLayerInitializer redisCacheLayerInitializer(RedisCacheLayerProcessor redisLayerProcessor){
        return new RedisCacheLayerInitializer(redisLayerProcessor);
    }

    public static class RedisCacheLayerInitializer extends AbstractCacheLayerInitializer<RedisCacheLayerProcessor> {
        RedisCacheLayerInitializer(RedisCacheLayerProcessor redisLayerProcessor){
            super(redisLayerProcessor);
        }
        @Override
        protected void initializeLayerProcessor(RedisCacheLayerProcessor layerProcessor, Map<String, CacheLayerConfig> config) {
            for (Map.Entry<String, CacheLayerConfig> entry : config.entrySet()){
                layerProcessor.buildAreaClient(entry.getKey(), entry.getValue());
            }
        }
    }

    /**
     * redis缓存池参数配置
     * 连接池配置信息（如果不用连接池可以省略这一步）
     * @author wujr
     * 2020/3/5
     * 变更历史
     * [wujr 2020/3/5 1.0 新增]
     *
     * @return 对象池配置
     */
    @Bean
    public GenericObjectPoolConfig<?> genericObjectPoolConfig(LayerCacheProperties layerCacheProperties) {
        CacheLayerConfig layerConfig = findRedisLayerConfig(layerCacheProperties);
        if (null == layerConfig){
            return new GenericObjectPoolConfig<>();
        }
        return RedisUtil.buildPoolConfig(layerConfig);
    }

    private CacheLayerConfig findRedisLayerConfig(LayerCacheProperties layerCacheProperties){
        Map<String, CacheLayerConfig> layerConfigs = layerCacheProperties.getConfig(CacheConstant.LAYER_REDIS);
        if (null == layerConfigs || layerConfigs.isEmpty()){
            return null;
        }
        CacheLayerConfig layerConfig = layerConfigs.get("default");
        if (null == layerConfig){
            layerConfig = layerConfigs.values().iterator().next();
        }
        return layerConfig;
    }

    @Bean(destroyMethod = "shutdown")
    @ConditionalOnProperty("layer-cache.redisson")
    public RedissonClient redissonClient(LayerCacheProperties layerCacheProperties){
        Config config = new Config();
        CacheLayerConfig layerConfig = findRedisLayerConfig(layerCacheProperties);
        if (null == layerConfig){
            return Redisson.create();
        }
        String[] uri = layerConfig.getUri();
        if (uri.length == 1){
            RedisURI redisUri = RedisURI.create(uri[0]);
            SingleServerConfig serverConfig = config.useSingleServer().setAddress("redis://" + redisUri.getHost() + ":" + redisUri.getPort());
            if (redisUri.getPassword() != null){
                serverConfig.setPassword(new String(redisUri.getPassword()));
            }
            serverConfig.setDatabase(redisUri.getDatabase());
            if (layerConfig.getPool() != null && layerConfig.getPool().getMaxActive() != null) {
                serverConfig.setConnectionPoolSize(layerConfig.getPool().getMaxActive());
            }
        }else{
            ClusterServersConfig serverConfig = config.useClusterServers();
            String[] address = new String[uri.length];
            String password = null;
            int database = 0;
            for (int index = 0; index < uri.length; ++ index){
                RedisURI redisUri = RedisURI.create(uri[index]);
                String addr = "redis://" + redisUri.getHost() + ":" + redisUri.getPort();
                address[index] = addr;
                if (null == password && null != redisUri.getPassword()){
                    password = new String(redisUri.getPassword());
                }
                if (database == 0 && redisUri.getDatabase() > 0) {
                    database = redisUri.getDatabase();
                }
            }
            serverConfig.addNodeAddress(address);
            if (null != password){
                serverConfig.setPassword(password);
            }
            if (layerConfig.getPool() != null && layerConfig.getPool().getMaxActive() != null) {
                serverConfig.setMasterConnectionPoolSize(layerConfig.getPool().getMaxActive());
            }
        }
        return Redisson.create(config);
    }

    @Bean
    public LettuceClientConfiguration lettuceClientConfiguration(GenericObjectPoolConfig<?> genericObjectPoolConfig){
        //构造LettucePoolingClientConfiguration对象，同时加入连接池配置信息
        return LettucePoolingClientConfiguration.builder().poolConfig(genericObjectPoolConfig).build();
    }
    private LettuceClientConfiguration buildLettuceClientConfiguration(CacheLayerConfig layerConfig){
        //连接池配置
        GenericObjectPoolConfig<?> genericObjectPoolConfig = RedisUtil.buildPoolConfig(layerConfig);

        //redis客户端配置
        LettucePoolingClientConfiguration.LettucePoolingClientConfigurationBuilder
                builder =  LettucePoolingClientConfiguration.builder().
                commandTimeout(Duration.ofMillis(layerConfig.getCommandTimeout()));

        builder.shutdownTimeout(Duration.ofMillis(layerConfig.getShutdownTimeout()));
        builder.poolConfig(genericObjectPoolConfig);
        return builder.build();
    }
    private LettuceConnectionFactory createClusterConnectionFaction(LayerCacheProperties layerCacheProperties, CacheLayerConfig layerConfig){
        RedisClusterConfiguration redisConfiguration = new RedisClusterConfiguration();
        String[] uris = layerConfig.getUri();
        Set<RedisNode> redisNodes = new HashSet<>(uris.length);
        int database = 0;
        char[] password = null;
        for (String uri : uris){
            RedisURI redisUri = RedisURI.create(uri);
            RedisNode node = new RedisNode(redisUri.getHost(), redisUri.getPort());
            redisNodes.add(node);
            if (database == 0 && redisUri.getDatabase() > 0){
                database = redisUri.getDatabase();
            }
            if (password == null && redisUri.getPassword() != null){
                password = redisUri.getPassword();
            }
        }
        redisConfiguration.setClusterNodes(redisNodes);
        redisConfiguration.setPassword(password);

        LettuceClientConfiguration clientConfiguration = buildLettuceClientConfiguration(layerConfig);

        //根据配置和客户端配置创建连接
        LettuceConnectionFactory factory = new LettuceConnectionFactory(redisConfiguration, clientConfiguration);
        factory.afterPropertiesSet();

        return factory;
    }
    private LettuceConnectionFactory createSingleConnectionFactory(LayerCacheProperties layerCacheProperties, CacheLayerConfig layerConfig){
        RedisURI uri = RedisURI.create(layerConfig.getUri()[0]);
        //redis配置
        RedisStandaloneConfiguration redisConfiguration = new RedisStandaloneConfiguration(uri.getHost(), uri.getPort());
        redisConfiguration.setDatabase(uri.getDatabase());
        redisConfiguration.setPassword(uri.getPassword());

        LettuceClientConfiguration clientConfiguration = buildLettuceClientConfiguration(layerConfig);

        //根据配置和客户端配置创建连接
        LettuceConnectionFactory factory = new LettuceConnectionFactory(redisConfiguration, clientConfiguration);
        factory.afterPropertiesSet();

        return factory;
    }
    @Bean
    public LettuceConnectionFactory lettuceConnectionFactory(LayerCacheProperties layerCacheProperties){
        CacheLayerConfig layerConfig = findRedisLayerConfig(layerCacheProperties);
        if (null == layerConfig){
            return new LettuceConnectionFactory();
        }
        if (layerConfig.getUri() == null){
            return new LettuceConnectionFactory();
        }else if (layerConfig.getUri().length == 1){
            return createSingleConnectionFactory(layerCacheProperties, layerConfig);
        }else{
            return createClusterConnectionFaction(layerCacheProperties, layerConfig);
        }
    }

    /**
     * redis模板操作类,类似于jdbcTemplate的一个类;
     * 虽然CacheManager也能获取到Cache对象，但是操作起来没有那么灵活；
     * 这里在扩展下：RedisTemplate这个类不见得很好操作，我们可以在进行扩展一个我们
     * 自己的缓存类，比如：RedisStorage类;
     * @param factory : 通过Spring进行注入，参数在application.properties进行配置；
     * @return redis模版对象
     */
    @Bean
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory factory) {
        return buildRedisTemplate(factory);
    }
    private RedisTemplate<String, Object> buildRedisTemplate(RedisConnectionFactory factory) {
        RedisTemplate<String, Object> redisTemplate = new RedisTemplate<>();
        redisTemplate.setConnectionFactory(factory);

        // key序列化方式;（不然会出现乱码;）,但是如果方法上有Long等非String类型的话，会报类型转换错误；
        // 所以在没有自己定义key生成策略的时候，以下这个代码建议不要这么写，可以不配置或者自己实现ObjectRedisSerializer
        // 或者JdkSerializationRedisSerializer序列化方式;
        // 使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值（默认使用JDK的序列化方式）
        Jackson2JsonRedisSerializer<Object> jacksonSerial = makeJackson();

        // 值采用json序列化
        redisTemplate.setValueSerializer(jacksonSerial);
        //使用StringRedisSerializer来序列化和反序列化redis的key值
        redisTemplate.setKeySerializer(new StringRedisSerializer());

        // 设置hash key 和value序列化模式
        redisTemplate.setHashKeySerializer(new StringRedisSerializer());
        redisTemplate.setHashValueSerializer(jacksonSerial);
        redisTemplate.afterPropertiesSet();

        return redisTemplate;
    }
    /**
     * key序列化方式;（不然会出现乱码;）,但是如果方法上有Long等非String类型的话，会报类型转换错误；
     * 所以在没有自己定义key生成策略的时候，以下这个代码建议不要这么写，可以不配置或者自己实现ObjectRedisSerializer
     * 或者JdkSerializationRedisSerializer序列化方式;
     * 使用Jackson2JsonRedisSerializer来序列化和反序列化redis的value值（默认使用JDK的序列化方式）
     * @return json的序列化
     */
    private Jackson2JsonRedisSerializer<Object> makeJackson(){
        Jackson2JsonRedisSerializer<Object> jacksonSerial = new Jackson2JsonRedisSerializer<>(Object.class);

        ObjectMapper om = new ObjectMapper();
        // 指定要序列化的域，field,get和set,以及修饰符范围，ANY是都有包括private和public
        om.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.ANY);
        // 指定序列化输入的类型，类必须是非final修饰的，final修饰的类，比如String,Integer等会跑出异常
        om.enableDefaultTyping(ObjectMapper.DefaultTyping.NON_FINAL);
        // 修正：设置日期的格式化，否则无法存储早于1970-01-01的事件的问题
        om.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
        jacksonSerial.setObjectMapper(om);
        return jacksonSerial;
    }
}
