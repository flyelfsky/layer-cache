package cn.flyelf.cache.starter.caffeine;

import cn.flyelf.cache.caffeine.CaffeineCacheLayerProcessor;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.starter.init.AbstractCacheLayerInitializer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * caffeine缓存层的自动配置
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
@Configuration
@Slf4j
public class CaffeineLayerAutoConfiguration {
    private static final String AUTO_BEAN_NAME = "caffeineLayerAutoInitializer";

    @Bean(name = AUTO_BEAN_NAME)
    public CaffeineCacheLayerInitializer caffeineCacheLayerInitializer(CaffeineCacheLayerProcessor caffeineLayerProcessor){
        return new CaffeineCacheLayerInitializer(caffeineLayerProcessor);
    }

    public static class CaffeineCacheLayerInitializer extends AbstractCacheLayerInitializer<CaffeineCacheLayerProcessor> {
        CaffeineCacheLayerInitializer(CaffeineCacheLayerProcessor layerProcessor){
            super(layerProcessor);
        }

        @Override
        protected void initializeLayerProcessor(CaffeineCacheLayerProcessor layerProcessor, Map<String, CacheLayerConfig> config) {
            for (Map.Entry<String, CacheLayerConfig> entry : config.entrySet()){
                buildArea(layerProcessor, entry.getKey(), entry.getValue());
            }
        }
        private void buildArea(CaffeineCacheLayerProcessor layerProcessor, String area, CacheLayerConfig config){
            log.trace("caffeine layer build area, processor = {}, area = {}, config = {}", layerProcessor, area, config);
        }
    }
}
