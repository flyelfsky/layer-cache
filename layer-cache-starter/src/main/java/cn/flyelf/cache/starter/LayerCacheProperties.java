package cn.flyelf.cache.starter;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.starter.conf.MonitorConfig;
import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;

/**
 * 多层缓存的spring配置
 * layer-cache:
 *   redis:
 *     default:
 *       value-serializer: default
 *       key-serializer: utf8
 *       uri: redis://pwd@ip:port/db
 *   caffeine:
 *     default:
 *       limit: 100
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
@ConfigurationProperties(prefix = "layer-cache")
@Getter
@Setter
public class LayerCacheProperties {
    /**
     * 穿透雪崩的配置
     */
    private CachePenetrationConfig penetration = new CachePenetrationConfig();
    /**
     * redis的配置
     */
    private Map<String, CacheLayerConfig> redis;
    /**
     * caffeine的配置
     */
    private Map<String, CacheLayerConfig> caffeine;
    /**
     * 监控配置
     */
    private MonitorConfig monitor = new MonitorConfig();

    public Map<String, CacheLayerConfig> getConfig(String layerName){
        if (CacheConstant.LAYER_REDIS.equals(layerName)){
            return redis;
        }else if (CacheConstant.LAYER_CAFFEINE.equals(layerName)){
            return caffeine;
        }else{
            return null;
        }
    }
}
