package cn.flyelf.cache.starter;

import cn.flyelf.cache.core.conf.CacheGlobalConfig;
import cn.flyelf.cache.spring.CacheManager;
import cn.flyelf.cache.starter.caffeine.CaffeineLayerAutoConfiguration;
import cn.flyelf.cache.starter.redis.RedisLayerAutoConfiguration;
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;

/**
 * 多级缓存的自动化配置
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
@Configuration
@ConditionalOnClass(CacheManager.class)
@EnableConfigurationProperties(LayerCacheProperties.class)
@Import({
        RedisLayerAutoConfiguration.class,
        CaffeineLayerAutoConfiguration.class
})
public class LayerCacheAutoConfiguration {

    @Bean
    @ConditionalOnMissingBean
    public CacheGlobalConfig globalConfig(LayerCacheProperties cacheProperties){
        CacheGlobalConfig globalConfig = new CacheGlobalConfig();
        globalConfig.setCacheMonitorInterval(cacheProperties.getMonitor().getInterval());
        globalConfig.setPenetrationConfig(cacheProperties.getPenetration());
        return globalConfig;
    }
}
