package cn.flyelf.cache.starter.init;

import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.starter.LayerCacheProperties;
import cn.flyelf.cache.starter.exception.LackOfCacheLayerPropertiesException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Map;

/**
 * 抽象的缓存层初始化
 * @param <P>: 缓存层处理器类型
 *
 * @author wujr
 * 2019/12/24
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/24 1.0 新增]
 */
@Slf4j
public abstract class AbstractCacheLayerInitializer<P extends CacheLayerProcessor> implements InitializingBean {
    @Autowired
    private LayerCacheProperties layerCacheProperties;
    private final P layerProcessor;

    public AbstractCacheLayerInitializer(P layerProcessor){
        this.layerProcessor = layerProcessor;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        Map<String, CacheLayerConfig> config = layerCacheProperties.getConfig(layerProcessor.name());
        if (null == config){
            throw new LackOfCacheLayerPropertiesException(layerProcessor.name());
        }
        initializeLayerProcessor(layerProcessor, config);
    }

    /**
     * 初始化缓存层
     * @author wujr
    * 2019/12/24
     * 变更历史
     * [wujr 2019/12/24 1.0 新增]
     *
     * @param layerProcessor: 缓冲层
     * @param config: 配置
     */
    protected abstract void initializeLayerProcessor(P layerProcessor, Map<String, CacheLayerConfig> config);
}
