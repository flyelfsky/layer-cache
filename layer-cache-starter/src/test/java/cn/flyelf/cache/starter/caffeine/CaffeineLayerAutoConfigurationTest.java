package cn.flyelf.cache.starter.caffeine;

import cn.flyelf.cache.caffeine.CaffeineCacheLayerProcessor;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

/**
 * CaffeineLayerAutoConfiguration的测试用例
 *
 * @author wujr
 * 2020/1/9
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/9 1.0 新增]
 */
public class CaffeineLayerAutoConfigurationTest {
    @Test
    public void testCaffeineCacheLayerInitializer(){
        CaffeineCacheLayerProcessor processor = Mockito.mock(CaffeineCacheLayerProcessor.class);
        CaffeineLayerAutoConfiguration.CaffeineCacheLayerInitializer initializer = new CaffeineLayerAutoConfiguration.CaffeineCacheLayerInitializer(processor);
        Map<String, CacheLayerConfig> map = new HashMap<>(1);
        CacheLayerConfig config = Mockito.mock(CacheLayerConfig.class);
        map.put("test", config);
        initializer.initializeLayerProcessor(processor, map);
        Assert.assertTrue(true);
    }

    @Test
    public void testAutoConfiguration(){
        CaffeineLayerAutoConfiguration configuration = new CaffeineLayerAutoConfiguration();
        CaffeineCacheLayerProcessor processor = Mockito.mock(CaffeineCacheLayerProcessor.class);
        CaffeineLayerAutoConfiguration.CaffeineCacheLayerInitializer result = configuration.caffeineCacheLayerInitializer(processor);
        Assert.assertNotNull(result);
    }
}
