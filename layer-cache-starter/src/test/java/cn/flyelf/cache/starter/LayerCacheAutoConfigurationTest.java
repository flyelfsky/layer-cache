package cn.flyelf.cache.starter;

import cn.flyelf.cache.core.conf.CacheGlobalConfig;
import org.junit.Assert;
import org.junit.Test;

/**
 * LayerCacheAutoConfiguration的测试用例
 *
 * @author wujr
 * 2020/1/9
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/9 1.0 新增]
 */
public class LayerCacheAutoConfigurationTest {
    @Test
    public void testGlobalConfig(){
        LayerCacheAutoConfiguration configuration = new LayerCacheAutoConfiguration();
        LayerCacheProperties properties = new LayerCacheProperties();
        CacheGlobalConfig result = configuration.globalConfig(properties);
        Assert.assertNotNull(result);
    }
}
