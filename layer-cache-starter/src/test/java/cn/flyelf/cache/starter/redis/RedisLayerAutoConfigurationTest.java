package cn.flyelf.cache.starter.redis;

import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.redis.RedisCacheLayerProcessor;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

/**
 * RedisLayerAutoConfiguration的测试用例
 *
 * @author wujr
 * 2020/1/9
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/9 1.0 新增]
 */
public class RedisLayerAutoConfigurationTest {
    @Test
    public void testRedisCacheLayerInitializer(){
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        RedisLayerAutoConfiguration.RedisCacheLayerInitializer initializer = new RedisLayerAutoConfiguration.RedisCacheLayerInitializer(processor);
        Map<String, CacheLayerConfig> map = new HashMap<>(1);
        CacheLayerConfig config = Mockito.mock(CacheLayerConfig.class);
        map.put("test", config);
        initializer.initializeLayerProcessor(processor, map);
        Assert.assertTrue(true);
    }

    @Test
    public void testAutoConfiguration(){
        RedisLayerAutoConfiguration configuration = new RedisLayerAutoConfiguration();
        RedisCacheLayerProcessor processor = Mockito.mock(RedisCacheLayerProcessor.class);
        RedisLayerAutoConfiguration.RedisCacheLayerInitializer result = configuration.redisCacheLayerInitializer(processor);
        Assert.assertNotNull(result);
    }
}
