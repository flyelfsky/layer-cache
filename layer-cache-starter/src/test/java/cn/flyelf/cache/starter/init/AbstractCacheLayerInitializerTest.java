package cn.flyelf.cache.starter.init;

import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.starter.LayerCacheProperties;
import cn.flyelf.cache.starter.exception.LackOfCacheLayerPropertiesException;
import org.joor.Reflect;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.HashMap;
import java.util.Map;

/**
 * AbstractCacheLayerInitializer的测试用例
 *
 * @author wujr
 * 2020/1/9
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/9 1.0 新增]
 */
public class AbstractCacheLayerInitializerTest {
    private static class TestCacheLayerInitializer extends AbstractCacheLayerInitializer{
        @SuppressWarnings("unchecked")
        TestCacheLayerInitializer(CacheLayerProcessor processor){
            super(processor);
        }
        @Override
        protected void initializeLayerProcessor(CacheLayerProcessor layerProcessor, Map config) {
        }
    }
    @Test(expected = LackOfCacheLayerPropertiesException.class)
    public void testAfterPropertiesSetLack() throws Exception{
        LayerCacheProperties properties = Mockito.mock(LayerCacheProperties.class);
        Mockito.when(properties.getConfig(Mockito.anyString())).thenReturn(null);
        CacheLayerProcessor processor = Mockito.mock(CacheLayerProcessor.class);
        Mockito.when(processor.name()).thenReturn("test");
        TestCacheLayerInitializer initializer = new TestCacheLayerInitializer(processor);
        Reflect.on(initializer).set("layerCacheProperties", properties);

        initializer.afterPropertiesSet();
        Assert.assertTrue(true);
    }
    @Test
    public void testAfterPropertiesSet() throws Exception{
        LayerCacheProperties properties = Mockito.mock(LayerCacheProperties.class);
        Map<String, CacheLayerConfig> config = new HashMap<>();
        Mockito.when(properties.getConfig(Mockito.anyString())).thenReturn(config);
        CacheLayerProcessor processor = Mockito.mock(CacheLayerProcessor.class);
        Mockito.when(processor.name()).thenReturn("test");
        TestCacheLayerInitializer initializer = new TestCacheLayerInitializer(processor);
        Reflect.on(initializer).set("layerCacheProperties", properties);

        initializer.afterPropertiesSet();
        Assert.assertTrue(true);
    }
}
