package cn.flyelf.cache.starter;

import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.conf.CacheLayerConfig;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import cn.flyelf.cache.starter.conf.MonitorConfig;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

/**
 * LayerCacheProperties的测试用例
 *
 * @author wujr
 * 2020/1/9
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/9 1.0 新增]
 */
public class LayerCachePropertiesTest {
    @Test
    public void testGetAndSet(){
        LayerCacheProperties properties = new LayerCacheProperties();
        CachePenetrationConfig config = properties.getPenetration();
        Assert.assertNotNull(config);
        config = new CachePenetrationConfig();
        properties.setPenetration(config);
        CachePenetrationConfig config2 = properties.getPenetration();
        Assert.assertEquals(config, config2);

        Map<String, CacheLayerConfig> redis = new HashMap<>();
        Map<String, CacheLayerConfig> redis1 = properties.getRedis();
        Assert.assertNull(redis1);
        properties.setRedis(redis);
        redis1 = properties.getRedis();
        Assert.assertEquals(redis, redis1);

        Map<String, CacheLayerConfig> caffeine = new HashMap<>();
        Map<String, CacheLayerConfig> caffeine1 = properties.getCaffeine();
        Assert.assertNull(caffeine1);
        properties.setCaffeine(caffeine);
        caffeine1 = properties.getCaffeine();
        Assert.assertEquals(caffeine, caffeine1);

        MonitorConfig monitorConfig = new MonitorConfig();
        MonitorConfig monitorConfig1 = properties.getMonitor();
        Assert.assertNotNull(monitorConfig1);
        properties.setMonitor(monitorConfig);
        monitorConfig1 = properties.getMonitor();
        Assert.assertEquals(monitorConfig, monitorConfig1);
    }

    @Test
    public void testGetConfigRedis(){
        LayerCacheProperties properties = new LayerCacheProperties();
        Map<String, CacheLayerConfig> result = properties.getConfig(CacheConstant.LAYER_REDIS);
        Assert.assertNull(result);
    }
    @Test
    public void testGetConfigCaffeine(){
        LayerCacheProperties properties = new LayerCacheProperties();
        Map<String, CacheLayerConfig> result = properties.getConfig(CacheConstant.LAYER_CAFFEINE);
        Assert.assertNull(result);
    }
    @Test
    public void testGetConfigOther(){
        LayerCacheProperties properties = new LayerCacheProperties();
        Map<String, CacheLayerConfig> result = properties.getConfig("test");
        Assert.assertNull(result);
    }
}
