package cn.flyelf.cache.caffeine.set;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.caffeine.BaseCaffeineCacheAction;
import cn.flyelf.cache.caffeine.CaffeineCacheLayerProcessor;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import com.github.benmanes.caffeine.cache.AsyncCache;
import reactor.core.publisher.Mono;

import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 * caffeine的set类型获取
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
public class CaffeineCacheSetGetAction<K, V> extends BaseCaffeineCacheAction<K, Set<V>, Set<V>> {
    public CaffeineCacheSetGetAction(CaffeineCacheLayerProcessor processor, String area){
        super(ACTION.GETSET, processor, area);
    }
    @Override
    public Mono<Boolean> onLoadAfter(CacheExchange<K, Set<V>, Set<V>> exchange, Set<V> value) {
        AsyncCache<K, Set<V>> cache = processor().getCache(exchange.getPolicy(), area);
        CompletableFuture<Set<V>> future = CompletableFuture.completedFuture(value);
        cache.put(exchange.getRequest().getKey(), future);
        return Mono.just(true);
    }
    @Override
    protected Mono<CacheResult<Set<V>>> doAction(CacheExchange<K, Set<V>, Set<V>> exchange) {
        AsyncCache<K, Set<V>> cache = processor().getCache(exchange.getPolicy(), area);
        CompletableFuture<Set<V>> future = cache.getIfPresent(exchange.getRequest().getKey());
        if (null == future){
            return Mono.error(new CacheNotExistException(processor().name(), exchange.getRequest().getKey().toString()));
        }
        return Mono.fromFuture(future).map(r -> CacheResult.success(processor().name(), r));
    }
}
