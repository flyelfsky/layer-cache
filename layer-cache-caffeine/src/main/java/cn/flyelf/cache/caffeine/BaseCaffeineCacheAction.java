package cn.flyelf.cache.caffeine;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.caffeine.context.CaffeineCacheContext;
import cn.flyelf.cache.core.AbstractCacheAction;
import cn.flyelf.cache.core.server.CacheExchange;

/**
 * caffeine缓存的动作的基础封装
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 * @param <R>: 缓存动作的返回类型
 *
 * @author wujr
 * 2019-12-22
 * @version 1.0
 * 变更历史
 * [wujr 2019-12-22 1.0 新增]
 */
public abstract class BaseCaffeineCacheAction<K, V, R> extends AbstractCacheAction<K, V, R, CaffeineCacheLayerProcessor> {
    public BaseCaffeineCacheAction(ACTION action, CaffeineCacheLayerProcessor processor, String area){
        super(action, processor, area);
    }

    @Override
    protected CaffeineCacheContext initContext(CacheExchange<K, V, R> exchange){
        return new CaffeineCacheContext();
    }
}
