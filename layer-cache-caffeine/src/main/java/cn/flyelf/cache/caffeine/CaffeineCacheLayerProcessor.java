package cn.flyelf.cache.caffeine;

import cn.flyelf.cache.caffeine.hash.CaffeineCacheMapAddAction;
import cn.flyelf.cache.caffeine.hash.CaffeineCacheMapPutAction;
import cn.flyelf.cache.caffeine.list.CaffeineCacheListGetAction;
import cn.flyelf.cache.caffeine.list.CaffeineCacheListPutAction;
import cn.flyelf.cache.caffeine.set.CaffeineCacheSetGetAction;
import cn.flyelf.cache.caffeine.set.CaffeineCacheSetPutAction;
import cn.flyelf.cache.caffeine.simple.CaffeineCacheGetAction;
import cn.flyelf.cache.caffeine.simple.CaffeineCachePutAction;
import cn.flyelf.cache.caffeine.util.CaffeineUtil;
import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.caffeine.hash.CaffeineCacheMapGetAction;
import cn.flyelf.cache.core.action.AbstractCacheLayerProcessor;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.model.CachePolicy;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Constructor;
import java.util.concurrent.ConcurrentHashMap;

/**
 * caffeine的缓存层处理器
 *
 * @author wujr
 * 2019/12/13
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/13 1.0 新增]
 */
@Slf4j
public class CaffeineCacheLayerProcessor extends AbstractCacheLayerProcessor {
    /**
     * 缓存caffeine实体
     * key：area-缓存策略名称
     * value：Cache实体，例如: AsyncCache
     */
    private final ConcurrentHashMap<String, Object> caches = new ConcurrentHashMap<>();
    private static final ConcurrentHashMap<ACTION, Constructor<?>> CONSTRUCTORS = new ConcurrentHashMap<>(9);

    static {
        CONSTRUCTORS.put(ACTION.GET, CaffeineUtil.actionConstructor(CaffeineCacheGetAction.class));
        CONSTRUCTORS.put(ACTION.PUT, CaffeineUtil.actionConstructor(CaffeineCachePutAction.class));
        CONSTRUCTORS.put(ACTION.DELETE, CaffeineUtil.actionConstructor(CaffeineCacheDeleteAction.class));
        CONSTRUCTORS.put(ACTION.PUTLIST, CaffeineUtil.actionConstructor(CaffeineCacheListPutAction.class));
        CONSTRUCTORS.put(ACTION.GETLIST, CaffeineUtil.actionConstructor(CaffeineCacheListGetAction.class));
        CONSTRUCTORS.put(ACTION.PUTSET, CaffeineUtil.actionConstructor(CaffeineCacheSetPutAction.class));
        CONSTRUCTORS.put(ACTION.GETSET, CaffeineUtil.actionConstructor(CaffeineCacheSetGetAction.class));
        CONSTRUCTORS.put(ACTION.PUTMAP, CaffeineUtil.actionConstructor(CaffeineCacheMapPutAction.class));
        CONSTRUCTORS.put(ACTION.GETMAP, CaffeineUtil.actionConstructor(CaffeineCacheMapGetAction.class));
        CONSTRUCTORS.put(ACTION.ADDMAPKEY, CaffeineUtil.actionConstructor(CaffeineCacheMapAddAction.class));
    }
    public CaffeineCacheLayerProcessor(CacheEventPublisher eventPublisher){
        super(eventPublisher);
    }
    @Override
    public CacheAction<?, ?, ?> action(ACTION action, String area) {
        Constructor<?> constructor = CONSTRUCTORS.get(action);
        if (null != constructor){
            return CaffeineUtil.action(this, constructor, area);
        }
        return null;
    }

    @SuppressWarnings("unchecked")
    public <T> T getCache(CachePolicy policy, String area){
        return (T)caches.computeIfAbsent(area + "-" + policy.name(), k -> CaffeineUtil.asyncCache(policy));
    }
}
