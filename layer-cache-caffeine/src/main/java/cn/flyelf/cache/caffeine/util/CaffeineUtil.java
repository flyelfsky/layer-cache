package cn.flyelf.cache.caffeine.util;

import cn.flyelf.cache.caffeine.CaffeineCacheLayerProcessor;
import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.model.CachePolicy;
import com.github.benmanes.caffeine.cache.AsyncCache;
import com.github.benmanes.caffeine.cache.Caffeine;
import lombok.extern.slf4j.Slf4j;

import java.lang.reflect.Constructor;

/**
 * 辅助工具
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
@Slf4j
public class CaffeineUtil {
    private CaffeineUtil(){}
    @SuppressWarnings("unchecked")
    public static <K, V> AsyncCache<K, V> asyncCache(CachePolicy policy){
        Caffeine caffeine = Caffeine.newBuilder();
        if (policy.getDuration() != null){
            caffeine.expireAfterWrite(policy.getDuration(), policy.getTimeUnit());
        }
        if (policy.getLimit() != null){
            caffeine.maximumSize(policy.getLimit());
        }

        return caffeine.buildAsync();
    }

    public static <T> Constructor actionConstructor(Class<T> clz){
        try {
            return clz.getDeclaredConstructor(CaffeineCacheLayerProcessor.class, String.class);
        } catch (NoSuchMethodException e) {
            throw new UnsupportedClassVersionError("CacheAction's constructor not valid");
        }
    }

    public static CacheAction action(CaffeineCacheLayerProcessor processor, Constructor constructor, String area){
        try {
            return (CacheAction)constructor.newInstance(processor, area);
        } catch (Exception e) {
            log.warn("caffeine CacheAction construct error: ", e);
        }
        return null;
    }
}
