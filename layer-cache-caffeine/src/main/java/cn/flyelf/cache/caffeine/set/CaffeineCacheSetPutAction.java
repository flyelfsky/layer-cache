package cn.flyelf.cache.caffeine.set;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.caffeine.BaseCaffeineCacheAction;
import cn.flyelf.cache.caffeine.CaffeineCacheLayerProcessor;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import com.github.benmanes.caffeine.cache.AsyncCache;
import reactor.core.publisher.Mono;

import java.util.Set;
import java.util.concurrent.CompletableFuture;

/**
 * caffeine的set类型的put操作
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
public class CaffeineCacheSetPutAction<K, V> extends BaseCaffeineCacheAction<K, Set<V>, Boolean> {
    public CaffeineCacheSetPutAction(CaffeineCacheLayerProcessor processor, String area){
        super(ACTION.PUTSET, processor, area);
    }

    @Override
    protected Mono<CacheResult<Boolean>> doAction(CacheExchange<K, Set<V>, Boolean> exchange) {
        AsyncCache<K, Set<V>> cache = processor().getCache(exchange.getPolicy(), area);
        CompletableFuture<Set<V>> future = CompletableFuture.completedFuture(exchange.getRequest().getValue());
        cache.put(exchange.getRequest().getKey(), future);
        return Mono.just(CacheResult.success(processor().name(), true));
    }
}
