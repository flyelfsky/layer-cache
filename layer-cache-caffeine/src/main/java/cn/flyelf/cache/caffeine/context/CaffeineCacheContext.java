package cn.flyelf.cache.caffeine.context;

import cn.flyelf.cache.core.context.CacheContext;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * caffeine的缓存上下文
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class CaffeineCacheContext extends CacheContext {
}
