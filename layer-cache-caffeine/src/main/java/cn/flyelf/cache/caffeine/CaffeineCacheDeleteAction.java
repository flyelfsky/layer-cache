package cn.flyelf.cache.caffeine;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import com.github.benmanes.caffeine.cache.AsyncCache;
import reactor.core.publisher.Mono;

import java.util.Arrays;
import java.util.Collection;

/**
 * 删除缓存
 * @param <K>: 缓存key类型
 * @param <V>: 缓存内容类型
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
public class CaffeineCacheDeleteAction<K, V> extends BaseCaffeineCacheAction<K, V, Long> {
    public CaffeineCacheDeleteAction(CaffeineCacheLayerProcessor processor, String area){
        super(ACTION.DELETE, processor, area);
    }

    @Override
    @SuppressWarnings("unchecked")
    protected Mono<CacheResult<Long>> doAction(CacheExchange<K, V, Long> exchange) {
        AsyncCache<K, V> cache = processor().getCache(exchange.getPolicy(), area);
        Object keys = exchange.getRequest().getAttachment(CacheConstant.ATTACH_KEY);
        long size;
        if (null == keys){
            cache.synchronous().invalidate(exchange.getRequest().getKey());
            size = 1L;
        }else{
            if (keys instanceof Collection){
                Collection<K> c = (Collection)keys;
                cache.synchronous().invalidateAll(c);
                size = c.size();
            }else{
                // array
                K[] ks = (K[])keys;
                cache.synchronous().invalidateAll(Arrays.asList(ks));
                size = ks.length;
            }
        }
        return Mono.just(CacheResult.success(processor().name(), size));
    }
}
