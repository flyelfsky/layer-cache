package cn.flyelf.cache.caffeine.hash;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.caffeine.BaseCaffeineCacheAction;
import cn.flyelf.cache.caffeine.CaffeineCacheLayerProcessor;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.server.CacheExchange;
import com.github.benmanes.caffeine.cache.AsyncCache;
import reactor.core.publisher.Mono;

import java.util.Map;
import java.util.concurrent.CompletableFuture;

/**
 * caffeine的hash类型获取
 * @param <K>: 缓存key类型
 * @param <H>: 缓存map的key类型
 * @param <V>: 缓存map的内容类型
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
public class CaffeineCacheMapGetAction<K, H, V> extends BaseCaffeineCacheAction<K, Map<H, V>, Map<H, V>> {
    public CaffeineCacheMapGetAction(CaffeineCacheLayerProcessor processor, String area){
        super(ACTION.GETMAP, processor, area);
    }
    @Override
    public Mono<Boolean> onLoadAfter(CacheExchange<K, Map<H, V>, Map<H, V>> exchange, Map<H, V> value) {
        AsyncCache<K, Map<H, V>> cache = processor().getCache(exchange.getPolicy(), area);
        CompletableFuture<Map<H, V>> future = CompletableFuture.completedFuture(value);
        cache.put(exchange.getRequest().getKey(), future);
        return Mono.just(true);
    }
    @Override
    protected Mono<CacheResult<Map<H, V>>> doAction(CacheExchange<K, Map<H, V>, Map<H, V>> exchange) {
        AsyncCache<K, Map<H, V>> cache = processor().getCache(exchange.getPolicy(), area);
        CompletableFuture<Map<H, V>> future = cache.getIfPresent(exchange.getRequest().getKey());
        if (null == future){
            return Mono.error(new CacheNotExistException(processor().name(), exchange.getRequest().getKey().toString()));
        }
        return Mono.fromFuture(future).map(r -> CacheResult.success(processor().name(), r));
    }
}
