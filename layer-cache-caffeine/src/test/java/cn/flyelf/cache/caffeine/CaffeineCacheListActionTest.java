package cn.flyelf.cache.caffeine;

import cn.flyelf.cache.core.cache.DefaultListCache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.penetration.caffeine.PenetrateCaffeineCache;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * caffeine缓存的list类型的单元测试
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
public class CaffeineCacheListActionTest {
    private static DefaultListCache<Long, CacheTestModel> cache;
    private static List<CacheTestModel> models;

    @BeforeClass
    public static void initCaffeine(){
        CacheLayerFactory factory = new CacheLayerFactory();
        factory.setPenetration(new PenetrateCaffeineCache(new CachePenetrationConfig()));
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(new String[]{"caffeine"});
        policy.setType(CacheConstant.CACHE_LIST);
        policy.duration("30m");
        List<CachePolicy> policies = new ArrayList<>();
        policies.add(policy);
        factory.setPolicy(policies);

        CacheEventPublisher eventPublisher = new CacheEventPublisherImp();
        CaffeineCacheLayerProcessor processor = new CaffeineCacheLayerProcessor(eventPublisher);
        List<CacheLayerProcessor> layers = new ArrayList<>(1);
        layers.add(processor);
        factory.setLayers(layers);

        CacheTestModel model = new CacheTestModel();
        model.setId(1L);
        model.setDate(new Date());
        model.setName("model1");
        model.setIndex(1);

        CacheTestModel model2 = new CacheTestModel();
        model2.setId(2L);
        model2.setName("model2");
        model2.setIndex(2);

        models = new ArrayList<>(Arrays.asList(model, model2));

        cache = new DefaultListCache<>(factory, policy.getLayer(), Long.class, CacheTestModel.class);
    }

    @Test
    public void testPut(){
        long key = 101L;
        Mono<CacheResult<Boolean>> putResult = cache.put(key, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(key);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && models.size() == r.getValue().size()).verifyComplete();
    }

    @Test
    public void testDelete(){
        long key = 102L;
        Mono<CacheResult<Boolean>> putResult = cache.put(key, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<Long>> delResult = cache.delete(key);
        Assert.assertNotNull(delResult);
        StepVerifier.create(delResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(key);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }

    @Test
    public void testGetNotExist(){
        long key = 103L;
        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(key, k -> models);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && models.size() == r.getValue().size()).verifyComplete();
    }

    @Test
    public void testGetAndAdd(){
        long expectSize = models.size() + 1;
        long key = 104L;
        Mono<CacheResult<Boolean>> putResult = cache.put(key, models);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<List<CacheTestModel>>> getResult = cache.get(key);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).thenConsumeWhile(CacheResult::isSuccess, r -> {
                    CacheTestModel model3 = new CacheTestModel();
                    model3.setId(3L);
                    model3.setName("model3");
                    r.getValue().add(model3);
                })
                .verifyComplete();

        getResult = cache.get(key);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && expectSize == r.getValue().size()).verifyComplete();
    }
}
