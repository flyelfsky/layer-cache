package cn.flyelf.cache.caffeine.util;

import cn.flyelf.cache.caffeine.CaffeineCacheLayerProcessor;
import cn.flyelf.cache.core.CacheAction;
import cn.flyelf.cache.core.model.CachePolicy;
import com.github.benmanes.caffeine.cache.AsyncCache;
import org.junit.Assert;
import org.junit.Test;

import java.lang.reflect.Constructor;


/**
 * caffeine辅助工具的单元测试
 *
 * @author wujr
 * 2020-01-02
 * @version 1.0
 * 变更历史
 * [wujr 2020-01-02 1.0 新增]
 */
public class CaffeineUtilTest {
    @Test
    public void testDurationNull(){
        CachePolicy policy = new CachePolicy();
        AsyncCache<String, String> cache = CaffeineUtil.asyncCache(policy);
        Assert.assertNotNull(cache);
    }
    @Test
    public void testLimitNotNull(){
        CachePolicy policy = new CachePolicy();
        policy.setLimit(100L);
        AsyncCache<String, String> cache = CaffeineUtil.asyncCache(policy);
        Assert.assertNotNull(cache);
    }

    @Test(expected = UnsupportedClassVersionError.class)
    public void testActionConstructorException(){
        CaffeineUtil.actionConstructor(CaffeineUtilTest.class);
    }

    @Test
    public void testActionNull() throws Exception{
        Constructor constructor = CaffeineUtilTest.class.getDeclaredConstructor();
        CaffeineCacheLayerProcessor processor = new CaffeineCacheLayerProcessor(null);
        CacheAction action = CaffeineUtil.action(processor, constructor, "default");
        Assert.assertNull(action);
    }
}
