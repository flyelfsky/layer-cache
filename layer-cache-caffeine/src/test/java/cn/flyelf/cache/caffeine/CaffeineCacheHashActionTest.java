package cn.flyelf.cache.caffeine;

import cn.flyelf.cache.core.cache.DefaultHashCache;
import cn.flyelf.cache.core.action.CacheLayerFactory;
import cn.flyelf.cache.core.action.CacheLayerProcessor;
import cn.flyelf.cache.annotation.CacheConstant;
import cn.flyelf.cache.core.event.CacheEventPublisher;
import cn.flyelf.cache.core.exception.CacheNotExistException;
import cn.flyelf.cache.core.model.CachePolicy;
import cn.flyelf.cache.core.model.CacheResult;
import cn.flyelf.cache.core.model.CacheTestModel;
import cn.flyelf.cache.core.monitor.CacheEventPublisherImp;
import cn.flyelf.cache.penetration.caffeine.PenetrateCaffeineCache;
import cn.flyelf.cache.penetration.conf.CachePenetrationConfig;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

import java.util.*;

/**
 * caffeine缓存的hash类型的单元测试
 *
 * @author wujr
 * 2019/12/23
 * @version 1.0
 * 变更历史
 * [wujr 2019/12/23 1.0 新增]
 */
public class CaffeineCacheHashActionTest {
    private static DefaultHashCache<Long, String, CacheTestModel> cache;
    private static Map<String, CacheTestModel> models;

    @BeforeClass
    public static void initCaffeine(){
        CacheLayerFactory factory = new CacheLayerFactory();
        factory.setPenetration(new PenetrateCaffeineCache(new CachePenetrationConfig()));
        CachePolicy policy = new CachePolicy();
        policy.setObject(CacheTestModel.class);
        policy.setLayer(new String[]{"caffeine"});
        policy.setType(CacheConstant.CACHE_MAP);
        policy.duration("30m");
        List<CachePolicy> policies = new ArrayList<>();
        policies.add(policy);
        factory.setPolicy(policies);

        CacheEventPublisher eventPublisher = new CacheEventPublisherImp();
        CaffeineCacheLayerProcessor processor = new CaffeineCacheLayerProcessor(eventPublisher);
        List<CacheLayerProcessor> layers = new ArrayList<>(1);
        layers.add(processor);
        factory.setLayers(layers);

        CacheTestModel model = new CacheTestModel();
        model.setId(1L);
        model.setDate(new Date());
        model.setName("model1");
        model.setIndex(1);

        CacheTestModel model2 = new CacheTestModel();
        model2.setId(2L);
        model2.setName("model2");
        model2.setIndex(2);

        models = new HashMap<>(2);
        models.put(model.getId().toString(), model);
        models.put(model2.getId().toString(), model2);

        cache = new DefaultHashCache<>(factory, policy.getLayer(), Long.class, CacheTestModel.class);
    }

    @Test
    public void testPut(){
        long key = 101L;
        Mono<CacheResult<Boolean>> putResult = cache.put(key, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<Map<String, CacheTestModel>>> getResult = cache.get(key);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && models.size() == r.getValue().size()).verifyComplete();
    }

    @Test
    public void testDelete(){
        long key = 102L;
        Mono<CacheResult<Boolean>> putResult = cache.put(key, models);
        Assert.assertNotNull(putResult);
        StepVerifier.create(putResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<Long>> delResult = cache.delete(key);
        Assert.assertNotNull(delResult);
        StepVerifier.create(delResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<Map<String, CacheTestModel>>> getResult = cache.get(key);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectError(CacheNotExistException.class).verify();
    }

    @Test
    public void testGetNotExist(){
        long key = 103L;
        Mono<CacheResult<Map<String, CacheTestModel>>> getResult = cache.get(key, k -> models);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && models.size() == r.getValue().size()).verifyComplete();
    }

    @Test
    public void testAdd(){
        long key = 122L;
        CacheTestModel model = new CacheTestModel();
        model.setId(22L);
        model.setName("model22");
        model.setIndex(222);
        Mono<CacheResult<Boolean>> addResult = cache.add(key, model.getId().toString(), model);
        Assert.assertNotNull(addResult);
        StepVerifier.create(addResult).expectNextMatches(CacheResult::isSuccess).verifyComplete();

        Mono<CacheResult<Map<String, CacheTestModel>>> getResult = cache.get(key);
        Assert.assertNotNull(getResult);
        StepVerifier.create(getResult).expectNextMatches(r -> r.isSuccess() && 1 == r.getValue().size()).verifyComplete();
    }
}
