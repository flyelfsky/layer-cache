package cn.flyelf.cache.caffeine;

import cn.flyelf.cache.annotation.ACTION;
import cn.flyelf.cache.core.CacheAction;
import org.junit.Assert;
import org.junit.Test;

/**
 * caffeine的缓存的处理单元测试
 *
 * @author wujr
 * 2020/1/3
 * @version 1.0
 * 变更历史
 * [wujr 2020/1/3 1.0 新增]
 */
public class CaffeineCacheLayerProcessorTest {
    @Test
    public void getActionNull(){
        CaffeineCacheLayerProcessor processor = new CaffeineCacheLayerProcessor(null);
        CacheAction action = processor.action(ACTION.HASMAPKEY, "default");
        Assert.assertNull(action);
    }
}
